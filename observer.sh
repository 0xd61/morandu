#!/usr/bin/env bash

POD=$(kubectl get pods | grep morandu00 | cut -d" " -f1)

kubectl port-forward "$POD" 9001 4369 &
#PORT_FORWARD_PID=$!
# wait until forwarding is ready
sleep 5
iex --name "$(whoami)@127.0.0.1" --cookie "$(kubectl get secret morandu00-config -o "go-template={{index .data \"erlang_cookie\"}}" | base64 -d)"

# kubectl process killed automatically
#kill ${PORT_FORWARD_PID}
