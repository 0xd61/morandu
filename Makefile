.PHONY: help


HASH := `git rev-parse --short HEAD`
export SERVICE := app
export APP := morandu
export PROJECT := $(APP)-253418
export REPOSITORY ?= gcr.io
export PROJECT_ROOT ?= $(PWD)
export IMAGE := $(patsubst /%,%,$(REPOSITORY)/$(PROJECT)/$(SERVICE):$(HASH))

help:
	@perl -nle'print $& if m{^[a-zA-Z_-]+:.*?## .*$$}' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

init: ## Install dependencies
	@echo "init client"
	@make -C ./app init
	@echo "init server"
	@make -C ./server/backend init

build: ## Build server
	@make -C ./app MORANDU_API_URL=/api MORANDU_SOCKET_URL=/socket MORANDU_VERSION="alpha ($(HASH))" __sapper__/export
	@cp -r app/__sapper__/export server/backend/apps/$(APP)/priv/app
	@make -C ./server/backend build

clean: ## Cleanup build files
	@make -C ./app clean
	rm -rf server/backend/apps/$(APP)/priv/app
	@make -C ./server/backend clean

rebuild: clean build

deploy: clean build ## Deploy server to gcloud app engine
	@make -C ./server/backend deploy

# HACK: docker build currently only work with a existing static app
# therefore we need to build it upfront
docker: clean build ## Build a release docker image
	@echo "\n~> building docker image"
	docker image build --build-arg APP_VSN="alpha ($(HASH))" -t $(IMAGE) .
ifeq "$(REPOSITORY)" "gcr.io"
	docker push $(IMAGE)
endif
