#!/usr/bin/env bash

LATEST_TAG=$(git log --tags -1 --pretty=format:'%D' | sed -e 's/.*tag: //' -e 's/,.*//')

APP="morandu"
FRONTEND_DIR="app"
APP_DIR="server/backend"
TARGET="debian10"

if [ "$1" == "prod" ]; then
	echo Build PROD image

	echo "NOTE: you can only build tagged versions for prod!"
	echo "checking out latest version tag: ${LATEST_TAG}"
	git -c advice.detachedHead=false checkout $LATEST_TAG

    echo "#### Building Frontend ####"
    pushd ${FRONTEND_DIR}
    
    MORANDU_API_URL=/api MORANDU_SOCKET_URL=/socket MORANDU_VERSION="${LATEST_TAG}" npm run export

    popd

    echo "#### Copying Frontend into app dir####"
    rm -rf ${APP_DIR}/apps/${APP}/priv/app
    cp -r ${FRONTEND_DIR}/__sapper__/export ${APP_DIR}/apps/${APP}/priv/app

    echo "#### Building Server ####"
    pushd ${APP_DIR}

	mix local.hex --force && mix local.rebar --force
	MIX_ENV=prod mix clean
    rm -rf ./apps/${APP}/priv/store
	mix deps.get --only prod
	MIX_ENV=prod mix compile
	npm install --prefix ./apps/${APP}/assets
	npm run deploy --prefix ./apps/${APP}/assets
	mix phx.digest

    popd

	# NOTE: We need to hack this with docker because the target (debian10) does not find shared libs
	echo "### Building app release ####"
	docker build -t ${APP}_build:${TARGET} -f Dockerfile.${TARGET} .
	CURRENT_UID=$(id -u)
	docker run -e "LANG=C.UTF-8" --rm -v ${PWD}:/build:rw ${APP}_build:${TARGET} /bin/bash -c "useradd -m -u ${CURRENT_UID} temp \
	&& su temp -c \"cd /build/${APP_DIR} && mix local.hex --force \
    && mix local.rebar --force && MIX_ENV=prod mix release --force --overwrite\""

	git checkout -
	echo 'run "./deploy.sh prod" to update the image on the PROD server'
else
    echo "Currently there is only the prod build"
fi
