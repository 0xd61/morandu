export const INITIAL = "INITIAL";
export const FETCHING = "FETCHING";
export const SUCCESS = "SUCCESS";
export const FAILURE = "FAILURE";

function applyState(obj, { loading, success, errors = {}, updates = {} }) {
  let states = {};
  Object.keys(obj).forEach((key, i) => {
    if (obj[key] == null || typeof obj[key] !== "object") {
      states[key] = {
        name: key || "",
        value: obj[key],
        error: null,
        loading: loading,
        success: success
      };
      return;
    }
    if (updates[key]) {
      states[key] = {
        ...updates[key],
        error: errors[key] || null,
        loading: loading,
        success: success
      };
      return;
    }

    states[key] = obj[key];
  });
  return states;
}

export function setState(states, updates, action, opts = {}) {
  const { errors } = opts;
  switch (action) {
    case INITIAL:
      return applyState(states, {
        loading: false,
        success: false,
        updates: updates,
        errors: errors
      });
    case FETCHING:
      return applyState(states, {
        loading: true,
        success: false,
        updates: updates,
        errors: errors
      });
    case SUCCESS:
      return applyState(states, {
        loading: false,
        success: true,
        updates: updates,
        errors: errors
      });
    case FAILURE:
      return applyState(states, {
        loading: false,
        success: false,
        updates: updates,
        errors: errors
      });
  }
}

export function stripStates(states) {
  let data = {};
  Object.keys(states).forEach((key, i) => {
    data[key] = states[key].value;
  });
  return data;
}
