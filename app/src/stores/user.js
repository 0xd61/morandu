import { writable } from "svelte/store";
import { get } from "../api";
import { getCookie, setCookie, deleteCookie } from "../utils";

const TOKEN_KEY = "authorization";
const USER_KEY = "user";
export let user;

if (typeof window !== "undefined") {
  let usr = getLocal() || null;
  const token = getCookie(TOKEN_KEY);

  if (usr) usr = { ...usr, token };

  user = writable(usr);

  lazyRefreshUser(usr, token);
} else {
  user = writable(null);
}

export function setUser({ user: usr, token: token }) {
  if (token) setCookie(TOKEN_KEY, { value: token, days: 365 });
  delete usr.token;
  setLocal(usr);
  user.set({ ...usr, token });
}

export function delUser() {
  user.set(null);
  localStorage.removeItem(USER_KEY);
  deleteCookie(TOKEN_KEY);
}

function setLocal(user) {
  localStorage.setItem(USER_KEY, JSON.stringify(user));
}

function getLocal() {
  const item = localStorage.getItem(USER_KEY);
  return JSON.parse(item);
}

// Tries to refresh user from server. If unauthorized,
// the local user will be deleted to trigger a login
async function lazyRefreshUser(usr, token) {
  if (!usr) return;

  try {
    const res = await get(`users/${usr.id}`, { token });
    if (res && res.id) {
      setUser({ user: res, token: token });
    }
  } catch (e) {
    if (e.status == 401) {
      delUser();
    }
    console.error(e);
  }
}
