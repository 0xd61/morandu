import { writable } from "svelte/store";

export let connected = writable(true);

export function setConnected(boolValue) {
  connected.set(boolValue);
}
