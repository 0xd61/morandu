import { getTranslator } from "../locale";
import { setTranslate } from "../stores/locale";

// Set locale to es
export default async function(req, res, next) {
  try {
    const translator = await getTranslator("es");
    setTranslate(translator);
    next();
  } catch (error) {
    next(error);
  }
}
