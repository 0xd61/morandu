import { locale } from "./utils";

let store = {};
const base = process.env.MORANDU_API_URL;

function send({ method, path, data, token, opts }) {
  const fetch = process.browser ? window.fetch : require("node-fetch").default;
  const timeout = opts.timeout || 10;
  const uri = url(path, opts);

  // make sure default values do not overwrite opts
  opts = { method, headers: {}, ...opts };

  if (data) {
    opts.headers["Content-Type"] = "application/json";
    opts.body = JSON.stringify(data);
  }

  if (token) {
    opts.headers["Authorization"] = `Bearer ${token}`;
  }

  return timeout_(timeout, fetch(uri, opts).then(r => handleResponse(r)));
}

function timeout_(s, promise) {
  return new Promise(function(resolve, reject) {
    setTimeout(function() {
      reject({code: "timeout", message: "Request Timeout", status: 408});
    }, s * 1000);
    promise.then(resolve, reject);
  });
}

function handleResponse(resp) {
  return new Promise(function(resolve, reject) {
    resp.text().then(json => {
      const data = parseJSON(json);
      if (resp.ok) {
        resolve(data);
      } else {
        const { error } = data;
        // NOTE: It can happen that another server returns an error
        // with a unknown structure. If this is the case, we
        // reject with a standard internal server error
        if (error)
          reject(error);
        else
          reject({status: 500, code: "error", message: "Internal Server Error"})
      }
    });
  });
}

function parseJSON(json) {
  try {
    return JSON.parse(json);
  } catch (err) {
    return json;
  }
}

export function url(path, opts = {}) {
  const version = opts.version || "v1";
  let url = `${base}/${version}/${path}`;

  if (url.match(/.*\?/g)) {
    return `${url}&lang=${locale()}`
  } else {
    return `${url}/?lang=${locale()}`
  }
}

export function prefetch(path, opts) {
  const expire = opts.expire || 5;
  const refetch = opts.refetch || false;
  // Check if already in prefetch store
  if (path in store) {
    console.log("load from store at: " + Date.now());
    return new Promise((resolve, _reject) => {
      resolve(store[path]);
    });
  } else {
    console.log("load from api at: " + Date.now());
    let promise = get(path, opts);
    setTimeout(() => {
      console.log(`delete "${path}" from prefetch store`);
      delete store[path];
      // TODO: We need to prefetch and just overwrite, if we
      // do a refetch
      if (refetch) prefetch(path, opts);
    }, expire * 1000);

    store[path] = promise;
    return promise;
  }
}

export function get(path, opts = {}) {
  const { token } = opts;
  return send({ method: "GET", path, token, opts });
}

export function del(path, opts = {}) {
  const { token } = opts;
  return send({ method: "DELETE", path, token, opts });
}

export function post(path, data, opts = {}) {
  const { token } = opts;
  return send({ method: "POST", path, data, token, opts });
}

export function put(path, data, opts = {}) {
  const { token } = opts;
  return send({ method: "PUT", path, data, token, opts });
}
