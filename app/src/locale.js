import { FluentBundle, FluentResource } from "@fluent/bundle";

function buildBundle(locale, ftl) {
  const resource = new FluentResource(ftl);
  const bundle = new FluentBundle(locale, { useIsolating: false });
  bundle.addResource(resource);
  return bundle;
}

export async function getTranslator(locale) {
  const bundles = [];
  if (locale !== "en") {
    const { default: translation } = await selectImport(locale);
    bundles.push(buildBundle(locale, translation));
  }
  const { default: en } = await selectImport();
  bundles.push(buildBundle("en", en));
  return function(id, attrs) {
    for (let bundle of bundles) {
      const msg = bundle.getMessage(id);
      if (msg && msg.value) {
        return bundle.formatPattern(msg.value, attrs);
      }
    }
  };
}

// TODO: make dynamic imports?
function selectImport(locale) {
  switch (locale) {
    case "es":
      return import("./../../locales/es/morandu.ftl");
    default:
      return import("./../../locales/en/morandu.ftl");
  }
}
