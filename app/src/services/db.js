import { openDB } from "idb";

const DEBTOR_STORE = "debtors";
const META_STORE = "meta";

export default {
  dbPromise: null,
  isInitialized: false,
  init(name, version) {
    if (!("indexedDB" in self)) {
      console.log("This browser doesn't support IndexedDB");
      return null;
    }

    if(!this.dbPromise){
      this.dbPromise = openDB(name, version, {
        upgrade(db) {
          // On upgrade remove existing stores to rebuild db
          [...db.objectStoreNames].forEach(store => {
            db.deleteObjectStore(store);
          });

          // Create a store of objects
          if (!db.objectStoreNames.contains(META_STORE)) {
            db.createObjectStore(META_STORE, {
              keyPath: "key",
              autoIncrement: false
            });
          }

          if (!db.objectStoreNames.contains(DEBTOR_STORE)) {
            const store = db.createObjectStore(DEBTOR_STORE, {
              keyPath: "id",
              autoIncrement: false
            });
            // Create an index on the 'd_cedula' property of the objects.
            store.createIndex("d_cedula", "d_cedula");
            store.createIndex("d_ruc", "d_ruc");
          }
        }
      });
    }
    this.isInitialized = true;
    return this.dbPromise;
  },
  async set(data, event = undefined) {
    if (!Array.isArray(data)) {
      return this.set([data], event_id);
    }

    this.update_event(event);

    if (data.length === 0) return;

    const tx = (await this.dbPromise).transaction(DEBTOR_STORE, "readwrite");
    data.forEach(item => {
      tx.store.put(item);
    });
    await tx.done;
  },
  async del(data, event = undefined) {
    if (!Array.isArray(data)) {
      return this.del([data], event_id);
    }

    this.update_event(event);

    if (data.length === 0) return;

    const tx = (await this.dbPromise).transaction(DEBTOR_STORE, "readwrite");
    data.forEach(item => {
      tx.store.delete(item);
    });
    await tx.done;
  },
  async get_by_cedula(cedulaId) {
    const tx = (await this.dbPromise).transaction(DEBTOR_STORE, "readonly");
    const index = tx.store.index("d_cedula");
    const data = index.getAll(cedulaId);
    await tx.done;
    return data;
  },

  async get_by_ruc(rucId) {
    const tx = (await this.dbPromise).transaction(DEBTOR_STORE, "readonly");
    const index = tx.store.index("d_ruc");
    const data = index.getAll(rucId);
    await tx.done;
    return data;
  },

  async update_event(id = undefined) {
    const event = await this.get_latest_event();

    // if no id provided simply update the timestamp
    if (id === undefined) {
      id = (event && event.id) || undefined;
    }

    // if a more recent event already happened
    // do nothing
    if (id < event && event.id) return;

    const tx = (await this.dbPromise).transaction(META_STORE, "readwrite");
    tx.store.put({ key: "latest_event", id: id, inserted_at: Date.now() });
    await tx.done;
  },
  async get_latest_event() {
    let db = await this.dbPromise;
    let result = null;
    if(db) {
      const tx = db.transaction(META_STORE, "readonly");
      result = tx.store.get("latest_event");
      await tx.done;
    }

    return result;
  }
};
