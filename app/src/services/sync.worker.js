import { expose } from "comlink";
import channel from "./socket";
import db from "./db";

const timestamp = process.env.MORANDU_TIMESTAMP;
const syncInterval = 1 * 60; // in seconds
const maxDbAge = 15; // in days (if db is older, a get request is rejected)
let interval = undefined;

const workerAPI = {
  async init_db() {
    db.init("morandu", timestamp);
  },
  async init_sync(phxToken) {
    if (!phxToken) return;
    channel.connect(phxToken, "sync:lobby");
    channelEventHandler(channel.Channel);

    this.sync();

    if(interval) {
      interval = clearInterval(interval);
    }
    interval = setInterval(this.sync, syncInterval * 1000);
  },
  async disconnect_sync() {
    if (interval) {
      clearInterval(interval);
      interval = null;
    }
    channel.disconnect();
  },
  async status() {
    return {
      isInitialized: channel.IsInitialized,
      connected: channel.Socket && channel.Socket.isConnected() || false
    };
  },
  async sync() {
    if(!db.isInitialized) db.init("morandu", timestamp);

    const event = await db.get_latest_event();
    const params = { last_event: (event && event.id) || 0 };
    channel.Channel.push("sync", { params });
  },
  async get(query) {
    // Check if ruc or cedula to search in correct index
    if (/^\d*-\d{1}/.test(query)) {
      return validateDbAge().then(() => db.get_by_ruc(query));
    }
    else {
      return validateDbAge().then(() => db.get_by_cedula(query));
    }
  }
};

expose(workerAPI);

function channelEventHandler(channel) {
  channel.on("changes", async ({ changes, event }) => {
    const { delete: toDelete, insert: toInsert } = changes;

    db.set(toInsert, event);
    db.del(toDelete.map(item => item.id), event);
  });
}

function validateDbAge() {
  if(!db.isInitialized) db.init("morandu", timestamp);
  return new Promise(function(resolve, reject) {
    db.get_latest_event()
      .then(event => {
        if (event.inserted_at < Date.now() - _days(maxDbAge)) {
          reject(Error("Offline data too old"));
        } else {
          resolve(true);
        }
      })
      .catch(err => reject(err));
  });
}

function _days(days) {
  return days * 24 * 60 * 60 * 1000;
}
