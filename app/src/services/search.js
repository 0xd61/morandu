import { wrap } from "comlink";
import { user } from "../stores/user";
import { setConnected } from "../stores/system";
import Worker from "./sync.worker.js";
import * as backend from "../api";

const connectionRetryIntervalTimer = 10; // in seconds
const statusIntervalTimer = 2; // in seconds

let api;
let apiToken = null;
let statusInterval = undefined;
let connectionRetryInterval = undefined;

if (process.browser && window.Worker) {
  const worker = new Worker();
  api = wrap(worker);

  // Update sync connection on each login/logout
  const unsubscribe = user.subscribe(current_user => {
    // NOTE: If we do not have a user and usertoken, we disconnect an clean up
    if (!current_user || !current_user.token) {
      apiToken = null;
      if (!api) return;
      api.disconnect_sync();
      if (statusInterval) {
        statusInterval = clearInterval(statusInterval);
      }
      return;
    }

    // TODO: Is this necessary here or should we handle everything in the
      // worker?
    api.init_db();

    // NOTE: Connect to channel, if the connection fails, we try again after some time
    connectToChannel(current_user.token);


    if (statusInterval) {
      statusInterval = clearInterval(statusInterval);
    }

    statusInterval = setInterval(async () => {
      let status = await api.status();
      setConnected(status.connected);

      // NOTE: To connect to the socket we need a token. If we are disconnected, the token may expire.
      // If the token expires, we constantly get a 403 error. To solve this issue, we check if we are
      // connected every 2 seconds. If we disconnect, we start the connection retry interval. If we
      // reconnect without a forbidden error, we clear the interval. If we get the error we stay
      // disconnected and we try to reinitiate the connection with a new token. The socket must then
      // cleanup the old socket.
      // TODO: it can happen that the status interval fails. Currently we have no idea why...
      if(status.connected) {
        if(connectionRetryInterval) {
          connectionRetryInterval = clearInterval(connectionRetryInterval);
        }
      } else {
        if(!connectionRetryInterval) {
          connectionRetryInterval = setInterval(() => connectToChannel(current_user.token), connectionRetryIntervalTimer*1000);
        }
      }
    }, statusIntervalTimer*1000);
  });
}

function connectToChannel(apiToken) {
  backend
    .post("channel", {}, { token: apiToken })
    .then(({ token }) => {
      api.init_sync(token);
    })
    .catch(_error => {
      console.log(`Failed to get the socket token.`);
    });
}

export default {
  get(query) {
    return api
      .get(String(query))
      .then(res => res)
      .catch(async err => {
        if (err) console.error(err);

        // fallback to network request
        if (apiToken) {
          return await backend.get(`lookup/${query}`, { token: apiToken });
        }
      });
  },
  sync() {
    api.sync();
  }
};
