import { Socket } from "phoenix";
import * as api from "../api";

const base = process.env.MORANDU_SOCKET_URL;

export default {
  IsInitialized: false,
  Socket: null,
  Channel: null,
  connect(token, channel, parameters) {
    const params = parameters || {};

    if (!token) {
      this.disconnect()
    }

    // NOTE: cleanup if a socket already exists
    if(this.Socket) this.disconnect();

    this.Socket = initSocket(token);
    // NOTE: We do not try to reconnect here, because if the downtime
    // was too long, the token could be invalid. Therefore this case is
    // handled in the caller code.
    //
    // this.Socket.onError(error => {
    //   if (this.IsInitialized == false) {
    //     console.log(`Trying again to initialize the socket in ${initRetryTimer} seconds.`);
    //     setTimeout(() => this.connect(token, channel, parameters), initRetryTimer*1000);
    //   }
    // });
    this.Socket.connect();

    this.Channel = initChannel(this.Socket, channel, params);
    this.Channel.join()
      .receive("ok", _resp => {
        this.IsInitialized = true;
      })
      .receive("error", _resp => {
        this.IsInitialized = false;
      });
  },
  disconnect() {
    if (this.Socket) {
      let socket = this.Socket;
      socket.disconnect(() => console.log(`Socket ${socket.morandu_id} disconnected`));
    }

    this.IsInitialized = false;
    this.Channel = null;
    this.Socket = null;
  }
};

function initSocket(token) {
  // NOTE: this ID is only for easier debugging
  const id = Math.floor(Math.random() * 1000 + 1);
  let socket = new Socket(base, {
    params: { token: token },
    logger: (kind, msg, data) => {
      console.log(`${id} => ${kind}: ${msg}`, data);
    }
  });

  socket.morandu_id = id;
  return socket;
}

function initChannel(socket, channel, params) {
  let chan = socket.channel(channel, { params });
  return chan;
}
