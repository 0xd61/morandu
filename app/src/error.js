// NOTE: t is the translation function which returns the translation for a key
export function error_msg(t, {status, code, message}) {
    if (!t) return "Something went wrong. Please try again later";

    switch(status) {
        case 403:
            return t("errorPermission");
        case 408:
            return t("errorTimeout");
        default:
            return t("errorGlobal");
    }
}
