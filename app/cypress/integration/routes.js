describe("Routes are exported", function() {
  it("Logged out", function() {
    cy.visit("/");
    cy.get(".modal").should("exist");
    cy.visit("/dashboard");
    cy.get(".modal").should("exist");
    cy.visit("/settings");
    cy.get(".modal").should("exist");
    cy.visit("/submission");
    cy.get(".modal").should("exist");
  });

  it("Logged in", function() {
    // Login
    login("/");

    cy.visit("/dashboard");
    cy.get(".modal").should("not.exist");

    cy.visit("/settings");
    cy.get(".modal").should("not.exist");

    cy.visit("/submission");
    cy.get(".modal").should("not.exist");
  });
});

describe("Settings", function() {
  beforeEach(() => {
    login("/settings");
  });

  it("shows user and person", function() {
    cy.get("#settings\\.phone").should("have.value", "0983123456");
    cy.get("#settings\\.email").should("have.value", "test@test.co");
  });

  it("can update email", function() {
    cy.get("#settings\\.email + button").should("not.exist");

    // Show error with invalid email
    cy.get("#settings\\.email")
      .clear()
      .type("invalid email");
    cy.get("#settings\\.email + button")
      .should("exist")
      .click();
    cy.contains("has invalid format").should("exist");

    // Save button should still exist
    cy.wait(2500);
    cy.get("#settings\\.email + button").should("exist");

    // Update with correct email
    cy.get("#settings\\.email")
      .clear()
      .type("test2@test.co");
    cy.get("#settings\\.email + button")
      .should("exist")
      .click();

    // Save button should disappear
    cy.wait(2500);
    cy.get("#settings\\.email + button").should("not.exist");

    // Cleanup
    cy.get("#settings\\.email")
      .clear()
      .type("test@test.co");
    cy.get("#settings\\.email + button")
      .should("exist")
      .click();
    cy.wait(500);
  });
});

describe("Dashboard", function() {
  beforeEach(() => {
    login("/dashboard");
  });

  it("shows person and cases", function() {
    // Check for loaded info
    cy.get("#dashboard\\.person").should("contain", "Test Company");
    cy.get("#dashboard\\.cedula").should("contain", "112233-4");

    cy.get("#dashboard\\.cases > div").should($cases => {
      // resolve case
      $cases[0].getElementsByTagName("button")[0].click();

      // new case should not inherit the css
      expect($cases[0].className).to.not.match(/.btn-disabled/);
    });
  });
});

describe("Submission", function() {
  beforeEach(() => {
    login("/submission");
  });

  it("creates new case", function() {
    // shows form errors
    cy.contains("Save").click();
    cy.contains("Validation error").should("exist");
    cy.contains("can't be blank").should("exist");

    cy.get("#submission\\.cedula").type("98573621-2");
    cy.get("#submission\\.name").type("Something Unknown");
    cy.get("#submission\\.phone").type("0987654321");
    cy.get("#submission\\.proof").then(subject => {
      const blob = new Uint8Array([137, 80, 78, 71, 13, 10, 26, 10, 0, 0, 0, 13, 73, 72, 68, 82, 0, 0, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 55,110,249, 36, 0, 0, 0, 4,103, 65, 77, 65, 0, 0,177,143, 11,252, 97, 5, 0, 0, 0, 32, 99, 72, 82, 77, 0, 0,122, 38, 0, 0,128,132, 0, 0, 250, 0, 0, 0,128,232, 0, 0,117, 48, 0, 0,234, 96, 0, 0, 58,152, 0, 0, 23,112,156,186, 81, 60, 0, 0, 0, 2, 98, 75, 71, 68, 0, 1,221,138, 19,164, 0, 0, 0, 7,116, 73, 77, 69, 7,227, 12, 23, 19, 22, 53, 0,113, 45,118, 0, 0, 0, 10, 73, 68, 65, 84, 8,215,99, 96, 0, 0, 0, 2, 0, 1,226, 33,188, 51, 0, 0, 0, 37,116, 69, 88,116,100, 97,116,101, 58, 99,114,101, 97,116,101, 0, 50, 48, 49, 57, 45, 49, 50, 45, 50, 51, 84, 49, 57, 58, 50, 50, 58, 53, 51, 43, 48, 48, 58, 48, 48, 7, 62,146, 22, 0, 0, 0, 37,116, 69, 88,116, 100, 97,116,101, 58,109,111,100,105,102,121, 0, 50, 48, 49, 57, 45, 49, 50, 45, 50, 51, 84, 49, 57, 58, 50, 50, 58, 53, 51, 43, 48, 48, 58, 48, 48,118, 99, 42,170, 0, 0, 0, 0, 73, 69, 78, 68,174, 66, 96,130]);
      const testFile = new File([blob], `${Date.now()}.png`, {
        type: "image/png"
      });
      const el = subject[0];
      const dataTransfer = new DataTransfer();
      dataTransfer.items.add(testFile);
      el.files = dataTransfer.files;
      cy.wrap(subject).trigger('change', { force: true })
    });

    cy.contains("Save").click();
    cy.contains("Creating Case");
    cy.wait(2500);

    cy.location("pathname").should("eq", "/dashboard");
    cy.contains("98573621-2").should("exist");
  });
});

function login(route) {
  cy.visit(route);
  cy.get("#login\\.email").type("test@test.co");
  cy.get("#login\\.password").type("password");
  cy.contains("Continue").click();
  cy.get(".modal").should("not.exist");
}
