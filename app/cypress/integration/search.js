describe("Search by Cedula or RUC on /", function() {
  beforeEach(() => {
    login();
  });

  it("Search by Cedula", function() {
    cy.visit("/");

    // Search existing
    cy.get("#search").type("234567");
    cy.contains("Submit").click();

    // Get result
    cy.get(".result").should("contain", "Be careful, we found 1 entry");
    cy.get(":nth-child(2) > .flex").should($cases => {
      expect($cases).to.have.length(1);
      expect($cases.eq(0)).to.contain("Test Company");
      expect($cases.eq(0)).to.contain("112233-4");
    });

    // Search not existing
    cy.get("#search").type("something else");
    cy.contains("Submit").click();

    // Get result
    cy.get(".result").should(
      "contain",
      "Whoa, we did not find anything. You are good to go"
    );
    cy.get(":nth-child(2) > .flex").should($cases => {
      expect($cases).to.have.length(0);
    });
  });

  it("Search by RUC", function() {
    cy.visit("/");

    // Search existing
    cy.get("#search").type("234567-0");
    cy.contains("Submit").click();

    // Get result
    cy.get(".result").should("contain", "Be careful, we found 1 entry");
    cy.get(":nth-child(2) > .flex").should($cases => {
      expect($cases).to.have.length(1);
      expect($cases.eq(0)).to.contain("Test Company");
      expect($cases.eq(0)).to.contain("112233-4");
    });

    // Search not existing
    cy.get("#search").type("something else");
    cy.contains("Submit").click();

    // Get result
    cy.get(".result").should(
      "contain",
      "Whoa, we did not find anything. You are good to go"
    );
    cy.get(":nth-child(2) > .flex").should($cases => {
      expect($cases).to.have.length(0);
    });
  });
});

describe("Search/Filter on dashboard", function() {
  it("Filter cases", function() {
    login();
    cy.visit("/dashboard");
    cy.get("#search").type("234567");
    cy.get("#dashboard\\.cases > div").should($cases => {
      expect($cases).to.have.length(1);
      expect($cases.eq(0)).to.contain("234567-0");
      expect($cases.eq(0)).to.contain("Person 234567");
    });

    cy.get("#search").type("8");
    cy.get("#dashboard\\.cases > div").should($cases => {
      expect($cases).to.have.length(0);
    });
  });
});

function login() {
    cy.visit("/");
    cy.get("#login\\.email").type("test@test.co");
    cy.get("#login\\.password").type("password");
    cy.contains("Continue").click();
    cy.get(".modal").should("not.exist");
}
