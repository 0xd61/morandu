const defaults = {
  MORANDU_API_URL: "http://localhost:4000/api",
  MORANDU_SOCKET_URL: "ws://localhost:4000/socket",
  MORANDU_TIMESTAMP: Date.now(),
  MORANDU_VERSION: "",
  MORANDU_LOCALES: ["es"]
};

module.exports = (targetPrefix = "process.env.") => {
  const SAPPER_APP_ENV_VARS = {};
  for (let key in defaults) {
    let envVar = process.env[key];
    if (envVar) {
      SAPPER_APP_ENV_VARS[targetPrefix + key] = "'" + envVar + "'";
    } else {
      SAPPER_APP_ENV_VARS[targetPrefix + key] = "'" + defaults[key] + "'";
    }
  }
  return SAPPER_APP_ENV_VARS;
};
