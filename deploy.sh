#!/usr/bin/env bash

USER="nobody"
GROUP="nogroup"
# make sure this is the correct app name (check mix.exs)
APP="morandu"
APP_DIR="./server/backend"
HOME_DIR="/opt/${APP}"
REMOTE_HOSTNAME="morandu00-entry"
REMOTE_HOST_SSH="gcloud beta compute ssh --zone us-east1-b root@${REMOTE_HOSTNAME} --project morandu-253418"
REMOTE_HOST_SCP="gcloud beta compute scp --zone us-east1-b --compress --recurse --project morandu-253418"

if [ "$1" == "prod" ]; then
  set -e
	read -p "Confirm if you want to deploy to PROD? [yN] " answer
	if [ "$answer" != "${answer#[Yy]}" ] ;then
        # get beam binary for setcap
        BEAM_PATH=$(ls ${APP_DIR}/_build/prod/rel/${APP}/erts*/bin/beam*)
        BEAM_BIN=${BEAM_PATH/"$APP_DIR/_build/prod/rel/${APP}/"/}

        # TODO: create a tar, upload it and untar on target via ssh
        tar -czvf /tmp/${APP}.tar.gz -C ${APP_DIR}/_build/prod/rel/${APP} .
		${REMOTE_HOST_SCP} /tmp/${APP}.tar.gz root@${REMOTE_HOSTNAME}:${HOME_DIR}
		${REMOTE_HOST_SSH} --command "cd ${HOME_DIR} \
            && tar -xvf ${APP}.tar.gz \
            &&  chown ${USER}:${GROUP} -R ${HOME_DIR}"

        echo
        echo "Please create/copy the env file by hand"
        echo
#		rsync -Pzr --ignore-existing .env ${REMOTE_HOST}:${HOME_DIR}/env
		${REMOTE_HOST_SSH} --command "chown root:root ${HOME_DIR}/env && chmod 600 ${HOME_DIR}/env"
#		ssh ${REMOTE_HOST} "chown root:root ${HOME_DIR}/env && chmod 600 ${HOME_DIR}/env"

        echo "Setting cap for ${HOME_DIR}/${BEAM_BIN}"
		${REMOTE_HOST_SSH} --command "setcap CAP_NET_BIND_SERVICE=+eip ${HOME_DIR}/${BEAM_BIN}"
        echo

        echo "Creating certs directory at ${HOME_DIR}/certs\nPlease set the corresponding env variable: MORANDU_CERT_DIR=${HOME_DIR}/certs"
		${REMOTE_HOST_SSH} --command "mkdir -p ${HOME_DIR}/certs && chown ${USER}:${GROUP} -R ${HOME_DIR}/certs"
        echo

		echo "Please make sure the service files are up to date."
		echo -e "To build the service files run \n$0 service\n"
		echo "Please restart the service manually via \"${REMOTE_HOST_SCP}\"!"
		echo -e "Run seeds with:\n export \$(cat ${HOME_DIR}/env | xargs) && ${HOME_DIR}/bin/${APP} eval \"Saldo.ReleaseTasks.seed()\"\n"
	fi
elif [ "$1" == "service" ]; then
		sed -e "s#{{PATH}}#$HOME_DIR#g" -e "s#{{NAME}}#$APP#g" -e "s#{{USER}}#$USER#g" morandu.service.template > /tmp/${APP}.service
		${REMOTE_HOST_SCP} /tmp/${APP}.service root@${REMOTE_HOSTNAME}:/etc/systemd/system/${APP}.service

		${REMOTE_HOST_SSH} --command "            && systemctl daemon-reload \
            && systemctl enable ${APP}.service \
            && systemctl start ${APP}.service"
else
    echo "Currently there is only deployment to prod"
fi
