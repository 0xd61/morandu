ARG ALPINE_VERSION=3.11

# Build elixir server
FROM erlang:22-alpine as build

ARG APP_EXPORT=/app/__sapper__/export
ARG SERVER_DIR=server
ARG BUILD_DIR=${SERVER_DIR}/backend
ARG APP_NAME=morandu
ARG APP_VSN
ARG MIX_ENV=prod

ARG APP_SUBDIR=apps/${APP_NAME}

# elixir expects utf8.
ENV APP_NAME=${APP_NAME} \
    VERSION=${APP_VSN} \
    MIX_ENV=${MIX_ENV} \
    ELIXIR_VERSION="v1.10.1" \
    RUST_VERSION="1.41.0" \
    RUSTUP_HOME=/usr/local/rustup \
    CARGO_HOME=/usr/local/cargo \
    PATH=/usr/local/cargo/bin:$PATH \
	  LANG=C.UTF-8

# build rustlang
RUN set -eux \
    && deps=' \
        ca-certificates \
        gcc \
        curl \
       ' \
    && apk add --no-cache --virtual .deps $deps \
    && url="https://static.rust-lang.org/rustup/archive/1.21.1/x86_64-unknown-linux-musl/rustup-init" \
    && wget "$url" \
    && echo "0c86d467982bdf5c4b8d844bf8c3f7fc602cc4ac30b29262b8941d6d8b363d7e *rustup-init" | sha256sum -c - \
    && chmod +x rustup-init \
    && ./rustup-init -y --no-modify-path --default-toolchain $RUST_VERSION \
    && rm rustup-init \
    && chmod -R a+w $RUSTUP_HOME $CARGO_HOME; \
    rustup --version; \
    cargo --version; \
    rustc --version;
# end build rustlang

# build elixir
RUN set -xe \
	&& ELIXIR_DOWNLOAD_URL="https://github.com/elixir-lang/elixir/archive/${ELIXIR_VERSION#*@}.tar.gz" \
	&& ELIXIR_DOWNLOAD_SHA512="823a685d62b6181be9f3314c1a86b69606dd3d3528a2053060f83802a561d8e136fb32099d51cb90542ea155a0a5768d7e6fc7aa1ae18d2c9f5fd5a5a2ac9cdc" \
	&& buildDeps=' \
		ca-certificates \
		curl \
		make \
	' \
	&& apk add --no-cache --virtual .build-deps $buildDeps \
	&& curl -fSL -o elixir-src.tar.gz $ELIXIR_DOWNLOAD_URL \
	&& echo "$ELIXIR_DOWNLOAD_SHA512  elixir-src.tar.gz" | sha512sum -c - \
	&& mkdir -p /usr/local/src/elixir \
	&& tar -xzC /usr/local/src/elixir --strip-components=1 -f elixir-src.tar.gz \
	&& rm elixir-src.tar.gz \
	&& cd /usr/local/src/elixir \
	&& make install clean \
	&& apk del .build-deps
# end build elixir

# install build dependencies
RUN apk add --update git build-base nodejs nodejs-npm python


# set build ENV
ENV MIX_ENV=prod

COPY ${SERVER_DIR} /${SERVER_DIR}
COPY locales /locales

WORKDIR /${BUILD_DIR}

RUN make init && make build && mix release && cp -r _build/prod/rel /release

# prepare release image
FROM alpine:${ALPINE_VERSION} AS app
RUN apk add --update --no-cache bash openssl build-base

# TODO: automatically sync cert path/volume with k8s config
RUN mkdir /app
WORKDIR /app

COPY --from=build /release ./
RUN chown -R nobody: /app

USER nobody

ENV HOME=/app \
    REPLACE_OS_VARS=true

CMD ["morandu/bin/morandu", "start"]

HEALTHCHECK --interval=30s --timeout=30s --start-period=5s --retries=2 \
 CMD nc -vz -w 2 localhost 8080 || exit 1
