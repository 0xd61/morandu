# Morandu is a brand name and should not be localized.
-morandu-brand = Morandu
-morandu-short-brand = Morandu
-samuu = Samuu S.R.L.
# deprecated
globalError = Something went wrong. Please try again later
navRouteDashboard = Dashboard
introHeading = Search
introTitle = { -morandu-brand }
introCaseLinkLabel = Report new case...
searchInputLabel = Search Cedula/RUC
searchDashboardLabel = Filter Cedula/RUC
searchButton = Submit
resultHeading =
    { $entries ->
        [zero] Result for { $id }
        [one] Result for { $id }
       *[other] Results for { $id }
    }
resultErrorHeading = Error
resultNothingFoundText = Whoa, we did not find anything. You are good to go
resultFoundText =
    { $entries ->
        [one] Be careful, we found { $entries } entry
       *[other] Be careful, we found { $entries } entries
    }
resultCardHeading = owes to
resultCardSince = since { DATETIME($date, month: "short", year: "numeric", day: "2-digit") }
loginHeading = Welcome to
loginEmailLabel = Your email
loginPasswordLabel = Your password
loginSubmitButton = Continue
loginContactLinkLabel = Contact Us
loginErrorInvalidEmail = Your email or password is invalid
loginErrorNotActive = This user account is deactivated
pricingMonthlyPeriod = monthly
pricingTierRequests = Unlimited requests
pricingTierCaseSubmission = Unlimited submission of cases
pricingTierTrial = 14 days trial
pricingTierBasicSupport = Basic Support
pricingTierProSupport = Pro Support
pricingTierFeatureRequests = Feature Requests
pricingTierFreeTitle = Free
pricingTierFreePrice = FREE
pricingTierFreeLinkLabel = Sign Up
pricingTierBasicTitle = Basic
pricingTierBasicLinkLabel = Contact us
pricingTierProTitle = Pro
pricingTierProLinkLabel = Contact us
submitButton = Save
backButton = Back
footerOfflineNotification = You are currently offline. Please expect limited functionality
footerConnectionNotification = No connection to the server. Please expect limited functionality
footerUpdateNotification = A new version is available.
footerUpdateRefresh = Please refresh.
footerLinkPricing = Pricing
footerCopyRight = © { -samuu } All rights reserved.
settingsHeading = Settings
settingsUserHeading = Login Credentials
# maybe rename to profileUserEmailTitle
userEmailTitle = Email Address
# maybe rename to profileUserEmailSubtitle
userEmailSubtitle = This is your login email address
# maybe rename to profileUserEmailInputPlaceholder
userEmailInputPlaceholder = Please enter your email address
# maybe rename to profileUserNameTitle
userNameTitle = Username
# maybe rename to profileUserNameInputPlaceholder
userNameInputPlaceholder = Please input your preferred username
# maybe rename to profileUserPasswordTitle
userPasswordTitle = Change Password
# maybe rename to profileUserPasswordInputPlaceholder
userPasswordInputPlaceholder = Please enter a secure password
# maybe rename to profileUserPasswordFooter
userPasswordFooter = Use a password with at least { $length } characters.
# maybe rename to profileUserDangerHeading
userDangerHeading = Danger Zone
# maybe rename to profileUserLogoutTitle
userLogoutTitle = Logout
# maybe rename to profileUserLogoutSubtitle
userLogoutSubtitle = Closes current session
# maybe rename to profileUserLogoutButton
userLogoutButton = Logout
dashboardHeading = Dashboard
#deprecated
dashboardPermissionError = Permission denied. Please contact your supervisor
dashboardMenuCases = Cases
dashboardMenuMembers = Members
dashboardMenuAccount = Account
dashboardActiveCasesHeading = Active Cases
dashboardEmptyCasesText = No active case. Nice!
dashboardCasesCardInfoHeading = Not yet verified
dashboardCasesCardResolveButton = done
dashboardCasesCardSince =  since { DATETIME($date, month: "long", year: "numeric", day: "2-digit") }
dashboardCasesCardAmount = owes ₲ { NUMBER($number) }
dashboardMembersHeading = Members
dashboardMembersRole1 = Admin
dashboardMembersRole2 = Reader
dashboardMembersRoleUpdateSuccess = Role updated
dashboardUserHeading = Settings
dashboardAccountHeading = Settings
# maybe rename to dashboardSettingsReadOnlyInfo
personReadOnlyInfo = Cannot be changed, currently.
# maybe rename to dashboardSettingsPhoneTitle
personPhoneTitle = Emergency Phone Number
# maybe rename to dashboardSettingsPhoneSubtitle
personPhoneSubtitle = How can we reach you if something happens?
# maybe rename to dashboardSettingsPhoneInputPlaceholder
personPhoneInputPlaceholder = Please enter your phone number
errorRefreshButton = Refresh
submissionHeading = Submission
submissionTitle = Report new case
submissionCedulaTitle = Cedula or RUC
submissionCedulaSubtitle = Who owes you money?
submissionCedulaPlaceholder = Input Cedula/RUC
submissionNameTitle = Name
submissionNameSubtitle = What is the name of the debtor?
submissionNamePlaceholder = Input debtor name
submissionPhoneTitle = Phone Number
submissionPhoneSubtitle = What is the phone number of the debtor?
submissionPhonePlaceholder = Input debtor phone number
submissionSignedAtTitle = Contract Closing
submissionSignedAtSubtitle = When was the contract signed?
submissionAmountTitle = Amount
submissionAmountSubtitle = What is the amount?
submissionAmountValue = { NUMBER($number) }
submissionProofTitle = Factura
submissionProofSubtitle = Please upload an image of the factura as a proof
submissionProofUploadButton = Upload
submissionProofFooter = Images and PDFs are supported
submissionLoadingTitle = Creating Case...
submissionErrorPrefix = Oops, something unexpected happened
submissionConfirmTitle = Is this case resolved?
submissionConfirmSubtitle = Resolve this case if { $id } have cleared their debts
submissionConfirmInputLabel = Type { $id } to confirm
errorPermission = Permission denied. Please contact your supervisor
errorTimeout = Request timeout. Please try again later
errorGlobal = Something went wrong. Please try again later
