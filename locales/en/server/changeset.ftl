unique-constraint = has already been taken
validate-required = can't be blank
validate-format = has invalid format
validate-length = { $kind ->
  [min] should be at least
  [max] should be at most
  *[other] should be
  } {$count} { $type ->
    *[string] character(s)
    [binary] byte(s)
    [list] item(s)
  }
validate-string-distance = is too different from "{$str}"
validate-name = does not match Cedula/RUC. Did you mean "{$str}"?
validate-filetype = filetype is not supported

badRequest = Bad request
pageNotFound = Page not found
requestTimeout = Request timeout
payloadTooLarge = Payload too large
unsupportedMediaType = Unsupported media type
validationError = Validation error(s)
internalServerError = Internal server error

authErrorUnauthorized = unauthorized
authErrorForbidden = forbidden
authErrorMissing = missing authentication information
authErrorInvalid = invalid authentication information
authErrorExpired = expired authentication information
authErrorPhoneUnverified = unverified phone number
authErrorKey = invalid API key
authErrorRevokedKey = API key revoked
authErrorResource = cannot access resource
authErrorLocked = resource is locked
authErrorDomain = key not authorized for this action
