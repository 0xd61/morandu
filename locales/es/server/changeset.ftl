unique-constraint = ya ha sido tomada
validate-required = no puede estar en blanco
validate-format = tiene un formato inválido
validate-length =
    { $kind ->
        [min] debería ser como minimo
        [max] debería ser como máximo
       *[other] debería ser
    }{ $type ->
       *[string] caracteres
        [binary] byte(s)
        [list] objeto
    }
validate-string-distance = es demasiado diferente de "{ $str }"
validate-name = no coincide con Cédula/RUC. ¿Querías decir "{ $str }"?
validate-filetype = no se admite el tipo de archivo
badRequest = Mala petición
pageNotFound = Página no encontrada
requestTimeout = Solicitar tiempo de espera
payloadTooLarge = La carga útil es demasiado grande
unsupportedMediaType = Tipo de medio sin soporte
validationError = Error(es) de validación
internalServerError = Error interno del servidor
authErrorUnauthorized = no autorizado
authErrorForbidden = prohibido
authErrorMissing = información de autenticación faltante
authErrorInvalid = Información de autenticación inválida
authErrorExpired = Información de autenticación caducada
authErrorPhoneUnverified = Número de teléfono no verificado
authErrorKey = Clave API inválida
authErrorRevokedKey = Clave API revocada
authErrorResource = No puede acceder al recurso
authErrorLocked = El recurso está bloqueado
authErrorDomain = llave no autorizada para esta acción
