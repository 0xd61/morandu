# Morandu is a brand name and should not be localized.
-morandu-brand = Morandu
-morandu-short-brand = Morandu
-samuu = Samuu S.R.L.
# deprecated
globalError = Algo salió mal. Por favor, inténtelo de nuevo más tarde
navRouteDashboard = Tablero
introHeading = Buscar
introTitle = { -morandu-brand }
introCaseLinkLabel = Denunciar nuevo caso...
searchInputLabel = Buscar Cedula/RUC
searchDashboardLabel = Buscar Cedula/RUC
searchButton = Enviar
resultHeading =
    { $entries ->
        [one] Resultado para { $id }
       *[other] Resultados para { $id }
    }
resultErrorHeading = Error
resultNothingFoundText = Whoa, no encontramos nada. Estará a salvo
resultFoundText =
    { $entries ->
        [one] Ten cuidado, hemos encontrado { $entries } entrada
       *[other] Ten cuidado, hemos encontrado { $entries } entradas
    }
resultCardHeading = Le debe a
resultCardSince = desde { DATETIME($date, month: "short", year: "numeric", day: "2-digit") }
loginHeading = Bienvenido a
loginEmailLabel = Tu correo electrónico
loginPasswordLabel = Tu contraseña
loginSubmitButton = Seguir
loginContactLinkLabel = Contáctenos
loginErrorInvalidEmail = Tu dirección de correo electrónico o contraseña está inválido
loginErrorNotActive = Esta cuenta de usuario está desactivada
pricingMonthlyPeriod = Mensual
pricingTierRequests = Solicitudes ilimitadas
pricingTierCaseSubmission = Sumisión ilimitada de casos
pricingTierTrial = 14 días de prueba
pricingTierBasicSupport = Apoyo básico
pricingTierProSupport = Soporte
pricingTierFeatureRequests = Solicitudes de características
pricingTierFreeTitle = Gratis
pricingTierFreePrice = GRATIS
pricingTierFreeLinkLabel = Registrarse
pricingTierBasicTitle = Básico
pricingTierBasicLinkLabel = Contáctenos
pricingTierProTitle = Profesional
pricingTierProLinkLabel = Contáctenos
submitButton = Guardar
backButton = Atrás
footerOfflineNotification = Actualmente está desconectado. Por favor, espere una funcionalidad limitada
footerConnectionNotification = Actualmente no hay conexión con el servidor. Por favor, espere una funcionalidad limitada
footerUpdateNotification = Una nueva versión está disponible.
footerUpdateRefresh = Por favor actualiza.
footerLinkPricing = Fijación de precios
footerCopyRight = © { -samuu } Todos los derechos reservados.
settingsHeading = Ajustes
settingsUserHeading = Credenciales de acceso
# maybe rename to profileUserEmailTitle
userEmailTitle = Correo electrónico
# maybe rename to profileUserEmailSubtitle
userEmailSubtitle = Esta es su dirección de correo electrónico de inicio de sesión
# maybe rename to profileUserEmailInputPlaceholder
userEmailInputPlaceholder = Por favor, introduzca su dirección de correo electrónico
# maybe rename to profileUserNameTitle
userNameTitle = Nombre de usuario
# maybe rename to profileUserNameInputPlaceholder
userNameInputPlaceholder = Por favor, introduzca su nombre de usuario preferido
# maybe rename to profileUserPasswordTitle
userPasswordTitle = Cambiar Contraseña
# maybe rename to profileUserPasswordInputPlaceholder
userPasswordInputPlaceholder = Por favor, introduzca una contraseña segura
# maybe rename to profileUserPasswordFooter
userPasswordFooter = Utilice una contraseña de al menos { $length } caracteres.
# maybe rename to profileUserDangerHeading
userDangerHeading = Zona peligrosa
# maybe rename to profileUserLogoutTitle
userLogoutTitle = Cerrar sesión
# maybe rename to profileUserLogoutSubtitle
userLogoutSubtitle = Cierra la sesión actual
# maybe rename to profileUserLogoutButton
userLogoutButton = Cerrar sessión
dashboardHeading = Tablero
dashboardPermissionError = Permiso denegado. Por favor, póngase en contacto con su supervisor
dashboardMenuCases = Casos
dashboardMenuMembers = Miembros
dashboardMenuAccount = Cuenta
dashboardActiveCasesHeading = Casos activos
dashboardEmptyCasesText = No hay ningún caso activo. ¡Bien!
dashboardCasesCardInfoHeading = Aún no se ha verificado
dashboardCasesCardResolveButton = hecho
dashboardCasesCardSince = desde { DATETIME($date, month: "long", year: "numeric", day: "2-digit") }
dashboardCasesCardAmount = Le debe ₲ { NUMBER($number) }
dashboardMembersHeading = Miembros
dashboardMembersRole1 = Administrador
dashboardMembersRole2 = Lector
dashboardMembersRoleUpdateSuccess = Papel se ha actualizado
dashboardUserHeading = Ajustes
dashboardAccountHeading = Ajustes
# maybe rename to dashboardSettingsReadOnlyInfo
personReadOnlyInfo = No se puede cambiar, actualmente.
# maybe rename to dashboardSettingsPhoneTitle
personPhoneTitle = Número de teléfono de emergencia
# maybe rename to dashboardSettingsPhoneSubtitle
personPhoneSubtitle = ¿Cómo podemos contactarte si algo sucede?
# maybe rename to dashboardSettingsPhoneInputPlaceholder
personPhoneInputPlaceholder = Por favor, introduzca su número de teléfono
errorRefreshButton = actualizar
submissionHeading = Presentación
submissionTitle = Denunciar nuevo caso
submissionCedulaTitle = Cedula o RUC
submissionCedulaSubtitle = ¿Quién te debe dinero?
submissionCedulaPlaceholder = Introduce Cedula/RUC
submissionNameTitle = Nombre
submissionNameSubtitle = ¿Cuál es el nombre del deudor?
submissionNamePlaceholder = Introduce el nombre del deudor
submissionPhoneTitle = Número de teléfono
submissionPhoneSubtitle = ¿Cuál es el número de teléfono del deudor?
submissionPhonePlaceholder = Introduce el número de teléfono del deudor
submissionSignedAtTitle = Cierre del contrato
submissionSignedAtSubtitle = ¿Cuándo se firmó el contrato?
submissionAmountTitle = Monto
submissionAmountSubtitle = ¿Cuál es el monto?
submissionAmountValue = { NUMBER($number) }
submissionProofTitle = Factura
submissionProofSubtitle = Por favor, suba una imagen de la factura como prueba
submissionProofUploadButton = Subir
submissionProofFooter = imágenes y los documentos PDF son compatibles
submissionLoadingTitle = Creando el caso...
submissionErrorPrefix = Algo inesperado sucedió
submissionConfirmTitle = ¿Este caso está resuelto?
submissionConfirmSubtitle = Resuelva este caso si { $id } ha saldado sus deudas
submissionConfirmInputLabel = Introduzca { $id } para confirmar
errorPermission = Permiso denegado. Por favor, póngase en contacto con su supervisor
errorTimeout = Se solicita tiempo de espera. Por favor, inténtelo de nuevo más tarde
errorGlobal = Algo salió mal. Por favor, inténtelo de nuevo más tarde
