# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
import Config

config :morandu,
  ecto_repos: [MoranduCore.Repo],
  case_verification: true,
  secret: "no_secret",
  store: MoranduCore.Store.Local,
  s3: [
    base_uri: "localhost:9000"
  ],
  acme_contacts: ["dev@samuu.srl"],
  acme_endpoints: [MoranduWeb.Endpoint],
  acme_ca_dir: "https://localhost:14000/dir",
  cert_dir: Path.expand("../apps/morandu/priv/cert", __DIR__)


# Configures the endpoint
config :morandu, MoranduWeb.Endpoint,
  server: true,
  url: [host: "example.own"],
  secret_key_base: "AyIUnCifOHQESOZ1omDdZ6qp3ttCXxzVGK12i8yAwXdVhsAI1Zjgd6LIh6mly9yz",
  render_errors: [view: MoranduWeb.ErrorView, accepts: ~w(html json)],
  pubsub_server: MoranduWeb.PubSub,
  live_view: [
    signing_salt: "xXtavSYWohPzDoZDtORnRi3ZQwTe3uyR6e1Ra/Fa7FgRAgiSlWmhKTroF9b8aoTR"
  ]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

config :rollbax, enabled: false

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
