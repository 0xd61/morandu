import Config

config :morandu,
  secret: System.fetch_env!("MORANDU_SECRET"),
  s3: [
    secret_key: System.fetch_env!("MORANDU_S3_SECRET_KEY"),
    access_key: System.fetch_env!("MORANDU_S3_ACCESS_KEY")
  ],
  acme_ca_dir: System.get_env("MORANDU_ACME_CA", "https://acme-staging-v02.api.letsencrypt.org/directory"),
  cert_dir: System.fetch_env!("MORANDU_CERT_DIR")

config :morandu, MoranduCore.Repo,
  url: System.fetch_env!("MORANDU_DATABASE_URL"),
  pool_size: String.to_integer(System.get_env("POOL_SIZE", "10"))

config :morandu, MoranduWeb.Endpoint,
  secret_key_base: System.fetch_env!("MORANDU_SECRET_KEY_BASE"),
  token_salt: System.fetch_env!("MORANDU_TOKEN_SALT"),
  url: [host: System.fetch_env!("MORANDU_HOSTNAME")],
  live_view: [
    signing_salt: System.fetch_env!("MORANDU_LIVE_SALT")
  ]

config :rollbax,
  access_token: System.fetch_env!("MORANDU_ROLLBAR_ACCESS_TOKEN")
