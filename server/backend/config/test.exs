import Config

config :morandu,
  case_verification: false,
  s3: [
    base_uri: "http://localhost:9000",
    secret_key: "secret_key",
    access_key: "access_key"
  ],
  ruc_backends: [MoranduCore.Commitments.Ruc.Local]

config :bcrypt_elixir, :log_rounds, 1

# Configure your database
config :morandu, MoranduCore.Repo,
  username: "postgres",
  password: "postgres",
  database: "morandu_test",
  hostname: "localhost",
  pool: Ecto.Adapters.SQL.Sandbox

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :morandu, MoranduWeb.Endpoint,
  http: [port: 4002],
  server: false,
  token_salt: "only_for_testing"

# Print only warnings and errors during test
config :logger, level: :warn
