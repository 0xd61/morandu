defmodule Backend.MixProject do
  use Mix.Project

  def project do
    [
      version: tag_version(),
      elixir: "~> 1.9",
      apps_path: "apps",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      aliases: aliases(),
      releases: releases()
    ]
  end

  def hex_version do
    "git rev-parse --short HEAD"
    |> String.to_charlist
    |> :os.cmd
    |> to_string()
    |> String.replace("\n", "")
  end

  def tag_version do
    version = "git log --tags -1 --pretty=format:'%d' | sed -e 's/.*tag: //' -e 's/,.*//' -e 's/).*//'"
    |> String.to_charlist
    |> :os.cmd
    |> to_string()
    |> String.replace("\n", "")

    if version == "" do
      "0.0.0-dev" # fallback if no tag available
    else
      version
    end
  end

  defp releases() do
    [
      morandu: [
        include_executables_for: [:unix],
        applications: [
          morandu: :permanent
        ]
      ]
    ]
  end

  defp aliases do
    [
      setup: ["deps.get", "ecto.setup", &setup_yarn/1],
      "ecto.seed": [
        "run apps/morandu/priv/repo/seeds.#{Mix.env()}.exs"
      ],
      "ecto.setup": ["ecto.create", "ecto.migrate", "ecto.seed"],
      "ecto.reset": ["ecto.drop", "ecto.setup"],
      test: ["ecto.create --quiet", "ecto.migrate", "test"]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:dialyxir, "~> 1.0", only: [:dev], runtime: false}
    ]
  end

  defp setup_yarn(_) do
    cmd("yarn", ["install"], cd: "apps/morandu/assets")
  end

  defp cmd(cmd, args, opts) do
    opts = Keyword.merge([into: IO.stream(:stdio, :line), stderr_to_stdout: true], opts)
    {_, result} = System.cmd(cmd, args, opts)

    if result != 0 do
      raise "Non-zero result (#{result}) from: #{cmd} #{Enum.map_join(args, " ", &inspect/1)}"
    end
  end
end
