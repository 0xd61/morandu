# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     MoranduCore.Repo.insert!(%Morandu.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

num_cases = 1000
num_proc = 5

Backoffice.Accounts.Users.new(%{
  email: "admin@samuu.srl",
  name: "Admin",
  password: "password password password"
})

alias MoranduCore.Repo
alias MoranduCore.Accounts
alias MoranduCore.Commitments

# Do not check ruc and name externally
Application.put_env(:morandu, :ruc_backends, [MoranduCore.Commitments.Ruc.Local])

person =
  Repo.insert!(%Commitments.Person{
    name: "Test Company",
    cedula: "112233",
    ruc: "112233-4",
    phone: "0983123456"
  })

user =
  Repo.insert!(%Accounts.User{
    email: "test@test.co",
    commitments_person_id: person.id,
    permissions: 0xFF
  })

Repo.insert!(%Accounts.Key{
  secret_first: Bcrypt.hash_pwd_salt("password"),
  type: :password,
  user_id: user.id
})

_password =
  Repo.insert!(%Accounts.Key{
    secret_first: Bcrypt.hash_pwd_salt("password"),
    type: :password,
    user_id: user.id
  })

client_info = %{
  used_at: DateTime.utc_now(),
  user_agent: ["Chrome"],
  ip: {127, 0, 0, 1}
}

rng = fn length ->
  (:rand.uniform() * :math.pow(10, length))
  |> Kernel.trunc()
  |> to_string
  |> String.pad_leading(length, "0")
end

create_case = fn ->
  cedula = "#{rng.(6)}"

  {:ok, active_case} =
    Commitments.Cases.submit(user, person, client_info, %{
      cedula_or_ruc: cedula,
      # ruc: "#{cedula}-#{rng.(1)}",
      name: "Person #{cedula}",
      phone: "0#{rng.(9)}",
      proof:
        Path.dirname(__ENV__.file)
        |> Path.join("../../test/morandu_core/proof_test.png")
        |> File.read!()
    })

  Commitments.Cases.verify!(user, client_info, active_case)

  if rem(String.to_integer(rng.(5)), 7) == 0,
    do: Commitments.Cases.resolve!(user, client_info, active_case)
end


{:ok, c} =
  Commitments.Cases.submit(user, person, client_info, %{
    cedula_or_ruc: "234567-0",
    name: "Person 234567",
    phone: "0#{rng.(9)}",
    proof:
      Path.dirname(__ENV__.file)
      |> Path.join("../../test/morandu_core/proof_test.png")
      |> File.read!()
  })

Commitments.Cases.verify!(user, client_info, c)

# Create cases until done or until an error happens
max_per_process = (num_cases / num_proc) |> Kernel.trunc()
tasks =
  Enum.map(1..num_proc, fn _i ->
    Task.async(fn ->
      for _i <- 1..max_per_process do
        create_case.()
      end
    end)
  end)

Task.yield_many(tasks, :infinity)



