defmodule MoranduCore.Repo.Migrations.AddUserPermissionsAndActiveFlag do
  use Ecto.Migration
  use MoranduCore.Permission.Actions

  def up do
    alter table(:users) do
      #add :active, :boolean
      add :permissions, :integer
    end

    flush()

    MoranduCore.Repo.update_all("users",
      #set: [active: true, permissions: Actions.read ||| Actions.edit]
      set: [permissions: Actions.read ||| Actions.edit ||| Actions.create ||| Actions.delete]
    )

    alter table(:users) do
      #modify :active, :boolean, null: false
      modify :permissions, :integer, null: false
    end
  end

  def down do
    alter table(:users) do
      #remove :active
      remove :permissions
    end
  end
end
