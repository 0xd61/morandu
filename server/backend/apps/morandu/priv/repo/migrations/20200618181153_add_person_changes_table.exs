defmodule MoranduCore.Repo.Migrations.AddPersonChangesTable do
  use Ecto.Migration

  def change do
    create table(:person_changes) do
      add(:value, :binary, null: false)
      add(:new_value, :binary)
      add(:type, :string, null: false)
      add(:field, :string, null: false)

      add(:person_id, references(:persons, on_delete: :delete_all), null: false)
      timestamps(updated_at: false)
    end
  end
end
