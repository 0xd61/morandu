defmodule MoranduCore.Repo.Migrations.AddAuditsTable do
  use Ecto.Migration

  def change do
    create table(:audits) do
      add(:action, :string)
      add(:params, :map)
      add(:accounts_user_id, :id)
      add(:client_info, :map)

      timestamps(updated_at: false)
    end
  end
end
