defmodule MoranduCore.Repo.Migrations.AddTrigramIndexToPersons do
  use Ecto.Migration

  def up() do
    execute("CREATE EXTENSION IF NOT EXISTS pg_trgm")
    execute("CREATE INDEX persons_name_trgm ON persons USING GIN (name gin_trgm_ops)")
  end

  def down() do
    execute("DROP INDEX IF EXISTS persons_name_trgm")
    execute("DROP EXTENSION IF EXISTS pg_trgm")
  end
end
