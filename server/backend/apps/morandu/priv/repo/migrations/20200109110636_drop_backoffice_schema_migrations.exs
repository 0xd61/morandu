defmodule MoranduCore.Repo.Migrations.DropBackofficeSchemaMigrations do
  use Ecto.Migration
  @prefix "backoffice"

  def change do
    drop_if_exists table(:schema_migrations, prefix: @prefix)
  end
end
