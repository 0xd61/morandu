defmodule MoranduCore.Repo.Migrations.AddBackofficeUsersTable do
  use Ecto.Migration
  @prefix "backoffice"

  def change do
    create_if_not_exists table(:users, prefix: @prefix) do
      add(:email, :string, null: false)
      add(:name, :string)
      add(:password_hash, :string, null: false)
      timestamps()
    end

    create_if_not_exists unique_index(:users, [:email], prefix: @prefix)
  end
end
