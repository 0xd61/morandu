defmodule MoranduCore.Repo.Migrations.RemovePersonConstraints do
  use Ecto.Migration

  def change do
    alter table(:persons) do
      # NOTE(dgl): We allow persons without name to make upvoting easier.
      modify(:name, :string, null: true)
    end
  end
end
