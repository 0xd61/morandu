defmodule MoranduCore.Repo.Migrations.AddCasesTable do
  use Ecto.Migration

  def change do
    create table(:cases) do
      add(:resolved_at, :utc_datetime_usec)
      add(:signed_at, :date, null: false)
      add(:amount, :float)
      add(:verified_at, :utc_datetime_usec)

      add(:debtor_id, references(:persons, on_delete: :delete_all), null: false)
      add(:creditor_id, references(:persons, on_delete: :nilify_all))

      timestamps()
    end
  end
end
