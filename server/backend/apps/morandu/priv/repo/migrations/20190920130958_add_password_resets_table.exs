defmodule MoranduCore.Repo.Migrations.AddPasswordResetTable do
  use Ecto.Migration

  def change do
    create table(:password_resets) do
      add(:key, :string, null: false)
      add(:email, :string, null: false)
      add(:user_id, references(:users, on_delete: :delete_all), null: false)
      timestamps(updated_at: false)
    end

    create(unique_index(:password_resets, [:key]))
  end
end
