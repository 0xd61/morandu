defmodule MoranduCore.Repo.Migrations.AddPersonAttributesTable do
  use Ecto.Migration

  def change do
    create table(:person_attributes) do
      add(:value, :binary)
      add(:type, :string, null: false)

      add(:person_id, references(:persons, on_delete: :delete_all), null: false)
      add(:set_by_id, references(:persons, on_delete: :delete_all), null: false)

      timestamps(updated_at: false)
    end
  end
end
