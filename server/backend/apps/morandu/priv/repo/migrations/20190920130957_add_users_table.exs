defmodule MoranduCore.Repo.Migrations.AddUsersTable do
  use Ecto.Migration

  def change do
    create table(:users) do
      add(:email, :string, null: false)
      add(:username, :string)
      add(:commitments_person_id, :id, null: false)
      timestamps()
    end

    create(unique_index(:users, [:email]))
  end
end
