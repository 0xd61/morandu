defmodule MoranduCore.Repo.Migrations.UpdatePersonFields do
  use Ecto.Migration

  def up do
    alter table(:persons) do
      add :is_verified, :boolean, default: false, null: false
    end

    flush()

    MoranduCore.Repo.update_all("persons",
      set: [is_verified: true]
    )
  end

  def down do
    alter table(:persons) do
      remove :is_verified
    end
  end
end
