defmodule MoranduCore.Repo.Migrations.AddProofToCases do
  use Ecto.Migration

  def change do
    alter table(:cases) do
      add :proof, :string
      add :proof_hash, :string
    end

    flush()

    MoranduCore.Repo.update_all("cases",
      set: [proof: "does_not_exist", proof_hash: "md5_205d3936a7be2837413cffb97317dee3"]
    )

    alter table(:cases) do
      modify :proof, :string, null: false
      modify :proof_hash, :string, null: false
    end
  end
end
