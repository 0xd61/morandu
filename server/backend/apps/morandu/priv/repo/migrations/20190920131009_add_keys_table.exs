defmodule MoranduCore.Repo.Migrations.AddKeysTable do
  use Ecto.Migration

  def change do
    create table(:keys) do
      add(:secret_first, :string)
      add(:secret_second, :string)
      add(:revoke_at, :utc_datetime_usec)
      add(:revoked_at, :utc_datetime_usec)
      add(:last_use, :map)
      add(:user_id, references(:users, on_delete: :delete_all), null: false)
      add(:type, :string, null: false)

      timestamps()
    end
  end
end
