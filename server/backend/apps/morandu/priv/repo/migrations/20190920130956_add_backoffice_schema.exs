defmodule MoranduCore.Repo.Migrations.AddBackofficeSchema do
  use Ecto.Migration

  def up do
    execute("CREATE SCHEMA IF NOT EXISTS backoffice")
  end

  def down do
    execute("DROP SCHEMA backoffice")
  end
end
