defmodule MoranduCore.Repo.Migrations.AddUserModToAudits do
  use Ecto.Migration

  def change do
    rename(table("audits"), :accounts_user_id, to: :user_id)

    alter table("audits") do
      add(:user_module, :string)
    end
  end
end
