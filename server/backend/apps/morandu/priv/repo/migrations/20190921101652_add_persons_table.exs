defmodule MoranduCore.Repo.Migrations.AddPersonsTable do
  use Ecto.Migration

  def change do
    create table(:persons) do
      add(:cedula, :string, null: false)
      add(:ruc, :string)
      add(:name, :string, null: false)
      add(:phone, :string)

      timestamps()
    end

    create(unique_index(:persons, [:cedula]))
  end
end
