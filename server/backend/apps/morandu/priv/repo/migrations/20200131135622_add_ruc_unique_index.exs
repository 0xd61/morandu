defmodule MoranduCore.Repo.Migrations.AddRucUniqueIndex do
  use Ecto.Migration

  def change do
    create(unique_index(:persons, [:ruc]))
  end
end
