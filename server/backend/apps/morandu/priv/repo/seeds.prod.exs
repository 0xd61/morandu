# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     MoranduCore.Repo.insert!(%Morandu.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

Backoffice.Accounts.Users.new(%{
  email: System.fetch_env!("MORANDU_BO_EMAIL"),
  name: "Admin",
  # Min 20 Characters
  password: System.fetch_env!("MORANDU_BO_PASSWORD")
})
