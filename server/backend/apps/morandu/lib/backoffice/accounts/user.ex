defmodule Backoffice.Accounts.User do
  use MoranduCore, :schema

  @schema_prefix "backoffice"
  schema "users" do
    field(:email, :string)
    field(:name, :string)
    field(:password, :string, virtual: true)
    field(:password_hash, :string)

    timestamps()
  end

  @attrs ~w(name password)a
  @required ~w(email)a

  @doc false
  def register_changeset(user, attrs) do
    user
    |> changeset(attrs)
    |> validate_required(~w(password)a)
  end

  @doc false
  def changeset(user, attrs) do
    user
    |> cast(attrs, @attrs ++ @required)
    |> validate_required(@required)
    |> validate_email(:email)
    |> unique_constraint(:email)
    |> validate_length(:password, min: 20)
    |> put_pass_hash()
  end

  defp put_pass_hash(changeset) do
    case changeset do
      %Ecto.Changeset{valid?: true, changes: %{password: pass}} ->
        put_change(changeset, :password_hash, Bcrypt.hash_pwd_salt(pass))

      _ ->
        changeset
    end
  end
end
