defmodule Backoffice.Accounts.Users do
  use MoranduCore, :context
  alias Backoffice.Accounts.User

  def new(attrs \\ %{}) do
    %User{}
    |> User.register_changeset(attrs)
    |> Repo.insert()
  end

  def get!(id, preload \\ []) do
    Repo.get!(User, id)
    |> Repo.preload(preload)
  end

  def get_by!(params, preload \\ []) do
    Repo.get_by!(User, params)
    |> Repo.preload(preload)
  end

  def get_by(params, preload \\ []) do
    Repo.get_by(User, params)
    |> Repo.preload(preload)
  end

  def authenticate_by_email_and_pass(email, password) do
    user = get_by(email: email)

    cond do
      user && Bcrypt.verify_pass(password, user.password_hash) ->
        {:ok, user}

      user ->
        {:error, :unauthorized}

      true ->
        Bcrypt.no_user_verify()
        {:error, :not_found}
    end
  end

  def list(preload \\ []) do
    Repo.all(User)
    |> Repo.preload(preload)
  end

  def change(%User{} = user) do
    User.changeset(user, %{})
  end

  def update(%User{} = user, attrs) do
    user
    |> User.changeset(attrs)
    |> Repo.update()
  end

  def delete(%User{} = user) do
    Repo.delete(user)
  end
end
