defmodule MoranduWeb.ControllerHelpers do
  import Plug.Conn
  import Phoenix.Controller
  alias MoranduWeb.Fluex

  def render_error(conn, status, assigns \\ []) do
    conn
    |> put_status(status)
    |> put_layout(false)
    |> put_view(MoranduWeb.ErrorView)
    |> render(:"#{status}", assigns)
    |> halt()
  end

  def changeset_error(conn, %Ecto.Changeset{} = changeset) do
    errors = traverse_errors(changeset)
    render_error(conn, 422, form: errors)
  end

  defp traverse_errors(changeset) do
    Ecto.Changeset.traverse_errors(changeset, fn {msg, opts} ->
      case Fluex.translate(msg, Map.new(opts)) do
        {:ok, translation} ->
          translation

        {:error, _} ->
          Enum.reduce(opts, msg, fn {key, value}, acc ->
            String.replace(acc, "%{#{key}}", to_string(value))
          end)
      end
    end)
  end
end
