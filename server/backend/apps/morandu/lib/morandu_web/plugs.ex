defmodule MoranduWeb.Plugs do
  import Plug.Conn
  alias MoranduWeb.AuthHelpers, as: Auth

  def user_agent(conn, _opts) do
    case get_req_header(conn, "user-agent") do
      [value | _] ->
        assign(conn, :user_agent, value)

      [] ->
        assign(conn, :user_agent, "missing")
    end
  end

  def login_session(conn, opts) do
    user_id = get_session(conn, :user_id)
    fun = opts[:user_fun]

    cond do
      user = conn.assigns[:current_user] ->
        assign(conn, :current_user, user)

      user = user_id && fun.(user_id) ->
        assign(conn, :current_user, user)

      true ->
        assign(conn, :current_user, nil)
    end
  end

  def authenticate(conn, opts) do
    case Auth.authenticate(conn, opts) do
      {:ok, %{key: key, user: user}} ->
        conn
        |> assign(:key, key)
        |> assign(:current_user, user)

      {:ok, %{user: user}} ->
        conn
        |> assign(:key, nil)
        |> assign(:current_user, user)

      {:error, :missing} ->
        conn
        |> assign(:key, nil)
        |> assign(:current_user, nil)

      {:error, _} = error ->
        Auth.error(conn, error)
    end
  end

  def authorize(conn, opts \\ []) do
    user = conn.assigns[:current_user]

    if user do
      Auth.authorize(conn, user, opts)
    else
      Auth.error(conn, {:error, :missing})
    end
  end

  def localize(conn, opts \\ []) do
    locale = conn.params["lang"] || Keyword.get(opts, :default, "en")

    Fluex.put_locale(MoranduWeb.Fluex, locale)

    conn
    |> assign(:locale, locale)
  end
end
