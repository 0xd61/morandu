defmodule MoranduWeb.ErrorHelpers do
  @moduledoc """
  Conveniences for translating and building error messages.
  """

  use Phoenix.HTML

  @doc """
  Generates tag for inlined form input errors.
  """
  def error_tag(form, field) do
    Enum.map(Keyword.get_values(form.errors, field), fn error ->
      content_tag(:span, translate_error(error), class: "help-block")
    end)
  end

  @doc """
  Translates an error message using gettext.
  """
  def translate_error({msg, opts}) do
    # When using gettext, we typically pass the strings we want
    # to translate as a static argument:
    #
    #     # Translate "is invalid" in the "errors" domain
    #     dgettext("errors", "is invalid")
    #
    #     # Translate the number of files with plural rules
    #     dngettext("errors", "1 file", "%{count} files", count)
    #
    # Because the error messages we show in our forms and APIs
    # are defined inside Ecto, we need to translate them dynamically.
    # This requires us to call the Gettext module passing our gettext
    # backend as first argument.
    #
    # Note we use the "errors" domain, which means translations
    # should be written to the errors.po file. The :count option is
    # set by Ecto and indicates we should also apply plural rules.
    if count = opts[:count] do
      Gettext.dngettext(MoranduWeb.Gettext, "errors", msg, msg, count, opts)
    else
      Gettext.dgettext(MoranduWeb.Gettext, "errors", msg, opts)
    end
  end
end

defmodule MoranduWeb.ErrorView do
  use MoranduWeb, :view
  alias MoranduWeb.Fluex, as: Locale

  def render(<<status::binary-3>> <> ".html", assigns) when status !== "err" do
    render(
      "error.html",
      conn: assigns.conn,
      error: true,
      status: status,
      message: assigns[:message] || message(status),
      code: assigns[:code]
    )
  end

  def render(<<status::binary-3>> <> _, assigns) when status !== "err" do
    error =
      assigns
      |> Map.take([:code, :message, :form])
      |> Map.put(:status, String.to_integer(status))
      |> Map.put_new(:message, message(status))
      |> Map.put_new(:locale, Fluex.get_locale(Locale))

    %{error: error}
  end

  # In case no render clause matches or no
  # template is found, let's render it as 500
  def template_not_found(_template, assigns) do
    render(
      "error.html",
      conn: assigns.conn,
      error: true,
      status: "500",
      message: message("500")
    )
  end

  defp message("400"), do: Locale.translate!("badRequest")
  defp message("404"), do: Locale.translate!("pageNotFound")
  defp message("408"), do: Locale.translate!("requestTimeout")
  defp message("413"), do: Locale.translate!("payloadTooLarge")
  defp message("415"), do: Locale.translate!("unsupportedMediaType")
  defp message("422"), do: Locale.translate!("validationError")
  defp message("423"), do: Locale.translate!("authErrorLocked")
  defp message("500"), do: Locale.translate!("internalServerError")
  defp message(_), do: nil
end
