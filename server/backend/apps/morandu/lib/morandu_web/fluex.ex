defmodule MoranduWeb.Fluex do
  use Fluex,
    otp_app: :morandu,
    dir: "../../../../locales",
    resources: ~w(server/changeset.ftl)
end
