defmodule MoranduWeb.Api.V1.ChannelController do
  use MoranduWeb, :controller
  alias MoranduWeb.AuthHelpers

  # NOTE: User can only connect to channel, if he has read access
  plug :authorize,
       [action: :read, funs: &AuthHelpers.access/4]
       when action in [:create]

  def create(conn, _params) do
    render(conn, "create.json", token: build_token(conn))
  end

  defp build_token(conn) do
    %{id: user_id} = conn.assigns.current_user
    Phoenix.Token.sign(conn, token_salt(), user_id)
  end

  defp token_salt, do: Application.get_env(:morandu, MoranduWeb.Endpoint)[:token_salt]
end

defmodule MoranduWeb.Api.V1.ChannelView do
  use MoranduWeb, :view

  def render("create.json", %{token: token}) do
    %{token: token}
  end
end
