defmodule MoranduWeb.Api.V1.AuthController do
  use MoranduWeb, :controller
  alias MoranduCore.Accounts.{Keys, Auth}
  alias MoranduWeb.AuthHelpers

  plug :authorize when action in [:delete]

  def create(conn, %{"params" => %{"email" => email, "password" => password}}) do
    case Auth.via_password(email, password, AuthHelpers.usage_info(conn)) do
      {:ok, %{user: user}} ->
        {:ok, key} = Keys.new(user)
        render(conn, "create.json", user: user, key: key)

      {:error, :revoked} ->
        # TODO: Use key revoked or permissions set to 0?
        render_error(conn, 423)

      {:error, _} ->
        AuthHelpers.error(conn, {:error, :invalid})
    end
  end

  def delete(conn, _params) do
    key = conn.assigns.key

    Keys.revoke(key)
    render(conn, "delete.json")
  end
end

defmodule MoranduWeb.Api.V1.AuthView do
  use MoranduWeb, :view
  alias MoranduWeb.Api.V1.UserView

  def render("create.json", %{user: user, key: key}) do
    %{user: render_one(user, UserView, "user.json"), token: key.user_secret}
  end

  def render("delete.json", _params) do
    %{success: true}
  end
end
