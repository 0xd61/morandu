defmodule MoranduWeb.Api.V1.UserHelpers do
  defmacro __using__(_) do
    quote do
      import MoranduCore.Permission.Actions
      import MoranduWeb.Api.V1.UserHelpers
      import Bitwise, only: [&&&: 2, |||: 2]
      # Roles
      @admin read() ||| edit() ||| create() ||| delete()
      @reader read()

      def role_from_permission(perm) when (perm &&& @admin) == @admin, do: "admin"
      def role_from_permission(perm) when (perm &&& @reader) == @reader, do: "reader"
      def role_from_permission(perm), do: "custom"

      def permission_from_role("admin"), do: {:ok, @admin}
      def permission_from_role("reader"), do: {:ok, @reader}
      def permission_from_role(_role), do: :error
    end
  end
end

defmodule MoranduWeb.Api.V1.UserController do
  use MoranduWeb, :controller
  use MoranduWeb.Api.V1.UserHelpers
  alias MoranduCore.Accounts.Users
  alias MoranduWeb.AuthHelpers
  @resource_id :user

  plug :fetch_user

  plug :authorize,
       [action: :read, funs: &AuthHelpers.access/4]
       when action in [:index]

  plug :authorize,
       [action: :read, funs: &AuthHelpers.resource_access/4, resource: @resource_id]
       when action in [:show]

  plug :authorize,
       [action: :edit, funs: &AuthHelpers.resource_access/4, resource: @resource_id]
       when action in [:update, :permission]

  def index(conn, _params) do
    %{commitments_person_id: id} = conn.assigns.current_user
    render(conn, "index.json", %{users: Users.list_by(commitments_person_id: id)})
  end

  def show(conn, _params) do
    render(conn, "show.json", %{user: conn.assigns.user})
  end

  def update(conn, %{"params" => user_params}) do
    case Users.update(conn.assigns[@resource_id], user_params) do
      {:ok, user} ->
        render(conn, "update.json", %{user: user})

      {:error, %Ecto.Changeset{} = changeset} ->
        changeset_error(conn, changeset)
    end
  end

  def permission(conn, %{"params" => %{"role" => role}}) do
    authenticated = conn.assigns.current_user
    user = conn.assigns[@resource_id]
    # NOTE: We need to check again if a user has "admin" permissions because
    # the normal authorization grants full access if the user ties to edit
    # his own user account. It should only be possible for administrators so set
    # permissions. Maybe we need to change the authorization in the future.
    with "admin" <- role_from_permission(authenticated.permissions),
         {:ok, perm_flag} <- permission_from_role(role) do
      user = Users.set_role(user, perm_flag)
      render(conn, "update.json", %{user: user})
    else
      :error ->
        render_error(conn, 400)
      _ ->
        AuthHelpers.error(conn, {:error, :resource})
    end
  end

  def fetch_user(conn, _opts) do
    if param = conn.params["user"] do
      user = Users.get_by([id: param], :password_hash)
      assign(conn, @resource_id, user)
    else
      assign(conn, @resource_id, nil)
    end
  end
end

defmodule MoranduWeb.Api.V1.UserView do
  use MoranduWeb, :view
  use MoranduWeb.Api.V1.UserHelpers
  alias MoranduCore.Accounts.User
  @derive Jason.Encoder
  defstruct [:id, :email, :username, :role]

  def render("index.json", %{users: users}) do
    render_many(users, __MODULE__, "user.json")
  end

  def render("show.json", %{user: user}) do
    render_one(user, __MODULE__, "user.json")
  end

  def render("update.json", %{user: user}) do
    render_one(user, __MODULE__, "user.json")
  end

  def render("user.json", %{user: %User{} = user}) do
    attrs =
      user
      |> Map.from_struct()
      |> Map.put(:role, role_from_permission(user.permissions))

    struct(__MODULE__, attrs)
  end
end
