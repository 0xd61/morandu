defmodule MoranduWeb.Api.V1.LookupController do
  use MoranduWeb, :controller
  alias MoranduCore.Commitments.Persons
  alias MoranduCore.Commitments.Ruc
  alias MoranduWeb.AuthHelpers

  plug :authorize,
       [action: :read, funs: &AuthHelpers.access/4]
       when action in [:show]

  def show(conn, %{"cedula_or_ruc" => id}) do
    search_key =
      if Ruc.is_valid?(id) do
        :ruc
      else
        :cedula
      end

    cases =
      case Persons.get_by([{search_key, id}], debts: [:debtor, :creditor]) do
        %{debts: cases} -> cases
        _ -> []
      end

    render(conn, "show.json", %{cases: cases})
  end
end

defmodule MoranduWeb.Api.V1.LookupView do
  use MoranduWeb, :view
  alias MoranduWeb.Api.V1.CaseView

  def render("show.json", %{cases: cases}) do
    render_many(cases, CaseView, "pub_case.json")
  end
end
