defmodule MoranduWeb.Api.V1.CaseController do
  use MoranduWeb, :controller
  alias MoranduCore.Commitments.{Cases, Persons}
  alias MoranduCore.Commitments.SubmissionWorkflow, as: Submission
  alias MoranduWeb.SyncChannel, as: Sync
  alias MoranduWeb.AuthHelpers
  @case_id :case
  @person_id :person


  plug :fetch_case

  plug :authorize,
       [action: :read, funs: &AuthHelpers.access/4]
       when action in [:index]

  plug :authorize,
       [action: :create, funs: &AuthHelpers.access/4]
       when action in [:create]

  plug :authorize,
       [action: :delete, funs: &AuthHelpers.resource_access/4, resource: @case_id]
       when action in [:resolve]

  plug :fetch_person

  def index(conn, _params) do
    # TODO: For future make general list_by function to list by active,
    # resolved, verified cases
    cases =
      Cases.list_unresolved_by([creditor_id: conn.assigns[@person_id].id], [:creditor, :debtor])

    render(conn, "index.json", cases: cases)
  end

  def create(conn, %{"params" => case_params}) do
    proof =
      (case_params["proof"] || "")
      |> String.split(",")
      |> List.last()
      |> Base.decode64!()

    params = %{case_params | "proof" => proof}
    info = AuthHelpers.usage_info(conn)
    user = conn.assigns.current_user
    person = conn.assigns[@person_id]

    case Submission.run(user, person, info, params) do
      {:ok, case_} ->
        render(conn, "create.json", %{case: case_})

      {:error, %Ecto.Changeset{} = changeset} ->
        changeset_error(conn, changeset)
    end
  end

  def resolve(conn, _params) do
    info = AuthHelpers.usage_info(conn)
    user = conn.assigns.current_user
    case_ = conn.assigns[@case_id]

    resolved = Cases.resolve!(user, info, case_)

    conn
    |> Sync.broadcast(resolved, resolved.current_audit_id)
    |> render("resolve.json", case: resolved)
  end

  defp fetch_case(conn, _opts) do
    if param = conn.params["case"] do
      current_case = Cases.get(param)
      assign(conn, @case_id, current_case)
    else
      assign(conn, @case_id, nil)
    end
  end

  defp fetch_person(conn, _opts) do
    assign(conn, @person_id, Persons.get_by!(id: conn.assigns.current_user.commitments_person_id))
  end
end

defmodule MoranduWeb.Api.V1.CaseView do
  use MoranduWeb, :view
  alias MoranduCore.Commitments.Case
  @derive Jason.Encoder

  # TODO: only store ID and sync debtor/creditor separate
  defstruct [
    :id,
    :signed_at,
    :amount,
    :verified_at,
    :c_name,
    :c_ruc,
    :c_cedula,
    :d_name,
    :d_cedula,
    :d_ruc
  ]

  def render("index.json", %{cases: cases}) do
    render_many(cases, __MODULE__, "case.json")
  end

  def render("create.json", %{case: case_}) do
    render_one(case_, __MODULE__, "case.json")
  end

  def render("resolve.json", _params) do
    %{success: true}
  end

  @doc """
    Remove entries for more privacy
  """
  def render("pub_case.json", %{case: %Case{} = case_}) do
    render("case.json", %{case: case_})
    |> Map.put(:amount, "[hidden]")
  end

  def render("case.json", %{case: %Case{} = current_case}) do
    attrs =
      Map.from_struct(current_case)
      # NOTE: will be probably replaced by a debtor and creditor object
      |> Map.put(:c_name, current_case.creditor.name)
      |> Map.put(:c_cedula, current_case.creditor.cedula)
      |> Map.put(:c_ruc, current_case.creditor.ruc)
      |> Map.put(:d_name, current_case.debtor.name)
      |> Map.put(:d_cedula, current_case.debtor.cedula)
      |> Map.put(:d_ruc, current_case.debtor.ruc)

    struct(__MODULE__, attrs)
  end
end
