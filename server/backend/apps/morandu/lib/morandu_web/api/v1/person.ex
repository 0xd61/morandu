defmodule MoranduWeb.Api.V1.PersonController do
  use MoranduWeb, :controller
  alias MoranduCore.Commitments.{Person, PersonAttribute}
  alias MoranduCore.Commitments.Persons
  alias MoranduWeb.AuthHelpers

  plug(:authorize, [action: :read, funs: &AuthHelpers.access/4] when action in [:index, :show])
  plug(:authorize, [action: :create, funs: &AuthHelpers.access/4] when action in [:vote])
  plug(:authorize, [action: :delete, funs: &AuthHelpers.access/4] when action in [:delete_vote])

  def action(conn, _) do
    %{commitments_person_id: person_id} = conn.assigns.current_user
    person = Persons.get_by!(id: person_id)
    args = [conn, conn.params, person]
    apply(__MODULE__, action_name(conn), args)
  end

  def index(conn, _params, person) do
    render(conn, "index.json", %{person: person})
  end

  def show(conn, %{"person" => cedula_or_ruc}, p_current) do
    # TODO(dgl): use with @@cleanup
    case Persons.get(cedula_or_ruc) do
      nil ->
        render_error(conn, 404)

      p_requested ->
        case Persons.get_attribute_by(
               person_id: p_requested.id,
               set_by_id: p_current.id,
               type: :vote
             ) do
          nil ->
            {score, count} = Persons.get_score(p_requested)
            render(conn, "show.json", %{person: p_requested, score: score, vote_count: count, voted: false})

          %{value: score} ->
            count = Persons.count_attributes_by(person_id: p_requested.id, type: :vote)
            render(conn, "show.json", %{person: p_requested, score: score, vote_count: count, voted: true})
        end
    end
  end

  # TODO(dgl): if there are more person attributes, move this into separate module
  def vote(conn, %{"person" => cedula_or_ruc, "value" => val}, p_current) when is_integer(val) do
    case Persons.vote(p_current, cedula_or_ruc, val) do
      {:ok, vote} -> render(conn, "vote.json", %{attribute: vote})
      :error -> render_error(conn, 400)
    end
  end

  def delete_vote(conn, %{"person" => cedula_or_ruc}, p_current) do
    with %Person{} = p_requested <- Persons.get(cedula_or_ruc),
         %PersonAttribute{} = vote <-
           Persons.get_attribute_by(
             person_id: p_requested.id,
             set_by_id: p_current.id,
             type: :vote
           ),
         {:ok, _vote} = Persons.delete_attribute(vote) do
      render(conn, "vote.json", %{})
    else
      nil ->
        render_error(conn, 404)
    end
  end
end

defmodule MoranduWeb.Api.V1.PersonView do
  use MoranduWeb, :view
  alias MoranduCore.Commitments.Person

  @derive Jason.Encoder
  defstruct [:id, :cedula, :ruc, :name, :phone]

  def render("index.json", %{person: person}) do
    render_one(person, __MODULE__, "person.json")
  end

  def render("update.json", %{person: person}) do
    render_one(person, __MODULE__, "person.json")
  end

  # TODO(dgl): The upvotes should be calculated in the controller. Should these
  # attributes become elements of the person struct?
  # TODO(dgl): Is this the right place, because we also have the lookup controller?
  # However the lookup controller checks if there is a case. Here we return person info
  def render("show.json", %{person: person, score: score, voted: voted, vote_count: count}) do
    render_one(person, __MODULE__, "person.json")
    |> Map.take([:id, :cedula, :ruc])
    |> Map.put(:score, score)
    |> Map.put(:voted, voted)
    |> Map.put(:vote_count, count)
  end

  def render("vote.json", _params) do
    %{success: true}
  end

  def render("person.json", %{person: %Person{} = person}) do
    attrs = Map.from_struct(person)

    struct(__MODULE__, attrs)
  end
end
