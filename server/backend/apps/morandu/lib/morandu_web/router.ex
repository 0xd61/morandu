defmodule MoranduWeb.Router do
  use MoranduWeb, :router
  use Plug.ErrorHandler
  import Phoenix.LiveDashboard.Router
  import Plug.BasicAuth

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :fetch_live_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug :user_agent
  end

  pipeline :backoffice do
    plug :login_session, user_fun: &Backoffice.Accounts.Users.get!/1
  end

  pipeline :api do
    plug :accepts, ["json"]
    plug :user_agent
    plug :localize
    plug :authenticate
  end

  pipeline :app do
  end

  # NOTE: this is not secure!! This is only to prevent accidental access
  # Therefore we do not case if these credentials are in version control!
  pipeline :admins_only do
    plug :basic_auth, username: "samuu", password: "You sh4ll not p4ss!"
  end

  scope "/backoffice", MoranduWeb.Backoffice, as: :backoffice do
    pipe_through [:browser, :backoffice]

    scope "/dashboard" do
      pipe_through [:admins_only]
      live_dashboard "/", metrics: MoranduCore.Telemetry
    end

    get "/", PageController, :index
    get "/persons", PersonController, :index
    get "/persons/new", PersonController, :new
    post "/persons", PersonController, :create
    get "/persons/export", PersonController, :export_csv
    get "/persons/:person", PersonController, :show
    get "/persons/:person/edit", PersonController, :edit
    put "/persons/:person", PersonController, :update
    get "/users/:person/new", UserController, :new
    post "/users/:person", UserController, :create
    get "/users/:user/edit", UserController, :edit
    put "/users/:user", UserController, :update
    delete "/users/:user", UserController, :delete

    scope "/users/:user", User, as: :user do
      put "/permissions", PermissionController, :update
    end

    delete "/users/:user/tokens/:token", TokenController, :delete
    get "/cases", CaseController, :index
    get "/cases/:creditor/new", CaseController, :new
    post "/cases/:creditor", CaseController, :create
    get "/cases/:case", CaseController, :show
    post "/cases/:case/verify", CaseController, :verify
    post "/cases/:case/resolve", CaseController, :resolve
    get "/cases/:case/proof", CaseController, :proof
    post "/cases/:case/clone", CaseController, :clone

    scope "/m", Mgmt, as: :mgmt do
      get "/session/new", SessionController, :new
      post "/session", SessionController, :create
      delete "/session", SessionController, :delete
      resources "/users", UserController, except: [:show]
    end
  end

  scope "/api", MoranduWeb, as: :api do
    pipe_through :api

    # NOTE: Everything is depending on the user assigned to a person.
    # E.g. does get(/users, :index) only list the users of the same person
    # Explicit access is managed via permissions, like get(/users/:user, :show).
    # Permission handling checks if a user has the permission to access this user
    # (if the user blongs to the same person)

    scope "/v1", Api.V1, as: :v1 do
      # if Mix.env() == :dev do
      #   get "/test", TestController, :index
      # end
      post "/auth", AuthController, :create
      delete "/auth", AuthController, :delete
      post "/channel", ChannelController, :create
      get "/lookup/:cedula_or_ruc", LookupController, :show
      get "/users", UserController, :index
      get "/users/:user", UserController, :show
      post "/users/:user", UserController, :update
      post "/users/:user/role", UserController, :permission
      get "/persons", PersonController, :index
      get "/persons/:person", PersonController, :show
      post "/persons/:person/vote", PersonController, :vote
      delete "/persons/:person/vote", PersonController, :delete_vote
      get "/cases", CaseController, :index
      post "/cases", CaseController, :create
      post "/cases/:case/resolve", CaseController, :resolve
    end
  end

  scope "/", MoranduWeb do
    pipe_through :app
    get "/.well-known/acme-challenge/:token", AcmeController, :index
    get "/*path", AppController, :index
  end

  defp handle_errors(conn, %{kind: kind, reason: reason, stack: stacktrace}) do
    if report?(kind, reason) do
      conn = maybe_fetch_params(conn)
      url = "#{conn.scheme}://#{conn.host}:#{conn.port}#{conn.request_path}"
      user_ip = conn.remote_ip |> :inet.ntoa() |> List.to_string()
      headers = conn.req_headers |> Map.new() |> filter_headers()
      params = filter_params(conn.params)
      endpoint_url = MoranduWeb.Endpoint.config(:url)

      conn_data = %{
        "request" => %{
          "url" => url,
          "user_ip" => user_ip,
          "headers" => headers,
          "params" => params,
          "method" => conn.method
        },
        "server" => %{
          "host" => endpoint_url[:host],
          "root" => endpoint_url[:path]
        }
      }

      Rollbax.report(kind, reason, stacktrace, %{}, conn_data)
    end
  end

  defp report?(:error, exception), do: Plug.Exception.status(exception) == 500
  defp report?(_kind, _reason), do: true

  defp maybe_fetch_params(conn) do
    try do
      Plug.Conn.fetch_query_params(conn)
    rescue
      _ ->
        %{conn | params: "[UNFETCHED]"}
    end
  end

  @filter_headers ~w(authorization cookie)

  defp filter_headers(headers) do
    Map.drop(headers, @filter_headers)
  end

  @filter_params ~w(body password)

  defp filter_params(%Plug.Upload{} = params) do
    filter_params(Map.from_struct(params))
  end

  defp filter_params(params) when is_map(params) do
    Map.new(params, fn {key, value} ->
      if key in @filter_params do
        {key, "[FILTERED]"}
      else
        {key, filter_params(value)}
      end
    end)
  end

  defp filter_params(params) when is_list(params) do
    Enum.map(params, &filter_params/1)
  end

  defp filter_params(other) do
    other
  end

  def list_routes() do
    __MODULE__.__routes__()
    |> Enum.map(&Map.take(&1, [:helper, :path]))
  end
end
