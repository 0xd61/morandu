defmodule MoranduWeb.AppController.StaticApp do
  @moduledoc """
    Serves client app via static pages
  """
  use Plug.Builder

  plug :expand

  plug Plug.Static,
    at: "/",
    gzip: true,
    from: {:morandu, "priv/app"}

  def expand(%{path_info: path} = conn, _) do
    case match?(List.last(path)) do
      true ->
        conn

      false ->
        append_file(conn, "index.html")
    end
  end

  defp match?(nil) do
    false
  end

  defp match?(name) do
    String.match?(name, ~r|\.|)
  end

  defp append_file(conn, file) do
    %{
      conn
      | request_path: Path.join(conn.request_path, file),
        path_info: conn.path_info ++ [file]
    }
  end
end

defmodule MoranduWeb.AppController do
  @moduledoc """
    Pipes through static app plug. This controller renders errors,
    if the static page is not found
  """
  use MoranduWeb, :controller

  plug __MODULE__.StaticApp

  # render HTML for error
  plug :accepts, ["html"]

  def index(conn, _params) do
    render_error(conn, 400)
  end
end
