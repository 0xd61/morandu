defmodule MoranduWeb.Acme.Cache do
  def put(name \\ __MODULE__, key, value) do
    true = :ets.insert(tab_name(name), {key, value})
  end

  def fetch(name \\ __MODULE__, key) do
    {:ok, :ets.lookup_element(tab_name(name), key, 2)}
  rescue
    ArgumentError -> :error
  end

  def new(name \\ __MODULE__) do
    name
    |> tab_name()
    |> :ets.new([
      :set,
      :named_table,
      :public,
      read_concurrency: true,
      write_concurrency: true
    ])
  end

  defp tab_name(name), do: :"#{name}_cache"
end
