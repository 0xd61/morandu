defmodule MoranduWeb.Acme.X509 do
  require X509.ASN1

  def new_csr(private_key, subject) do
    {:ok, key} = X509.PrivateKey.from_pem(private_key)

    X509.CSR.new(key, format_subject(subject),
      extension_request: [
        X509.Certificate.Extension.subject_alt_name(subject.common_name)
      ]
    )
    |> X509.CSR.to_der()
  end

  def seconds_valid(cert) do
    # Our certs can only expire
    cert
    |> X509.Certificate.from_pem!()
    |> X509.Certificate.validity()
    |> X509.ASN1.validity(:notAfter)
    |> X509.DateTime.to_datetime()
    |> DateTime.diff(DateTime.utc_now())
  end

  @subject_keys %{
    common_name: "CN",
    country_name: "C",
    organization_name: "O",
    organizational_unit: "OU",
    state_or_province: "ST",
    locality_name: "L"
  }

  defp format_subject(subject) do
    subject
    |> Enum.map(fn {k, v} ->
      "/#{@subject_keys[k]}=#{v}"
    end)
    |> Enum.join()
  end
end
