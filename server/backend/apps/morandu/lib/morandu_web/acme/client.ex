defmodule MoranduWeb.Acme.Request do
  defstruct [:method, :url, :nonce, :kid, :jwk, :payload]
end

defmodule MoranduWeb.Acme.Client do
  alias MoranduWeb.Acme
  alias MoranduWeb.Acme.Cache
  alias MoranduWeb.Acme.Request

  @mix_env Mix.env()

  # - DONE: Nonce Values can fail. We need some kind of replay
  # - TODO: Need to support multiple domains -> multiple auths per order

  def run(%Acme{} = acme) do
    case acme do
      %{next_task: nil} ->
        acme

      %{next_task: task} ->
        task.(acme) |> run()
    end
  end

  def run(ca, contacts, domains) when is_list(contacts) and is_list(domains) do
    run(%Acme{ca_dir: ca, contacts: contacts, domains: domains, next_task: &new_nonce/1})
  end

  def directory(acme, key) do
    directory(acme)
    |> Map.get(key)
  end

  def directory(acme) do
    req = %Request{
      method: :get,
      url: acme.ca_dir
    }

    {:ok, 200, _header, body} = send_request(req)
    body
  end

  def new_nonce(%Acme{} = acme) do
    req = %Request{
      method: :head,
      url: directory(acme, "newNonce")
    }

    {:ok, 200, %{"Replay-Nonce" => nonce}, _body} = send_request(req)
    %{acme | next_task: &new_account/1, nonce: nonce}
  end

  def new_account(%Acme{} = acme) do
    jwk =
      case Cache.fetch({acme.contacts, :jwk}) do
        {:ok, jwk} ->
          jwk

        :error ->
          jwk = JOSE.JWS.generate_key(%{"alg" => "ES256"})
          Cache.put({acme.contacts, :jwk}, jwk)
          jwk
      end

    req = %Request{
      method: :post,
      url: directory(acme, "newAccount"),
      nonce: acme.nonce,
      payload: %{termsOfServiceAgreed: true, contact: acme.contacts |> Enum.map(&"mailto:#{&1}")},
      jwk: jwk
    }

    case send_request(req) do
      {:ok, status, %{"Replay-Nonce" => nonce, "Location" => kid}, _body}
      when status == 200 or status == 201 ->
        %{acme | next_task: &new_order/1, nonce: nonce, jwk: jwk, kid: kid}

      {:ok, 400, %{"Replay-Nonce" => nonce}, %{"type" => "urn:ietf:params:acme:error:badNonce"}} ->
        %{acme | nonce: nonce}
    end
  end

  def new_order(%Acme{} = acme) do
    domains = Enum.map(acme.domains, &%{type: "dns", value: &1})

    req = %Request{
      method: :post,
      url: directory(acme, "newOrder"),
      kid: acme.kid,
      jwk: acme.jwk,
      nonce: acme.nonce,
      payload: %{identifiers: domains}
    }

    case send_request(req) do
      {:ok, 201, %{"Replay-Nonce" => nonce, "Location" => order}, _body} ->
        %{acme | next_task: &request_order/1, nonce: nonce, order: order}

      {:ok, 400, %{"Replay-Nonce" => nonce}, %{"type" => "urn:ietf:params:acme:error:badNonce"}} ->
        %{acme | nonce: nonce}
    end
  end

  def request_order(%Acme{} = acme) do
    req = %Request{
      method: :post,
      url: acme.order,
      kid: acme.kid,
      jwk: acme.jwk,
      nonce: acme.nonce
    }

    case send_request(req) do
      {:ok, 200, %{"Replay-Nonce" => nonce}, %{"status" => "ready", "finalize" => finalize}} ->
        %{
          acme
          | next_task: &finalize_order/1,
            nonce: nonce,
            follow_url: finalize
        }

      {:ok, 200, %{"Replay-Nonce" => nonce}, %{"status" => "pending", "authorizations" => auths}} ->
        # Run authorizations, wait some time and check if status has changed
        Enum.each(auths, fn auth ->
          run(%{acme | next_task: &authorize/1, nonce: nonce, follow_url: auth})
        end)

        :timer.sleep(2000)
        %{acme | next_task: &request_order/1, nonce: nonce}

      {:ok, 200, %{"Replay-Nonce" => nonce}, %{"status" => "processing"}} ->
        :timer.sleep(5000)
        %{acme | next_task: &request_order/1, nonce: nonce}

      {:ok, 200, %{"Replay-Nonce" => nonce} = header, %{"status" => "invalid"} = body} ->
        # TODO: handle with logger
        IO.inspect(header)
        IO.inspect(body)
        %{acme | next_task: nil, nonce: nonce}

      {:ok, 200, %{"Replay-Nonce" => nonce}, %{"status" => "valid", "certificate" => cert}} ->
        %{acme | next_task: &certificate/1, nonce: nonce, follow_url: cert}

      {:ok, 400, %{"Replay-Nonce" => nonce}, %{"type" => "urn:ietf:params:acme:error:badNonce"}} ->
        %{acme | nonce: nonce}
    end
  end

  def finalize_order(%Acme{} = acme) do
    # Build csr from pem file
    key = X509.PrivateKey.new_ec(:secp256r1) |> X509.PrivateKey.to_pem()
    Cache.put({acme.contacts, :key}, key)

    csr =
      MoranduWeb.Acme.X509.new_csr(key, %{
        common_name: acme.domains,
        organization_name: "Samuu S.R.L",
        organizational_unit: "IT",
        locality_name: "Neu-Halbstadt",
        state_or_province: "Boqueron",
        country_name: "PY"
      })

    req = %Request{
      method: :post,
      url: acme.follow_url,
      kid: acme.kid,
      jwk: acme.jwk,
      nonce: acme.nonce,
      payload: %{csr: Base.url_encode64(csr, padding: false)}
    }

    case send_request(req) do
      {:ok, 200, %{"Replay-Nonce" => nonce}, _body} ->
        %{acme | next_task: &request_order/1, nonce: nonce}

      {:ok, 400, %{"Replay-Nonce" => nonce}, %{"type" => "urn:ietf:params:acme:error:badNonce"}} ->
        %{acme | nonce: nonce}
    end
  end

  def certificate(%Acme{} = acme) do
    req = %Request{
      method: :post,
      url: acme.follow_url,
      kid: acme.kid,
      jwk: acme.jwk,
      nonce: acme.nonce
    }

    case send_request(req) do
      {:ok, 200, %{"Replay-Nonce" => nonce}, cert} ->
        Cache.put({acme.contacts, :cert}, cert)
        %{acme | next_task: nil, nonce: nonce}

      {:ok, 400, %{"Replay-Nonce" => nonce}, %{"type" => "urn:ietf:params:acme:error:badNonce"}} ->
        %{acme | nonce: nonce}
    end
  end

  def authorize(%Acme{} = acme) do
    req = %Request{
      method: :post,
      url: acme.follow_url,
      kid: acme.kid,
      jwk: acme.jwk,
      nonce: acme.nonce
    }

    case send_request(req) do
      {:ok, 200, %{"Replay-Nonce" => nonce}, %{"challenges" => challenges}} ->
        challenges = Enum.group_by(challenges, &Map.fetch!(&1, "type"))

        %{
          acme
          | next_task: &challenge/1,
            nonce: nonce,
            follow_url: Map.fetch!(challenges["http-01"] |> List.first(), "url")
        }

      {:ok, 400, %{"Replay-Nonce" => nonce}, %{"type" => "urn:ietf:params:acme:error:badNonce"}} ->
        %{acme | nonce: nonce}
    end
  end

  def challenge(%Acme{} = acme) do
    req = %Request{
      method: :post,
      url: acme.follow_url,
      kid: acme.kid,
      jwk: acme.jwk,
      nonce: acme.nonce,
      # requires empty payload
      payload: %{}
    }

    case send_request(req) do
      {:ok, 200, %{"Replay-Nonce" => nonce}, _body} ->
        %{acme | next_task: nil, nonce: nonce}

      {:ok, 400, %{"Replay-Nonce" => nonce}, %{"type" => "urn:ietf:params:acme:error:badNonce"}} ->
        %{acme | nonce: nonce}
    end
  end

  def send_request(%Request{} = req, opts \\ []) do
    opts =
      [connect_timeout: 10_000, recv_timeout: 5_000, with_body: true]
      |> Keyword.merge(opts)
      # allow insecure ssl in dev and test env
      |> (&if(@mix_env != :prod, do: Keyword.put_new(&1, :insecure, true), else: &1)).()

    req_header =
      [
        {"User-Agent", "Elixir Acme Client"},
        {"Cache-Control", "no-store"},
        {"Content-Type", "application/jose+json"},
        Keyword.get(opts, :header, [])
      ]
      |> List.flatten()

    {method, body} =
      case req.method do
        :get ->
          {:get, <<>>}

        :head ->
          {:head, <<>>}

        :post ->
          {:post, signed_body(req.jwk, req.payload, build_header(req))}

        :post_as_get ->
          {:get, signed_body(req.jwk, nil, build_header(req))}
      end

    case :hackney.request(method, req.url, req_header, body, opts) do
      {status, code, header, body} when is_binary(body) ->
        body =
          case Jason.decode(body) do
            {:ok, decoded} -> decoded
            {:error, _} -> body
          end

        {status, code, Map.new(header), body}

      {status, code, header, client} ->
        body =
          case :hackney.body(client) do
            {:ok, body} -> Jason.decode!(body)
            _ -> nil
          end

        {status, code, Map.new(header), body}

      {status, code, header} ->
        {status, code, Map.new(header), nil}

      {status, reason} ->
        {status, reason, nil, nil}
    end
  end

  #defp signed_body(jwk, header), do: signed_body(jwk, "", header)

  defp signed_body(jwk, nil, header), do: signed_body(jwk, "", header)

  defp signed_body(jwk, payload, header) do
    encoded_payload = if payload == "", do: "", else: Jason.encode!(payload)

    {_, signed} =
      JOSE.JWS.sign(
        jwk,
        encoded_payload,
        header
      )

    Jason.encode!(signed)
  end

  defp build_header(%Request{kid: nil} = req) do
    %{
      "url" => req.url,
      "nonce" => req.nonce,
      "alg" => Map.get(req.jwk.fields, "alg"),
      "jwk" => JOSE.JWK.to_public_map(req.jwk) |> elem(1)
    }
  end

  defp build_header(%Request{} = req) do
    %{
      "url" => req.url,
      "nonce" => req.nonce,
      "alg" => Map.get(req.jwk.fields, "alg"),
      "kid" => req.kid
    }
  end
end
