defmodule MoranduWeb.AuthHelpers do
  import Plug.Conn
  import MoranduWeb.ControllerHelpers
  alias MoranduCore.Accounts.Auth
  alias MoranduCore.Permission
  alias MoranduWeb.Fluex, as: Locale

  def login(conn, user) do
    conn
    |> assign(:current_user, user)
    |> put_session(:user_id, user.id)
    |> configure_session(renew: true)
  end

  def logout(conn) do
    conn
    |> configure_session(drop: true)
  end

  def usage_info(conn) do
    %{
      used_at: DateTime.utc_now(),
      ip: conn.remote_ip,
      user_agent: conn.assigns.user_agent
    }
  end

  def authenticate(conn, _opts) do
    cond do
      user = conn.assigns[:current_user] ->
        {:ok, %{user: user}}

      key = fetch_key(conn) ->
        key_auth(key, conn)

      true ->
        {:error, :missing}
    end
  end

  defp fetch_key(conn) do
    case get_req_header(conn, "authorization") do
      ["Bearer " <> key] ->
        key

      _ ->
        nil
    end
  end

  defp key_auth(key, conn) do
    case Auth.via_key(key, usage_info(conn)) do
      {:ok, result} -> {:ok, result}
      {:error, :revoked} -> {:error, :revoked_key}
      {:error, _} -> {:error, :key}
    end
  end

  def error(conn, error) do
    case error do
      {:error, :missing} ->
        unauthorized(conn, Locale.translate!("authErrorMissing"))

      {:error, :invalid} ->
        unauthorized(conn, Locale.translate!("authErrorInvalid"))

      {:error, :expired} ->
        unauthorized(conn, Locale.translate!("authErrorExpired"))

      {:error, :phone_unverified} ->
        forbidden(conn, Locale.translate!("authErrorPhoneUnverified"))

      {:error, :key} ->
        unauthorized(conn, Locale.translate!("authErrorKey"))

      {:error, :revoked_key} ->
        unauthorized(conn, Locale.translate!("authErrorRevokedKey"))

      {:error, :resource} ->
        forbidden(conn, Locale.translate!("authErrorResource"))

      {:error, :domain} ->
        forbidden(conn, Locale.translate!("authErrorDomain"))
    end
  end

  def unauthorized(conn, reason) do
    render_error(conn, 401, code: Locale.translate!("authErrorUnauthorized"), message: reason)
  end

  def forbidden(conn, reason) do
    render_error(conn, 403, code: Locale.translate!("authErrorForbidden"), message: reason)
  end

  def authorize(conn, user, opts) do
    funs = Keyword.get(opts, :funs) |> List.wrap()
    resource = Keyword.get(opts, :resource)
    action = Keyword.get(opts, :action)

    cond do
      funs ->
        Enum.find_value(funs, fn fun ->
          case fun.(conn, user, action, resource) do
            true -> nil
            false -> error(conn, {:error, :resource})
          end
        end) || conn

      true ->
        conn
    end
  end

  # NOTE(dgl): Check if user has a specific permission flag set
  # TODO(dgl): This should be enough. Everything else could be handled in the controller
  # for more simplicity @@cleanup
  def access(%Plug.Conn{} = _conn, user, action, nil) do
    Permission.can?(action, user)
  end

  # NOTE(dgl): Check if user has access on other resource
  def resource_access(%Plug.Conn{} = conn, user, action, resource)
      when is_atom(action) and is_atom(resource) do
    case conn.assigns[resource] do
      nil ->
        false

      resource ->
        Permission.can?(action, user, resource)
    end
  end
end
