defmodule MoranduWeb.Acme do
  @enforce_keys [:contacts, :domains, :ca_dir]
  defstruct [:follow_url, :nonce, :jwk, :kid, :order, :contacts, :domains, :ca_dir, :next_task]

  @moduledoc """
    Acme client for lets encrypt ssl certs
  """
  use GenServer
  alias MoranduWeb.Acme.{Client, Cache, X509}

  # TODO: set to specific time
  @renew_interval :timer.minutes(5)

  def get_jwk(name \\ __MODULE__) do
    case GenServer.call(name, {:fetch, :jwk}) do
      {:ok, jwk} -> jwk
      :error -> nil
    end
  end

  def refresh(name \\ __MODULE__) do
    GenServer.cast(name, :refresh)
  end

  # Server
  def start_link(opts) do
    # Wait 10 sec before starting
    #:timer.sleep(10000)
    opts = Keyword.put_new(opts, :name, __MODULE__)
    GenServer.start_link(__MODULE__, opts, name: opts[:name])
  end

  def init(_opts) do
    # Init cache
    Cache.new()

    # Init state
    # TODO: Should this be passed with opts?
    state = %{
      contacts: Application.get_env(:morandu, :acme_contacts),
      endpoints: Application.get_env(:morandu, :acme_endpoints),
      ca: Application.fetch_env!(:morandu, :acme_ca_dir),
      cert_dir: Application.fetch_env!(:morandu, :cert_dir),
      timer: nil
    }

    {:ok, renew_cert(state)}
  end

  def handle_call({:fetch, key}, _from, state) do
    {:reply, Cache.fetch({state.contacts, key}), state}
  end

  def handle_cast(:refresh, state) do
    {:noreply, renew_cert(state)}
  end

  def handle_info(:renew, state) do
    Task.start(fn ->
      path = Path.join([state.cert_dir, Enum.join(state.endpoints, "_")])
      cert_path = Path.join(path, "cert.pem")
      key_path = Path.join(path, "key.pem")

      # Check if key and cert exist and if cert is valid for more than 5 days.
      # Otherwise order a fresh certificate
      with true <- File.exists?(key_path),
           {:ok, cert} <- File.read(cert_path),
           seconds when seconds > 5 * 24 * 60 * 60 <- X509.seconds_valid(cert) do
        :ok
      else
        _ ->
          # create new key and cert
          %{contacts: contacts} =
            Client.run(state.ca, state.contacts, state.endpoints |> Enum.map(& &1.host()))

          # store cert and key
          with {:ok, cert} <- Cache.fetch({contacts, :cert}),
               {:ok, key} <- Cache.fetch({contacts, :key}) do
            File.mkdir_p!(path)
            File.write!(cert_path, cert)
            File.write!(key_path, key)
          else
            _ ->
              # On error kill process and retry
              # TODO: handled by task supervisor
              raise RuntimeError, "could not retrieve cert or key"
          end
      end

      if File.exists?(key_path) and File.exists?(cert_path) do
        # Configure Endpoints
        # TODO: Hotupgrade Endpoint.. somehow change_config does nothing
        Enum.each(state.endpoints, fn endpoint ->
          default_config = Application.get_env(:morandu, endpoint)
          https_port = endpoint.config(:https)[:port] || Keyword.fetch!(default_config, :https) |> Keyword.fetch!(:port)
          config =
            default_config
            |> Keyword.merge(
              # TODO: use config for port
              https: [keyfile: key_path, certfile: cert_path, port: https_port, cipher_suite: :strong]
            )

          :ok = Application.put_env(:morandu, endpoint, config)

          :ok = Supervisor.terminate_child(MoranduCore.Supervisor, endpoint)
          :ok = Supervisor.delete_child(MoranduCore.Supervisor, endpoint)
          {:ok, _pid} = Supervisor.start_child(MoranduCore.Supervisor, endpoint)
        end)
      end
    end)

    {:noreply, renew_cert(state, @renew_interval)}
  end

  defp renew_cert(state, interval \\ :timer.seconds(0)) do
    %{state | timer: Process.send_after(self(), :renew, interval)}
  end
end

defmodule MoranduWeb.AcmeController do
  @moduledoc """
    Controller for calls from acme servers
  """
  use MoranduWeb, :controller
  alias MoranduWeb.Acme

  # Used for http-01 challenge
  def index(conn, %{"token" => token}) do
    IO.inspect(token)
    jwk = Acme.get_jwk()
    keyAuthorization = token <> "." <> JOSE.JWK.thumbprint(jwk)
    send_download(conn, {:binary, keyAuthorization}, filename: token)
  end
end
