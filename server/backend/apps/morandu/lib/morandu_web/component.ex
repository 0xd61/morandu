defmodule MoranduWeb.ComponentHelpers do
  use Phoenix.HTML

  def c(template, assigns) do
    MoranduWeb.ComponentView.render(template, assigns)
  end
end

defmodule MoranduWeb.ComponentView do
  use MoranduWeb, :view

  def get_content(content) do
    case content do
      [] -> nil
      _ -> content
    end
  end
end

