defmodule MoranduWeb.Backoffice.Mgmt.UserController do
  use MoranduWeb, :controller

  alias Backoffice.Accounts.{User, Users}

  plug :authorize

  def index(conn, _params) do
    users = Users.list()
    render(conn, "index.html", users: users)
  end

  def new(conn, _params) do
    changeset = Users.change(%User{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"params" => user_params}) do
    case Users.new(user_params) do
      {:ok, _user} ->
        conn
        |> put_flash(:info, "User created successfully.")
        |> redirect(to: Routes.backoffice_mgmt_user_path(conn, :index))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def edit(conn, %{"id" => id}) do
    user = Users.get!(id)
    changeset = Users.change(user)
    render(conn, "edit.html", user: user, changeset: changeset)
  end

  def update(conn, %{"id" => id, "params" => user_params}) do
    user = Users.get!(id)

    case Users.update(user, user_params) do
      {:ok, _user} ->
        conn
        |> put_flash(:info, "User updated successfully.")
        |> redirect(to: Routes.backoffice_mgmt_user_path(conn, :index))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", user: user, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    user = Users.get!(id)
    {:ok, _user} = Users.delete(user)

    conn
    |> put_flash(:info, "User deleted successfully.")
    |> redirect(to: Routes.backoffice_mgmt_user_path(conn, :index))
  end
end

defmodule MoranduWeb.Backoffice.Mgmt.UserView do
  use MoranduWeb, :view
end
