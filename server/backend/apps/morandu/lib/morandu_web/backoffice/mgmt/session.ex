defmodule MoranduWeb.Backoffice.Mgmt.SessionController do
  use MoranduWeb, :controller
  alias Backoffice.Accounts.Users
  alias MoranduWeb.AuthHelpers

  plug :authorize when action in [:delete]

  def new(conn, _params) do
    render(conn)
  end

  def create(conn, %{"params" => %{"email" => email, "password" => password}}) do
    case Users.authenticate_by_email_and_pass(email, password) do
      {:ok, user} ->
        conn
        |> AuthHelpers.login(user)
        |> put_flash(:info, "Welcome back!")
        |> redirect(to: Routes.backoffice_page_path(conn, :index))

      {:error, _reason} ->
        conn
        |> put_flash(:error, "Invalid email/password combination")
        |> render("new.html")
    end
  end

  def delete(conn, _params) do
    conn
    |> AuthHelpers.logout()
    |> redirect(to: Routes.backoffice_mgmt_session_path(conn, :new))
  end
end

defmodule MoranduWeb.Backoffice.Mgmt.SessionView do
  use MoranduWeb, :view
end
