defmodule MoranduWeb.Backoffice.PageController do
  use MoranduWeb, :controller
  alias MoranduCore.Commitments.Cases

  plug :auth_redirect when action in [:index]

  def index(conn, _params) do
    count = Cases.list_unverified_by() |> length()
    render(conn, "index.html", case_count: count)
  end

  defp auth_redirect(conn, _opts) do
    if conn.assigns[:current_user] do
      conn
    else
      redirect(conn, to: Routes.backoffice_mgmt_session_path(conn, :new))
      |> halt()
    end
  end
end

defmodule MoranduWeb.Backoffice.PageView do
  use MoranduWeb, :view
end
