defmodule MoranduWeb.Backoffice.CaseController do
  use MoranduWeb, :controller
  alias MoranduCore.Commitments.{Cases, Persons, Audits}
  alias MoranduWeb.AuthHelpers
  alias MoranduWeb.SyncChannel, as: Sync

  plug :authorize

  def index(conn, _params) do
    cases = Cases.list_unresolved_by([], [:debtor, :creditor])
    render(conn, "index.html", cases: cases)
  end

  def new(conn, %{"creditor" => id}) do
    changeset = Cases.change()
    creditor = Persons.get_by!(id: id)
    render(conn, "new.html", creditor: creditor, changeset: changeset)
  end

  def create(conn, %{"creditor" => id, "params" => case_params}) do
    creditor = Persons.get_by!(id: id)
    user = conn.assigns.current_user
    info = AuthHelpers.usage_info(conn)

    params =
      case File.read((case_params["proof"] && case_params["proof"].path) || "./does_not_exist") do
        {:ok, file} -> %{case_params | "proof" => file}
        {:error, _} -> case_params
      end

    case Cases.submit(user, creditor, info, params) do
      {:ok, current_case} ->
        conn
        |> put_flash(:info, "Case created successfully.")
        |> redirect(to: Routes.backoffice_case_path(conn, :show, current_case))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", creditor: creditor, changeset: changeset)
    end
  end

  def show(conn, %{"case" => id}) do
    current_case = Cases.get!(id, [:debtor, :creditor])
    render(conn, "show.html", case: current_case)
  end

  def resolve(conn, %{"case" => id, "origin" => origin}) do
    info = AuthHelpers.usage_info(conn)
    current_case = Cases.get!(id)
    user = conn.assigns.current_user

    resolved = Cases.resolve!(user, info, current_case)

    conn
    |> Sync.broadcast(resolved, resolved.current_audit_id)
    |> put_flash(:info, "Case #{id} resolved.")
    |> redirect(to: origin)
  end

  def resolve(conn, %{"case" => id} = params) do
    params = Map.put_new(params, "origin", Routes.backoffice_case_path(conn, :show, id))
    resolve(conn, params)
  end

  def verify(conn, %{"case" => id, "origin" => origin}) do
    info = AuthHelpers.usage_info(conn)
    current_case = Cases.get!(id)
    user = conn.assigns.current_user

    verified = Cases.verify!(user, info, current_case)

    conn
    |> Sync.broadcast(verified, verified.current_audit_id)
    |> put_flash(:info, "Case #{id} verified.")
    |> redirect(to: origin)
  end

  def verify(conn, %{"case" => id} = params) do
    params = Map.put_new(params, "origin", Routes.backoffice_case_path(conn, :show, id))
    verify(conn, params)
  end

  def proof(conn, %{"case" => id}) do
    current_case = Cases.get!(id)

    case Cases.get_proof(current_case) do
      {:ok, file, name} ->
        send_download(conn, {:binary, file}, filename: name)

      {:error, reason} ->
        conn
        |> put_flash(:error, "Error while downloading proof: #{reason}")
        |> redirect(to: Routes.backoffice_case_path(conn, :show, current_case))
    end
  end

  def clone(conn, %{"case" => id}) do
    case_ = Cases.get!(id, [:creditor, :debtor])
    user = conn.assigns.current_user

    info =
      Audits.get_by!(action: "case.submit", params: %{id: id})
      |> Map.get(:client_info)
      |> Map.from_struct()

    # TODO: Only link to existing proof to prevent reuploading
    {:ok, proof, _} = Cases.get_proof(case_)

    params = %{
      cedula_or_ruc: case_.debtor.ruc || case_.debtor.cedula,
      name: case_.debtor.name,
      phone: case_.debtor.phone,
      signed_at: case_.signed_at,
      amount: case_.amount,
      proof: proof
    }

    case Cases.submit(user, case_.creditor, info, params) do
      {:ok, cloned} ->
        conn
        |> put_flash(:info, "Case cloned successfully.")
        |> redirect(to: Routes.backoffice_case_path(conn, :show, cloned))

      {:error, _err} ->
        conn
        |> put_flash(:error, "Failed cloning case. Please try again later.")
        |> redirect(to: Routes.backoffice_case_path(conn, :show, id))
    end
  end
end

defmodule MoranduWeb.Backoffice.CaseView do
  use MoranduWeb, :view
end
