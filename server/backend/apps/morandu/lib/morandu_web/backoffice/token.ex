defmodule MoranduWeb.Backoffice.TokenController do
  use MoranduWeb, :controller
  alias MoranduCore.Accounts.Keys

  plug :authorize

  def delete(conn, %{"user" => user, "token" => id}) do
    key = Keys.get_by!(id: id, user_id: user)

    Keys.revoke(key)

    conn
    |> put_flash(:info, "Key revoked")
    |> redirect(to: Routes.backoffice_user_path(conn, :edit, user))
  end
end
