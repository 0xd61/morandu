defmodule MoranduWeb.Backoffice.UserController do
  use MoranduWeb, :controller

  alias MoranduCore.Accounts.{Users, User}
  alias MoranduCore.Commitments.Persons

  plug :authorize

  def new(conn, %{"person" => person_id}) do
    person = Persons.get_by!(id: person_id)
    changeset = Users.change(%User{})
    render(conn, "new.html", changeset: changeset, person: person)
  end

  def create(conn, %{"person" => person_id, "params" => user_params}) do
    person = Persons.get_by!(id: person_id)

    case Users.new(person, user_params) do
      {:ok, _user} ->
        conn
        |> put_flash(:info, "User created successfully.")
        |> redirect(to: Routes.backoffice_person_path(conn, :show, person))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset, person: person)
    end
  end

  def edit(conn, %{"user" => id}) do
    user = Users.get!(id, :keys)
    changeset = Users.change(user)
    render(conn, "edit.html", user: user, changeset: changeset)
  end

  def update(conn, %{"user" => id, "params" => user_params}) do
    user = Users.get!(id, [:password_hash, :keys])

    case Users.update(user, user_params) do
      {:ok, _user} ->
        conn
        |> put_flash(:info, "User updated successfully.")
        |> redirect(
          to: Routes.backoffice_person_path(conn, :show, user.commitments_person_id)
        )

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", user: user, changeset: changeset)
    end
  end

  def delete(conn, %{"user" => id}) do
    user = Users.get!(id)
    {:ok, _user} = Users.delete(user)

    conn
    |> put_flash(:info, "User deleted successfully.")
    |> redirect(to: Routes.backoffice_person_path(conn, :show, user.commitments_person_id))
  end
end

defmodule MoranduWeb.Backoffice.UserView do
  use MoranduWeb, :view
  alias MoranduCore.Permission

  def can_create?(user) do
    Permission.can?(:create, user)
  end

  def can_read?(user) do
    Permission.can?(:read, user)
  end

  def can_edit?(user) do
    Permission.can?(:edit, user)
  end

  def can_delete?(user) do
    Permission.can?(:delete, user)
  end
end
