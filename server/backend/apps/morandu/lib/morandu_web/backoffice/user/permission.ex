defmodule MoranduWeb.Backoffice.User.PermissionController do
  use MoranduWeb, :controller
  alias MoranduCore.Accounts.Users
  use MoranduCore.Permission.Actions, as: A

  plug :authorize

  def update(conn, %{"user" => id, "params" => permissions}) do
    user = Users.get!(id)

    flags =
      Enum.reduce(permissions, 0, fn
        {"create", "true"}, acc -> acc ||| A.create()
        {"read", "true"}, acc -> acc ||| A.read()
        {"edit", "true"}, acc -> acc ||| A.edit()
        {"delete", "true"}, acc -> acc ||| A.delete()
        _, acc -> acc
      end)

    Users.set_role(user, flags)

    conn
    |> put_flash(:info, "Permissions updated")
    |> redirect(to: Routes.backoffice_user_path(conn, :edit, id))
  end
end
