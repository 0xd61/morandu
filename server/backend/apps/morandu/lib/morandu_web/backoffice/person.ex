defmodule MoranduWeb.Backoffice.PersonController do
  use MoranduWeb, :controller

  alias MoranduCore.Commitments.{Persons, Person}
  alias MoranduCore.Accounts.Users

  plug :authorize

  def index(conn, _params) do
    live_render(conn, MoranduWeb.PersonSearchLive)
  end

  def new(conn, _params) do
    changeset = Persons.change(%Person{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"params" => person_params}) do
    case Persons.new_checked(person_params) do
      {:ok, person} ->
        conn
        |> put_flash(:info, "Person created successfully.")
        |> redirect(to: Routes.backoffice_person_path(conn, :show, person))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"person" => id}) do
    person = Persons.get_by!([id: id], [cases: [:debtor]])
    users = Users.list_by(commitments_person_id: id)
    render(conn, "show.html", person: person, users: users)
  end

  def edit(conn, %{"person" => id}) do
    person = Persons.get_by!(id: id)
    changeset = Persons.change(person)
    render(conn, "edit.html", person: person, changeset: changeset)
  end

  def update(conn, %{"person" => id, "params" => person_params}) do
    person = Persons.get_by!(id: id)

    case Persons.update_checked(person, person_params) do
      {:ok, person} ->
        conn
        |> put_flash(:info, "Person updated successfully.")
        |> redirect(to: Routes.backoffice_person_path(conn, :show, person))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", person: person, changeset: changeset)
    end
  end

  # TODO(dgl): Create delete action, where an admin can delete invalid persons

  def export_csv(conn, _params) do
    # NOTE: Move logic to DB select if too slow for many records @performance
    users_by_person_id =
      Users.list_by([])
      |> Enum.map(&Map.take(&1, [:commitments_person_id, :email, :id]))
      |> Enum.group_by(& &1.commitments_person_id)

    person_ids = Map.keys(users_by_person_id)

    data =
      Persons.list_by_ids(person_ids, :open_cases)
      |> Enum.map(fn person ->
        user_emails =
          users_by_person_id
          |> Map.get(person.id)
          |> Enum.map(& &1.email)

        %{
          "1_id" => person.id,
          "2_cedula" => person.cedula,
          "3_ruc" => person.ruc,
          "4_name" => person.name,
          "5_phone" => person.phone,
          "6_user_count" => length(user_emails),
          "7_emails" => Enum.join(user_emails, " "),
          "8_case_count" => length(person.open_cases)
        }
      end)

    csv = MoranduCore.Utils.build_csv(data)
    send_download(conn, {:binary, csv}, filename: "person_export.csv")
  end
end

defmodule MoranduWeb.PersonSearchLive do
  use MoranduWeb, :live
  alias MoranduCore.Commitments.Persons
  alias MoranduCore.Accounts.Users
  alias MoranduWeb.Backoffice.PersonView

  def render(assigns) do
    Phoenix.View.render(PersonView, "index.html", assigns)
  end

  def mount(_params, _session, socket) do
    {:ok, assign(socket, query: nil, result: nil, loading: false)}
  end

  def handle_event("search", %{"q" => query}, socket) when byte_size(query) <= 100 do
    send(self(), {:search, query})
    {:noreply, assign(socket, query: query, loading: true)}
  end

  def handle_info({:search, query}, socket) do
    result =
      case is_email?(query) do
        true ->
          case Users.get_by(email: query) do
            nil ->
              []

            %{commitments_person_id: id} ->
              Persons.get_by!(id: id)
              |> List.wrap()
          end

        false ->
          Persons.search(query)
      end

    {:noreply, assign(socket, loading: false, result: result)}
  end

  defp is_email?(text) do
    String.match?(
      text,
      ~r|^[A-Za-z0-9._%+-+']+@[A-Za-z0-9.-]+\.[A-Za-z]+$|
    )
  end
end

defmodule MoranduWeb.Backoffice.PersonView do
  use MoranduWeb, :view
end
