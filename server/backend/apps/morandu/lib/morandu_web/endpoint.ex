defmodule MoranduWeb.Endpoint do
  use Phoenix.Endpoint, otp_app: :morandu

  @session_options [
    store: :cookie,
    key: "_morandu_key",
    signing_salt: "+L293q0A"
  ]

  socket "/live", Phoenix.LiveView.Socket, websocket: [connect_info: [session: @session_options]]
  socket "/socket", MoranduWeb.UserSocket

  # Serve at "/backoffice" the static files from "priv/static" directory.
  #
  # You should set gzip to true if you are running phx.digest
  # when deploying your static files in production.
  plug Plug.Static,
    at: "/backoffice",
    from: :morandu,
    gzip: true,
    only: ~w(css fonts images js favicon.ico robots.txt)

  # Code reloading can be explicitly enabled under the
  # :code_reloader configuration of your endpoint.
  if code_reloading? do
    socket "/phoenix/live_reload/socket", Phoenix.LiveReloader.Socket
    plug Phoenix.LiveReloader
    plug Phoenix.CodeReloader
  end

  plug Plug.RequestId
  plug Plug.Telemetry, event_prefix: [:phoenix, :endpoint]

  plug Plug.Parsers,
    # allow max 10MB requests
    # TODO: manually handling json requests for payload too large response
    parsers: [:urlencoded, :multipart, {:json, length: 10_000_000}],
    pass: ["*/*"],
    json_decoder: Phoenix.json_library()

  plug Plug.MethodOverride
  plug Plug.Head

  # Only use Corsica in dev env.
  # In prod we serve the app via static assets
  if Mix.env() == :dev do
    plug Corsica,
      origins: "*",
      allow_headers: ["content-type", "authorization"],
      allow_methods: ["GET", "POST", "DELETE"]
  end

  # The session will be stored in the cookie and signed,
  # this means its contents can be read but not tampered with.
  # Set :encryption_salt if you would also like to encrypt it.
  plug Plug.Session, @session_options

  plug MoranduWeb.Router
end
