defmodule MoranduWeb.SyncChannel do
  use MoranduWeb, :channel
  alias MoranduCore.Commitments.Cases
  alias MoranduWeb.Api.V1.CaseView
  alias Phoenix.View

  def join("sync:lobby", _params, socket) do
    {:ok, socket}
  end

  def handle_in("sync", %{"params" => %{"last_event" => id}}, socket) do
    {cases, audit_id} = Cases.list_changed_since(id, [:creditor, :debtor])

    # return id if audit_id is nil (nothing changed)
    push(socket, "changes", %{changes: render_changes(cases), event: audit_id || id})
    {:noreply, socket}
  end

  # takes cases and puts resolved cases into the delete list
  # everything else into the insert list
  # afterwards these lists will be rendered
  defp render_changes(changes) do
    changes
    |> Enum.group_by(
      &case &1.resolved_at do
        nil -> :insert
        _ -> :delete
      end
    )
    |> Map.put_new(:insert, [])
    |> Map.put_new(:delete, [])
    |> Enum.map(fn {key, val} ->
      {key, View.render_many(val, CaseView, "pub_case.json")}
    end)
    |> Map.new()
  end

  def broadcast(cases, event) when is_list(cases) do
    preloaded = Cases.preload(cases, [:debtor, :creditor])

    MoranduWeb.Endpoint.broadcast("sync:lobby", "changes", %{
      changes: render_changes(preloaded),
      event: event
    })
  end

  def broadcast(current_case, event) do
    broadcast([current_case], event)
  end

  def broadcast(conn, cases, event) do
    :ok = broadcast(cases, event)
    conn
  end
end
