defmodule MoranduWeb do
  @moduledoc """
  The entrypoint for defining your web interface, such
  as controllers, views, channels and so on.

  This can be used in your application as:

      use MoranduWeb, :controller
      use MoranduWeb, :view

  The definitions below will be executed for every view,
  controller, etc, so keep them short and clean, focused
  on imports, uses and aliases.

  Do NOT define functions inside the quoted expressions
  below. Instead, define any helper function in modules
  and import those modules here.
  """

  def controller do
    quote do
      use Phoenix.Controller, namespace: MoranduWeb

      import Plug.Conn
      import MoranduWeb.Gettext
      alias MoranduWeb.Router.Helpers, as: Routes
      import MoranduWeb.ControllerHelpers
      import MoranduWeb.Plugs
      import Phoenix.LiveView.Controller
    end
  end

  def view do
    quote do
      use Phoenix.View,
        root: "lib/morandu_web/_templates",
        namespace: MoranduWeb

      # Import convenience functions from controllers
      import Phoenix.Controller, only: [get_flash: 1, get_flash: 2, view_module: 1]

      # Use all HTML functionality (forms, tags, etc)
      use Phoenix.HTML

      import MoranduWeb.ErrorHelpers
      import MoranduWeb.ComponentHelpers, only: [c: 2]
      import MoranduWeb.Gettext
      alias MoranduWeb.Router.Helpers, as: Routes

      import Phoenix.LiveView.Helpers
    end
  end

  def live do
    quote do
      use Phoenix.LiveView
      use Phoenix.HTML
      alias MoranduWeb.Router.Helpers, as: Routes
      import MoranduWeb.ErrorHelpers
      import MoranduWeb.ComponentHelpers, only: [c: 2]
    end
  end

  def router do
    quote do
      use Phoenix.Router
      import Plug.Conn
      import Phoenix.Controller
      import MoranduWeb.Plugs
      import Phoenix.LiveView.Router
    end
  end

  def channel do
    quote do
      use Phoenix.Channel
      import MoranduWeb.Gettext
    end
  end

  @doc """
  When used, dispatch to the appropriate controller/view/etc.
  """
  defmacro __using__(which) when is_atom(which) do
    apply(__MODULE__, which, [])
  end
end
