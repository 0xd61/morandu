defmodule MoranduCore.Commitments.SubmissionWorkflow do
  use MoranduCore, :context
  alias MoranduCore.Commitments.SubmissionForm, as: Form
  alias MoranduCore.Commitments.{Persons, Case, Cases, Audits, Person}

  def change() do
    Form.changeset(%{})
  end

  def run(user, %Person{} = creditor, client_info, params) do
    with %{valid?: true} = form_changeset <- Form.changeset(params),
         multi <- create_case(creditor, form_changeset.changes),
         audited_multi <- audit(multi, user, client_info),
         {:ok, %{case: current_case}} <- transaction(audited_multi) do
      {:ok, current_case}
    else
      %{valid?: false} = changeset ->
        {:error, %{changeset | action: :insert}}
    end
  end

  defp create_case(%Person{} = creditor, params) do
    Multi.new()
    |> maybe_new_debtor(:debtor, params)
    |> Multi.merge(fn %{debtor: debtor} ->
      Multi.new()
      |> upload_proof(:proof, creditor, debtor, params)
    end)
    |> Multi.insert(:case, fn %{debtor: debtor, proof: %{uri: uri, hash: hash}} ->
      params =
        params
        |> Map.put(:proof, uri)
        |> Map.put_new(:proof_hash, hash)

      Case.build(creditor, debtor, params)
    end)
  end

  defp transaction(multi) do
    case Repo.transaction(multi) do
      {:ok, %{"case.verify": audit, case: current_case} = data} ->
        {:ok, put_in(data, [:case], %{current_case | current_audit_id: audit.id})}

      {:ok, data} ->
        {:ok, data}

      error ->
        error
    end
  end

  def maybe_new_debtor(multi, action, %{cedula: cedula} = params) do
    # TODO: check for diffs between params and debtor
    # Name should match (checked in form changeset)
    # How do we handle different phone numbers?
    # For now we ignore changes, if the person exists
    if debtor = Persons.get_by(cedula: cedula) do
      changeset = Persons.change(debtor, params)
      multi
      |> Persons.backup_old_values(changeset)
      |> Persons.update(action, changeset)
    else
      Persons.new(multi, action, params)
    end
  end

  def upload_proof(multi, action, creditor, debtor, %{
        proof: proof
      }) do
    multi
    |> Multi.run(action, fn _, _ ->
      Cases.store_proof(creditor, debtor, proof)
    end)
  end

  # TODO: Only use Cases.verify! for verification
  defp audit(multi, user, client_info) do
    multi
    |> Audits.add(user, client_info, "case.submit", fn %{case: current_case} ->
      current_case
    end)
    |> Audits.maybe_add(user, client_info, "case.verify", fn %{case: current_case} ->
      case current_case do
        %{verified_at: nil} -> {:error, current_case}
        %{verified_at: _} -> {:ok, current_case}
      end
    end)
  end
end
