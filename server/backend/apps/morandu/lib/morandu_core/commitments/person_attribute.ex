
defmodule MoranduCore.Commitments.PersonAttribute do
  use MoranduCore, :schema
  alias MoranduCore.Commitments.Person
  alias MoranduCore.Ecto.Atom
  alias MoranduCore.Ecto.Term


  schema "person_attributes" do
    field(:type, Atom)
    field(:value, Term)
    belongs_to(:person, Person)
    belongs_to(:set_by, Person)

    timestamps(updated_at: false)
  end

  @optional ~w(value)a
  @required ~w(type)a

  def changeset(change, attrs) do
    change
    |> cast(attrs, @required ++ @optional)
    |> validate_required(@required, message: "validate-required")
  end

  def build(%Person{} = to, %Person{} = from, attrs) do
    %__MODULE__{}
    |> changeset(attrs)
    |> put_assoc(:person, to)
    |> put_assoc(:set_by, from)
  end
end
