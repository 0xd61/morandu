defmodule MoranduCore.Commitments.Case do
  use MoranduCore, :schema
  alias MoranduCore.Commitments.Person

  schema "cases" do
    field(:resolved_at, :utc_datetime_usec)
    field(:signed_at, :date)
    field(:amount, :float)
    field(:verified_at, :utc_datetime_usec)
    field(:proof, :string)
    field(:proof_hash, :string)
    # NOTE: this field is use to report the audit id back to the caller
    field(:current_audit_id, :id, virtual: true)
    belongs_to(:debtor, Person)
    belongs_to(:creditor, Person)

    timestamps()
  end

  # NOTE: resolved_at, verified_at will be set
  # through explicit functions
  @attrs ~w(amount signed_at)a
  @required ~w(proof proof_hash)a

  def changeset(
        changeset,
        attrs,
        verify? \\ not Application.get_env(:morandu, :case_verification)
      ) do
    changeset
    |> cast(attrs, @attrs ++ @required)
    |> validate_required(@required, message: "validate-required")
    |> maybe_put_today(:signed_at)
    |> put_change(:verified_at, (verify? && DateTime.utc_now()) || nil)
  end

  defp maybe_put_today(changeset, field) when is_atom(field) do
    case get_field(changeset, field) do
      nil -> put_change(changeset, field, Date.utc_today())
      _ -> changeset
    end
  end

  def build(%Person{} = creditor, %Person{} = debtor, params) do
    %__MODULE__{}
    |> changeset(params)
    |> put_assoc(:creditor, creditor)
    |> put_assoc(:debtor, debtor)
  end

  def resolve(active_case, resolved_at \\ DateTime.utc_now()) do
    active_case
    |> change()
    |> put_change(:resolved_at, resolved_at)
    |> validate_required(:resolved_at, message: "validate-required")
  end

  def verify(active_case, verified_at \\ DateTime.utc_now()) do
    active_case
    |> change()
    |> put_change(:verified_at, verified_at)
    |> validate_required(:verified_at, message: "validate-required")
  end
end
