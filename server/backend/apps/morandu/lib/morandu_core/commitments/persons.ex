defmodule MoranduCore.Commitments.Persons do
  use MoranduCore, :context
  alias MoranduCore.Commitments.PersonChange
  alias MoranduCore.Commitments.PersonAttribute
  alias MoranduCore.Commitments.Person
  alias MoranduCore.Commitments.Ruc

  def new_checked(attrs \\ %{}) do
    %Person{}
    |> Person.changeset(attrs, true)
    |> Repo.insert()
  end

  def new(attrs \\ %{}) do
    %Person{}
    |> Person.changeset(attrs)
    |> Repo.insert()
  end

  def new(%Ecto.Multi{} = multi, action, params) do
    Multi.insert(multi, action, Person.changeset(%Person{}, params))
  end

  def get(cedula_or_ruc, preload \\ []) do
    search_key =
      if Ruc.is_valid?(cedula_or_ruc) do
        :ruc
      else
        :cedula
      end

    get_by([{search_key, cedula_or_ruc}], preload)
  end

  def get_by!(params, preload \\ []) do
    Repo.get_by!(Person, params)
    |> Repo.preload(preload)
  end

  def get_by(params, preload \\ []) do
    Repo.get_by(Person, params)
    |> Repo.preload(preload)
  end

  def list_by_ids(ids, preload \\ []) do
    from(p in Person,
      where: p.id in ^ids
    )
    |> Repo.all()
    |> Repo.preload(preload)
  end

  def search(query, preload \\ []) do
    from(c in Person)
    |> Person.search(query)
    |> Repo.all()
    |> Repo.preload(preload)
  end

  def change(%Person{} = person, attrs \\ %{}) do
    Person.changeset(person, attrs)
  end

  def update_checked(%Person{} = person, attrs) do
    person
    |> Person.changeset(attrs, true)
    |> Repo.update()
  end

  def update(%Ecto.Multi{} = multi, action, %Ecto.Changeset{data: %Person{}} = changeset) do
    Multi.update(multi, action, changeset)
  end

  # TODO: what if we store changesets? Too much data?
  def backup_old_values(
        %Ecto.Multi{} = multi,
        %Ecto.Changeset{data: %Person{} = person, valid?: true} = changeset
      ) do
    Enum.reduce(changeset.changes, multi, fn {field, value}, acc_multi ->
      with {:ok, old_value} when not is_nil(old_value) <- Map.fetch(person, field),
           type when is_atom(type) <- changeset.types[field] do
        person_changeset =
          PersonChange.build(person, %{
            field: field,
            type: type,
            value: old_value,
            new_value: value
          })

        Multi.insert(acc_multi, "backup_#{field}", person_changeset)
      else
        _ -> acc_multi
      end
    end)
  end

  def backup_old_values(%Ecto.Multi{} = multi, %Ecto.Changeset{data: %Person{}, valid?: false}) do
    multi
  end

  # TODO(dgl): Should this be a separate module?
  def set_attribute(%Person{} = from, %Person{} = to, attrs \\ %{}) do
    PersonAttribute.build(to, from, attrs)
    |> Repo.insert()
  end

  def count_attributes_by(params) do
    from(a in PersonAttribute,
      where: ^Enum.to_list(params),
      select: count()
    )
    |> Repo.one!()
  end

  def get_attribute_by(params) do
    Repo.get_by(PersonAttribute, params)
  end

  def list_attributes_by(params) do
    from(a in PersonAttribute,
      where: ^Enum.to_list(params)
    )
    |> Repo.all()
  end

  def vote(%Person{} = from, cedula_or_ruc, value)
      when is_integer(value) and value > 0 and value <= 5 do
    # NOTE(dgl): If the person does not exist currently, we create one. The "verified" flag is set to false until
    # another person upvotes or we find an entry in the ruc lookup. For more info check the changeset
    {:ok, to} =
      case get(cedula_or_ruc) do
        nil -> new(%{cedula_or_ruc: cedula_or_ruc})
        person -> {:ok, person}
      end

    # TODO(dgl): @@cleanup and use a with statement
    if from.id == to.id do
      :error
    else
      case get_attribute_by(person_id: to.id, set_by_id: from.id, type: :vote) do
        nil ->
          set_attribute(from, to, %{type: :vote, value: value})

        vote ->
          {:ok, vote}
      end
    end
  end

  def get_score(%Person{id: id}) do
    case count_attributes_by(person_id: id, type: :vote) do
      0 ->
        {0, 0}

      count ->
        {list_attributes_by(person_id: id, type: :vote)
         |> Enum.map(fn %{value: val} -> val end)
         |> Enum.sum()
         |> Kernel./(count), count}
    end
  end

  def delete_attribute(%PersonAttribute{} = attr) do
    Repo.delete(attr)
  end

  def delete(%Person{} = person) do
    # TODO(dgl): set all cases where this person is debtor or creditor to "resolved"
    Repo.delete(person)
  end
end
