defmodule MoranduCore.Commitments.Audit do
  use MoranduCore, :schema

  alias MoranduCore.Commitments, as: C
  alias MoranduCore.Ecto.ClientInfo
  alias MoranduCore.Ecto.Atom

  schema "audits" do
    field(:action, :string)
    field(:params, :map)
    field(:user_module, Atom)
    field(:user_id, :id)
    embeds_one(:client_info, ClientInfo)

    timestamps(updated_at: false)
  end

  def build(%mod{id: id}, client, action, params) do
    filtered = filter_params(action, params)

    %__MODULE__{
      action: action,
      user_module: mod,
      user_id: id,
      params: filtered,
      client_info: ClientInfo.build(client)
    }
  end

  defmacrop params_query(key, val) do
    quote do
      fragment("(params->?)::jsonb = ?::jsonb", ^"#{unquote(key)}", ^unquote(val))
    end
  end

  def query_params(query, params) do
    Enum.reduce(params, query, fn {key, val}, acc ->
      Ecto.Query.where(acc, params_query(key, maybe_integer(val)))
    end)
  end

  defp maybe_integer(integer) when is_integer(integer), do: integer

  defp maybe_integer(string) do
    case Integer.parse(string) do
      {integer, _} ->
        integer

      _ ->
        string
    end
  end

  def filter_params("case.submit", %C.Case{} = current_case) do
    serialize(current_case)
  end

  def filter_params("case.verify", %C.Case{} = current_case) do
    serialize(current_case)
  end

  def filter_params("case.resolve", %C.Case{} = current_case) do
    serialize(current_case)
  end

  defp serialize(%C.Case{} = params) do
    Map.take(params, fields(params))
  end

  defp fields(%C.Case{}),
    do: [:id, :debtor_id, :creditor_id, :verified_at, :resolved_at]
end
