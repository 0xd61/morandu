defmodule MoranduCore.Commitments.Cases do
  use MoranduCore, :context

  alias MoranduCore.Commitments.Case
  alias MoranduCore.Commitments.Audits
  alias MoranduCore.Commitments.Person
  alias MoranduCore.Commitments.SubmissionWorkflow
  alias MoranduCore.Store

  # Until now, there was no need to put this into config
  @proof_bucket "proof00.morandu.samuu.srl"
  @proof_region "us-east-1"

  defdelegate submit(user, creditor, client_info, params), to: SubmissionWorkflow, as: :run
  @doc """
    Return changeset of the submission form. The changeset from Case is for internal use only.
  """
  defdelegate change(), to: SubmissionWorkflow, as: :change

  # There will be no undo (to make sync easier).
  # To undo resolve and create new case
  def verify!(user, client_info, %Case{verified_at: nil} = current_case) do
    multi =
      Multi.new()
      |> Multi.update(:case, Case.verify(current_case))
      |> Audits.add(user, client_info, "case.verify", fn %{case: current_case} ->
        current_case
      end)

    case Repo.transaction(multi) do
      {:ok, %{case: current_case, "case.verify": audit}} ->
        %{current_case | current_audit_id: audit.id}

      {:error, _action, _changeset, _} ->
        raise Ecto.InvalidChangesetError
    end
  end

  def verify!(_user, _client_info, %Case{} = current_case) do
    current_case
  end

  def resolve!(user, client_info, %Case{resolved_at: nil} = current_case) do
    multi =
      Multi.new()
      |> Multi.update(:case, Case.resolve(current_case))
      |> Audits.add(user, client_info, "case.resolve", fn %{case: current_case} ->
        current_case
      end)

    case Repo.transaction(multi) do
      {:ok, %{case: current_case, "case.resolve": audit}} ->
        %{current_case | current_audit_id: audit.id}

      {:error, _action, _changeset, _} ->
        raise Ecto.InvalidChangesetError
    end
  end

  def resolve!(_user, _client_info, %Case{} = current_case) do
    current_case
  end

  @spec get_proof(Case.t()) :: {:ok, binary, String.t()} | {:error, term}
  def get_proof(%Case{} = case_) do
    with %{path: path} <- URI.parse(case_.proof),
         key <- Path.basename(path),
         {:ok, file} <- Store.get(@proof_region, @proof_bucket, key),
         true <- "md5_" <> hash_md5(file) === case_.proof_hash do
      {:ok, file, key}
    else
      false ->
        {:error, :hash_mismatch}

      {:error, reason} ->
        {:error, reason}
    end
  end

  @spec store_proof(Person.t(), Person.t(), binary) :: {:ok, map()} | {:error, term}
  def store_proof(%Person{cedula: creditor}, %Person{cedula: debtor}, file) do
    now = DateTime.utc_now() |> DateTime.to_unix() |> to_string()

    with {:ok, _type, ext} <- Store.asset_type(file),
         name <- Enum.join([creditor, debtor, now <> ext], "_"),
         {:ok, uri} <- Store.put(@proof_region, @proof_bucket, name, file) do
      {:ok, %{uri: uri, hash: "md5_" <> hash_md5(file)}}
    else
      :error -> {:error, :decode_error}
      {:error, error} -> {:error, error}
    end
  end

  defp hash_md5(data) do
    :crypto.hash(:md5, data)
    |> Base.encode16(case: :lower)
  end

  def preload(cases, attrs) do
    Repo.preload(cases, attrs)
  end

  def list_active_by(params \\ [], preload \\ []) do
    # HACK: is_nil not working for params (cannot set nil)
    # should make a parse function to build where statements
    from(c in Case,
      where: not is_nil(c.verified_at)
    )
    |> query_active(params)
    |> Repo.all()
    |> Repo.preload(preload)
  end

  # def list_by(params \\ [], preload \\ []) do
  #   from(c in Case,
  #     # is_nil filter
  #     where: ^Enum.filter(params, fn {key, nil} ->
  #       is_nil(c[key])
  #     end)
  #     #
  #   )
  # end

  # defmacro build_where(params) when is_list(params) do
  #   quote do
  #     Enum.map(unquote(params), fn item ->
  #       case item do
  #         {key, nil} ->
  #           {:where, is_nil(key)}
  #         {key, val} ->
  #           {:where, [{key, val}]}
  #       end
  #     end)
  #   end
  # end
  def list_unverified_by(params \\ [], preload \\ []) do
    from(c in Case,
      where: is_nil(c.verified_at)
    )
    |> query_active(params)
    |> Repo.all()
    |> Repo.preload(preload)
  end

  def list_unresolved_by(params \\ [], preload \\ []) do
    from(c in Case,
      order_by: [desc_nulls_first: c.verified_at, desc: c.signed_at]
    )
    |> query_active(params)
    |> Repo.all()
    |> Repo.preload(preload)
  end

  defp query_active(query, params) do
    from(c in query,
      where: is_nil(c.resolved_at),
      where: ^Enum.to_list(params)
    )
  end

  def get!(id, preload \\ []) do
    Case
    |> Repo.get!(id)
    |> Repo.preload(preload)
  end

  def get(id, preload \\ []) do
    Case
    |> Repo.get(id)
    |> Repo.preload(preload)
  end

  @spec list_changed_since(integer) :: {changed_cases :: Case.t(), latest_audit_id :: number}
  def list_changed_since(audit_id, preload \\ []) when is_integer(audit_id) do
    required_actions = ["case.verify", "case.resolve"]

    audits = Audits.get_since(audit_id, required_actions)
    latest = audits |> Enum.sort(&(&1.id >= &2.id)) |> List.first()

    cases =
      audits
      |> build_changes(required_actions)
      |> Enum.map(fn {id, _actions} -> id end)
      |> get_by_ids(preload)

    {cases, (latest && latest.id) || nil}
  end

  defp get_by_ids(ids, preload) when is_list(ids) do
    from(
      c in Case,
      where: c.id in ^ids,
      where: not is_nil(c.verified_at)
    )
    |> Repo.all()
    |> Repo.preload(preload)
  end

  defp build_changes(audits, required_actions) do
    audits
    |> Enum.group_by(&case_id/1, fn %{action: action} -> action end)
    |> Enum.filter(fn audit -> !fulfilled?(audit, required_actions) end)
  end

  defp case_id(%{params: %{"id" => case_id}}) do
    case_id
  end

  defp fulfilled?({_key, value}, actions) do
    case unapplied_actions(value, actions) do
      [] -> true
      _ -> false
    end
  end

  defp unapplied_actions(actions, required) when is_list(actions) and is_list(required) do
    Enum.reduce(required, [], fn action, missing_actions ->
      if Enum.member?(actions, action),
        do: missing_actions,
        else: [action | missing_actions]
    end)
  end
end
