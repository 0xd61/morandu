defmodule MoranduCore.Commitments.PersonChange do
  use MoranduCore, :schema
  alias MoranduCore.Commitments.Person
  alias MoranduCore.Ecto.Atom
  alias MoranduCore.Ecto.Term

  schema "person_changes" do
    field(:field, Atom)
    field(:type, Atom)
    field(:value, Term)
    field(:new_value, Term)
    belongs_to(:person, Person)
    timestamps(updated_at: false)
  end

  @optional ~w(new_value)a
  @required ~w(field type value)a

  def changeset(change, attrs) do
    change
    |> cast(attrs, @required ++ @optional)
    |> validate_required(@required, message: "validate-required")
  end

  def build(%Person{} = person, attrs) do
    %__MODULE__{}
    |> changeset(attrs)
    |> put_assoc(:person, person)
  end
end
