defmodule MoranduCore.Commitments.Person do
  use MoranduCore, :schema

  alias MoranduCore.Commitments.Case
  alias MoranduCore.Commitments.Ruc
  alias MoranduCore.Commitments.PersonAttribute

  schema "persons" do
    field(:cedula_or_ruc, :string, virtual: true)
    field(:cedula, :string)
    field(:ruc, :string)
    field(:name, :string)
    field(:phone, :string)
    # NOTE(dgl): This field is used to determine if we checked the
    # cedula and name or not
    # a checked create/update is always verified
    field(:is_verified, :boolean, default: false)

    has_many(:debts, Case,
      foreign_key: :debtor_id,
      where: [resolved_at: nil, verified_at: {:not, nil}]
    )

    has_many(:cases, Case, foreign_key: :creditor_id)

    has_many(:open_cases, Case,
      foreign_key: :creditor_id,
      where: [resolved_at: nil, verified_at: {:not, nil}]
    )

    has_many(:attributes, PersonAttribute)

    timestamps()
  end

  # NOTE(dgl):  @@important Regarding is_verified:
  # Is a person is created and we do not have lookup info,
  # the is_verified flag is false. If another case or data for this person is
  # created/updated, we set the is_verified flag true (because then we have "local"
  # lookup info). TODO(dgl): is this the behaviour we want? @@cleanup

  @required ~w(name phone cedula)a
  @optional ~w(ruc cedula_or_ruc)a

  def changeset(person, attrs, verified? \\ false) do
    required = if verified?, do: @required, else: ~w(cedula)a

    person
    |> cast(attrs, @required ++ @optional)
    # NOTE: Is updated if cedula and name are verified or fetched by RUC lookup
    # (in build_info/1)
    |> put_change(:is_verified, verified?)
    |> validate(required)
  end

  def validate(changeset, required \\ @required) do
    changeset
    |> validate_format(:cedula_or_ruc, ~r|[0-9]+(-[0-9])?|, message: "validate-format")
    |> build_info()
    # NOTE(dgl): must be after build_info because it may set additional fields
    |> validate_required(required, message: "validate-required")
    |> unique_constraint(:cedula, name: :persons_cedula_index, message: "unique-constraint")
    |> unique_constraint(:ruc, name: :persons_ruc_index, message: "unique-constraint")
    |> validate_phone(:phone, message: "validate-format")
    |> copy_errors(:cedula, :cedula_or_ruc)
  end

  def build_info(changeset, backends \\ Application.get_env(:morandu, :ruc_backends)) do
    case fetch_change(changeset, :cedula_or_ruc) do
      {:ok, cedula_or_ruc} ->
        case Ruc.lookup(cedula_or_ruc, backends: backends) do
          nil ->
            changeset
            |> put_change(:cedula, Ruc.get_cedula(cedula_or_ruc))
            |> put_change(
              :ruc,
              if(Ruc.is_valid?(cedula_or_ruc), do: cedula_or_ruc, else: nil)
            )

          %{cedula: cedula, ruc: ruc, name: name} ->
            changeset
            |> put_change(:cedula, cedula)
            |> put_change(:ruc, ruc)
            |> validate_string_distance(:name, name, message: "validate-name")
            |> maybe_put_change(:is_verified, true)
        end

      :error ->
        changeset
    end
  end

  def build(attrs) do
    %__MODULE__{}
    |> changeset(attrs)
  end

  #
  #
  #

  defmacrop like_query(p, search) do
    quote do
      ilike(fragment("?::text", unquote(p).cedula), ^unquote(search))
    end
  end

  defmacrop name_query(p, search) do
    quote do
      fragment(
        "similarity(?, ?) > 0.4",
        unquote(p).name,
        ^unquote(search)
      )
    end
  end

  defp like_search(search), do: search <> "%"

  defp name_search(search) do
    search
    |> String.replace(~r/[^\w\s]/u, "")
    |> String.trim()
    |> String.replace(~r"\s+"u, " | ")
  end

  def search(query, search) do
    from(p in query,
      where: like_query(p, like_search(search)) or name_query(p, name_search(search))
    )
  end
end
