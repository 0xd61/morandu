defmodule MoranduCore.Commitments.Audits do
  use MoranduCore, :context
  alias MoranduCore.Commitments.Audit

  def add(multi, user, client_info, action, params) when is_map(params) do
    Multi.insert(multi, multi_key(action), Audit.build(user, client_info, action, params))
  end

  def add(multi, user, client_info, action, fun) when is_function(fun, 1) do
    maybe_add(multi, user, client_info, action, &{:ok, fun.(&1)})
  end

  @doc """
    If fun returns {:ok, data} a new audit entry will be performend. Otherwise nothing happens.
  """
  def maybe_add(multi, user, client_info, action, fun) when is_function(fun, 1) do
    Multi.merge(multi, fn data ->
      case fun.(data) do
        {:ok, params} ->
          Multi.insert(
            Multi.new(),
            multi_key(action),
            Audit.build(user, client_info, action, params)
          )

        _ ->
          Multi.new()
      end
    end)
  end

  def get_by!(params, preload \\ []) do
    case Keyword.pop(params, :params) do
      {nil, remain} ->
        from(a in Audit,
          where: ^Enum.to_list(remain)
        )

      {audit_params, remain} ->
        from(a in Audit,
          where: ^Enum.to_list(remain)
        )
        |> Audit.query_params(audit_params)
    end
    |> Repo.one!()
    |> Repo.preload(preload)
  end

  def get_since(id, actions \\ []) when is_list(actions) do
    from(l in Audit,
      where: l.id > ^id
    )
    |> filter_actions(actions)
    |> Repo.all()
  end

  defp filter_actions(query, list) when length(list) === 0 do
    query
  end

  defp filter_actions(query, list) do
    query
    |> Ecto.Query.where([a], a.action in ^list)
  end

  defp multi_key(action) when is_binary(action), do: String.to_atom(action)
  defp multi_key(action) when is_atom(action), do: action
end
