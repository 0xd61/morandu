defmodule MoranduCore.Commitments.SubmissionForm do
  use MoranduCore, :schema
  alias MoranduCore.Commitments.Person
  alias MoranduCore.Store

  embedded_schema do
    field(:cedula_or_ruc, :string, virtual: true)
    field(:cedula, :string)
    field(:ruc, :string)
    field(:name, :string)
    field(:phone, :string)
    field(:signed_at, :date)
    field(:amount, :float)
    field(:proof, :binary)
  end

  @required ~w(cedula_or_ruc name phone proof)a
  @optional ~w(signed_at amount)a

  def changeset(attrs) do
    %__MODULE__{}
    |> cast(attrs, @required ++ @optional)
    |> Person.validate()
    # NOTE(dgl): validate again after Person.validate because this sets some keys.
    # The additional validation is only necessary for keys not in the person schema.
    # However we check again for simplicity. @@performance
    |> validate_required(@required, message: "validate-required")
    |> validate_filetype(:proof, message: "validate-filetype")
  end

  def validate_filetype(changeset, field, opts \\ []) do
    validate_change(changeset, field, fn _, data ->
      case Store.asset_type(data) do
        {:ok, _, _} ->
          []

        {:error, :type_not_supported} ->
          [{field, opts[:message] || "filetype is not supported"}]
      end
    end)
  end
end
