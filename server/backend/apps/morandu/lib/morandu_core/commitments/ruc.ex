defmodule MoranduCore.Commitments.Ruc do
  @moduledoc """
    Here is a list of zip archives https://www.set.gov.py/portal/PARAGUAY-SET/InformesPeriodicos?folder-id=repository:collaboration:/sites/PARAGUAY-SET/categories/SET/Informes%20Periodicos/listado-de-ruc-con-sus-equivalencias

    TODO: create offline backend with these archives
  """
  alias MoranduCore.Commitments.Ruc.{Local, RucComPy}
  @callback lookup(cedula_or_ruc :: String.t()) :: %{cedula: String.t(), name: String.t()} | nil

  @backends [Local, RucComPy]

  def lookup(cedula_or_ruc, opts \\ []) do
    backends = Keyword.get(opts, :backends) || @backends

    Enum.reduce(backends, nil, fn
      backend, nil ->
        case backend.lookup(cedula_or_ruc) do
          nil ->
            nil

          result ->
            result
        end

      _backend, acc ->
        acc
    end)
  end

  def get_cedula(ruc) do
    String.split(ruc, "-") |> List.first()
  end

  def is_valid?(ruc) do
    String.match?(ruc, ~r|^\d*-\d{1}$|)
  end
end

defmodule MoranduCore.Commitments.Ruc.Local do
  @behaviour MoranduCore.Commitments.Ruc
  alias MoranduCore.Commitments.Persons

  def lookup(cedula_or_ruc) do
    cedula = String.split(cedula_or_ruc, "-") |> List.first()

    case Persons.get_by(cedula: cedula) do
      nil ->
        nil

      %{name: name, ruc: ruc, cedula: cedula} ->
        %{name: name, ruc: ruc, cedula: cedula}
    end
  end
end

defmodule MoranduCore.Commitments.Ruc.RucComPy do
  alias MoranduCore.Commitments.Ruc
  @host "https://ruc.com.py"
  @behaviour Ruc

  def lookup(cedula) do
    url = URI.parse(@host) |> URI.merge("index.php/inicio/consulta_ruc") |> to_string()
    payload = "buscar=" <> cedula

    case send_request(:post, url, payload) do
      {:ok, 200, _header, body} ->
        body
        |> String.replace("\n", "")
        |> String.split(~r|<.*?>.*?<.*?>|, trim: true)
        |> Enum.map(&String.trim(&1))
        |> Enum.reduce(%{}, fn item, acc ->
          cond do
            # match ruc
            Ruc.is_valid?(item) ->
              acc
              |> Map.put_new(:ruc, item)
              |> Map.put_new(:cedula, Ruc.get_cedula(item))

            # match cedula
            String.match?(item, ~r|^\d*$|) ->
              acc
              |> Map.put_new(:cedula, item)
              |> Map.put_new(:ruc, nil)

            # match name
            String.match?(item, ~r|^.*,.*$|) ->
              [lastname, firstname] =
                item
                |> String.split(",")
                |> Enum.map(fn word ->
                  String.split(word)
                  |> Enum.map(&String.capitalize/1)
                  |> Enum.join(" ")
                end)

              Map.put_new(acc, :name, firstname <> " " <> lastname)

            true ->
              nil
          end
        end)

      _ ->
        nil
    end
  end

  def send_request(method, url, payload, opts \\ []) do
    opts =
      [connect_timeout: 5_000, recv_timeout: 5_000, with_body: true]
      |> Keyword.merge(opts)

    headers = [
      {"User-Agent", "Elixir ruc.com.py Client"},
      {"X-Requested-With", "XMLHttpRequest"},
      {"Content-Type", "application/x-www-form-urlencoded; charset=UTF-8"}
      | Keyword.get(opts, :headers, [])
    ]

    case :hackney.request(method, url, headers, payload, opts) do
      {status, code, header, body} when is_binary(body) ->
        {status, code, Map.new(header), body}

      {status, reason} ->
        {status, reason, nil, nil}
    end
  end
end

