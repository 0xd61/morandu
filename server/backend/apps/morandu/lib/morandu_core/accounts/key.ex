defmodule MoranduCore.Accounts.Key do
  use MoranduCore, :schema
  alias MoranduCore.Accounts.User
  alias MoranduCore.Accounts.Auth
  alias MoranduCore.Ecto.Atom
  alias MoranduCore.Ecto.ClientInfo

  schema "keys" do
    field(:user_secret, :string, virtual: true)
    field(:secret_first, :string)
    field(:secret_second, :string)
    field(:revoke_at, :utc_datetime_usec)
    field(:revoked_at, :utc_datetime_usec)
    belongs_to(:user, User)
    embeds_one(:last_use, ClientInfo, on_replace: :delete)

    field(:type, Atom)

    timestamps()
  end

  # if secret exists, a password key is created. Otherwise a access key is created
  def changeset(key, params, secret \\ nil) do
    key
    |> cast(params, ~w()a)
    |> validate_required(~w()a, message: "validate-required")
    |> add_keys(secret)
  end

  def revoke(key, revoked_at \\ DateTime.utc_now()) do
    key
    |> change()
    |> put_change(:revoked_at, key.revoked_at || revoked_at)
    |> validate_required(:revoked_at, message: "validate-required")
  end

  def update_last_use(key, params) do
    info = ClientInfo.build(params)

    key
    |> change()
    |> put_embed(:last_use, info)
  end

  defp add_keys(changeset, nil) do
    key = gen_key()
    put_keys(changeset, key, :key)
  end

  defp add_keys(changeset, secret) do
    hash = gen_password(secret)
    put_keys(changeset, hash, :password)
  end

  defp put_keys(changeset, {user_secret, first, second}, type) do
    changeset
    |> put_change(:user_secret, user_secret)
    |> put_change(:secret_first, first)
    |> put_change(:secret_second, second)
    |> put_change(:type, type)
  end

  defp gen_password(secret) do
    hash = Auth.gen_password(secret)
    {secret, hash, nil}
  end

  defp gen_key() do
    user_secret = Auth.gen_key()

    app_secret = Application.get_env(:morandu, :secret)

    <<first::binary-size(32), second::binary-size(32)>> =
      :crypto.hmac(:sha256, app_secret, user_secret)
      |> Base.encode16(case: :lower)

    {user_secret, first, second}
  end
end
