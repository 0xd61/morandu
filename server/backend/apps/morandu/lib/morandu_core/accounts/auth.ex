defmodule MoranduCore.Accounts.Auth do
  use MoranduCore, :context
  alias MoranduCore.Accounts.{Keys, Users}
  alias MoranduCore.Utils

  def gen_password(password) do
    Bcrypt.hash_pwd_salt(password)
  end

  def gen_key() do
    :crypto.strong_rand_bytes(16)
    |> Base.encode16(case: :lower)
  end

  def via_password(email, user_secret, usage_info) do
    user = Users.get_by([email: email], :password_hash)
    key = user && user.password_hash

    cond do
      user && Bcrypt.verify_pass(user_secret, key.secret_first) ->
        if Keys.revoked?(key) do
          {:error, :revoked}
        else
          Keys.update_last_use(key, usage_info)
          {:ok, %{key: key, user: user, source: :password}}
        end

      user ->
        {:error, :unauthorized}

      true ->
        Bcrypt.no_user_verify()
        {:error, :not_found}
    end
  end

  def via_key(user_secret, usage_info) do
    # Database index lookup on the first part of the key and then
    # secure compare on the second part to avoid timing attacks
    # credits to https://github.com/hexpm/hexpm/blob/cffcf2e31fb5bb3b711ebfca6986937b5e96a168/lib/hexpm/accounts/auth.ex#L7
    app_secret = Application.get_env(:morandu, :secret)

    <<first::binary-size(32), second::binary-size(32)>> =
      :crypto.hmac(:sha256, app_secret, user_secret)
      |> Base.encode16(case: :lower)

    result = Keys.get(first)

    case result do
      nil ->
        {:error, :not_found}

      key ->
        if Utils.secure_check(key.secret_second, second) do
          if Keys.revoked?(key) do
            {:error, :revoked}
          else
            Keys.update_last_use(key, usage_info)

            {:ok, %{key: key, user: key.user, source: :key}}
          end
        else
          {:error, :unauthorized}
        end
    end
  end
end
