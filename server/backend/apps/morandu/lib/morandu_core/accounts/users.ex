defmodule MoranduCore.Accounts.Users do
  use MoranduCore, :context
  use MoranduCore.Permission.Actions, as: A
  alias MoranduCore.Accounts.User

  def new(%MoranduCore.Commitments.Person{} = person, attrs \\ %{}) do
    User.register(person, attrs)
    |> set_permissions(A.read())
    |> Repo.insert()
  end

  def get!(id, preload \\ []) do
    Repo.get!(User, id) |> Repo.preload(preload)
  end

  def get_by!(params, preload \\ []) do
    Repo.get_by!(User, params)
    |> Repo.preload(preload)
  end

  def get_by(params, preload \\ []) do
    Repo.get_by(User, params)
    |> Repo.preload(preload)
  end

  def list_by(params, preload \\ []) do
    from(u in User,
      where: ^Enum.to_list(params)
    )
    |> Repo.all()
    |> Repo.preload(preload)
  end

  def list_by_ids(ids, preload \\ []) do
    from(u in User,
      where: u.id in ^ids
    )
    |> Repo.all()
    |> Repo.preload(preload)
  end

  def change(%User{} = user) do
    User.changeset(user, %{})
  end

  def update(%User{} = user, attrs) do
    user
    |> User.changeset(attrs)
    |> Repo.update()
  end

  def delete(%User{} = user) do
    Repo.delete(user)
  end

  def set_role(user_or_changeset, permission_flag) when is_integer(permission_flag) do
    set_permissions(user_or_changeset, permission_flag)
    |> Repo.update!()
  end

  defp set_permissions(user_or_changeset, permission_flag) when is_integer(permission_flag) do
    User.set_permissions(user_or_changeset, permission_flag)
  end
end
