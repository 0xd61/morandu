defmodule MoranduCore.Accounts.User do
  use MoranduCore, :schema
  alias MoranduCore.Accounts.{Key, PasswordReset}

  schema "users" do
    field(:email, :string)
    field(:username, :string)
    field(:password, :string, virtual: true)
    field(:commitments_person_id, :id)
    has_one(:password_hash, Key, where: [type: :password], on_replace: :update)
    has_many(:keys, Key, where: [type: :key, revoked_at: nil])
    has_many(:password_resets, PasswordReset)

    # NOTE: Maybe we have to update to permissions per resource
    # aka. Permissions for other Users,
    # Permissions for Persons,
    # Permissions for Cases, etc.
    # Then we should use an embedded schema with a one_to_many relationship.
    # This relationship should have the domain and permission flag
    field(:permissions, :integer, default: 0)

    timestamps()
  end

  @attrs ~w(username password)a
  @required ~w(email)a

  @doc false
  def register(%{id: id}, attrs) do
    %__MODULE__{commitments_person_id: id}
    |> changeset(attrs)
    |> validate_required(~w(password)a)
  end

  @doc false
  def changeset(user, attrs) do
    user
    |> cast(attrs, @attrs ++ @required)
    |> validate_required(@required, message: "validate-required")
    |> validate_email(:email, message: "validate-format")
    |> unique_constraint(:email, message: "unique-constraint")
    |> validate_length(:password, min: 10, message: "validate-length")
    |> hash_password()
  end

  defp hash_password(changeset) do
    case fetch_change(changeset, :password) do
      {:ok, change} ->
        changeset = %{changeset | params: Map.put(changeset.params, "password_hash", %{})}

        cast_assoc(changeset, :password_hash, with: &Key.changeset(&1, &2, change), required: true)

      _ ->
        changeset
    end
  end

  def set_permissions(user, permissions) do
    user
    |> change(permissions: permissions)
  end
end
