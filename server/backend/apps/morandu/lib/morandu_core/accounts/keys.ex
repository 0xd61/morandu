defmodule MoranduCore.Accounts.Keys do
  use MoranduCore, :context
  alias MoranduCore.Accounts.Key

  def new(user, attrs \\ %{}) do
    user
    |> build_assoc(:keys)
    |> Key.changeset(attrs)
    |> Repo.insert()
  end

  def get(secret_first) do
    from(
      k in Key,
      where: k.secret_first == ^secret_first,
      preload: [:user]
    )
    |> Repo.one()
  end

  def get_by!(params, preload \\ []) do
    Repo.get_by!(Key, params)
    |> Repo.preload(preload)
  end

  def update_last_use(%Key{} = key, usage_info) do
    key
    |> Key.update_last_use(usage_info)
    |> Repo.update!()
  end

  def revoke(key) do
    Repo.update(Key.revoke(key))
  end

  def revoked?(%Key{} = key) do
    not is_nil(key.revoked_at) or
      (not is_nil(key.revoke_at) and DateTime.compare(key.revoke_at, DateTime.utc_now()) == :lt)
  end
end
