defmodule MoranduCore.Accounts.PasswordReset do
  use MoranduCore, :schema
  alias MoranduCore.Accounts.Auth
  alias MoranduCore.Accounts.User

  schema "password_resets" do
    field(:key, :string)
    field(:email, :string)
    belongs_to(:user, User)

    timestamps(updated_at: false)
  end

  def changeset(reset, user) do
    change(reset, %{
      key: Auth.gen_key(),
      email: User.email(user)
    })
  end

  def can_reset?(reset, email, key) do
    valid_email? = email == reset.email
    valid_key? = !!(reset.key && MoranduCore.Utils.secure_check(reset.key, key))
    within_time? = MoranduCore.Utils.within_last_day?(reset.inserted_at)

    valid_email? and valid_key? and within_time?
  end
end
