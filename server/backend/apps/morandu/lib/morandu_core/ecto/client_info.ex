defmodule MoranduCore.Ecto.ClientInfo do
  use MoranduCore, :schema

  embedded_schema do
    field(:used_at, :utc_datetime_usec)
    field(:user_agent, :string)
    field(:ip, :string)
  end

  def build(params) do
    __MODULE__
    |> struct(params)
    |> to_string(:ip)
    |> to_string(:user_agent)
  end

  defp to_string(%{ip: ip} = struct, :ip) when not is_nil(ip) and not is_binary(ip) do
    %{struct | ip: ip |> :inet.ntoa() |> to_string()}
  end

  defp to_string(%{user_agent: user_agent} = struct, :user_agent) when not is_nil(user_agent) and not is_binary(user_agent) do
    %{struct | user_agent: to_string(user_agent)}
  end

  defp to_string(struct, _), do: struct
end
