defmodule MoranduCore.Ecto.Atom do
  use Ecto.Type

  def type, do: :string

  def cast(value), do: {:ok, value}

  def load(value), do: {:ok, String.to_atom(value)}

  def dump(value) when is_atom(value), do: {:ok, Atom.to_string(value)}
  def dump(_), do: :error
end

defmodule MoranduCore.Ecto.Term do
  use Ecto.Type

  def type, do: :binary

  def cast(value), do: {:ok, value}

  def load(value), do: {:ok, :erlang.binary_to_term(value, [:safe])}

  def dump(value), do: {:ok, :erlang.term_to_binary(value)}
end
