defmodule MoranduCore.Ecto.Changeset do
  import Ecto.Changeset

  def maybe_validate_length(changeset, field, opts) do
    case get_change(changeset, field) do
      nil -> changeset
      _value -> validate_length(changeset, field, opts)
    end
  end

  def copy_errors(changeset, from, to) do
    errors =
      Keyword.get_values(changeset.errors, from)
      |> Enum.map(&{to, &1})

    %{changeset | errors: errors ++ changeset.errors}
  end

  def validate_email(changeset, field, opts \\ []) do
    changeset
    |> validate_format(field, ~r/^[A-Za-z0-9._%+\-+']+@[A-Za-z0-9.\-]+\.[A-Za-z]+$/, opts)
  end

  def validate_phone(changeset, field, opts \\ []) do
    changeset
    |> validate_format(
      field,
      ~r/^\s*(?:\+?(\d{1,3}))?[-. (]*(\d{3})[-. )]*(\d{3})[-. ]*(\d{4})(?: *x(\d+))?\s*$/,
      opts
    )
  end

  def validate_string_distance(changeset, field, str, opts \\ []) do
    distance = opts[:distance] || 0.75

    validate_change(changeset, field, fn _, input ->
      if String.jaro_distance(input, str) >= distance do
        []
      else
        [{field, {opts[:message] || "is too different from %{str}", str: str}}]
      end
    end)
  end

  def maybe_put_change(changeset, field, value) do
    case fetch_field(changeset, field) do
      {_, _} -> put_change(changeset, field, value)
      _ -> changeset
    end
  end


end
