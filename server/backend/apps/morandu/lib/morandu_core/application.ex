defmodule MoranduCore.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    # List all child processes to be supervised
    children = [
      MoranduCore.Repo,
      MoranduWeb.Endpoint,
      # Start the PubSub system
      {Phoenix.PubSub, name: MoranduWeb.PubSub},
      MoranduWeb.Fluex,
      MoranduWeb.Acme
    ]

    opts = [strategy: :one_for_one, name: MoranduCore.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    MoranduWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
