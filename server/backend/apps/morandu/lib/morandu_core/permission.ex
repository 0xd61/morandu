defmodule MoranduCore.Permission.Actions do
  import Bitwise, only: [<<<: 2]

  @actions [create: 1 <<< 0, read: 1 <<< 1, edit: 1 <<< 2, delete: 1 <<< 3]

  @actions
  |> Enum.each(fn {action, id} ->
    defmacro unquote(action)(), do: unquote(id)
  end)

  @doc false
  defmacro __using__(options) do
    alias_ = Keyword.get(options, :as, Actions)

    quote do
      import Bitwise, only: [|||: 2]
      require MoranduCore.Permission.Actions
      alias MoranduCore.Permission.Actions, as: unquote(alias_)
    end
  end
end

# TODO(dgl): Simplify permission system @@cleanup
defmodule MoranduCore.Permission do
  use MoranduCore.Permission.Actions, as: A
  import Bitwise, only: [|||: 2, &&&: 2]
  alias MoranduCore.Accounts.User
  alias MoranduCore.Commitments.{Case}

  def can?(action, actor, resource \\ nil)

  # Resource Permissions
  # User can access his own user without permissions
  def can?(:read, %User{id: id}, %User{id: id}), do: true
  def can?(:edit, %User{id: id}, %User{id: id}), do: true

  # User can read other users from same person (company)
  def can?(:read, %User{commitments_person_id: id, permissions: perm}, %User{
        commitments_person_id: id
      })
      when (perm &&& A.read()) !== 0,
      do: true

  # User needs edit permission to change other users from same person (company)
  def can?(:edit, %User{commitments_person_id: id, permissions: perm}, %User{
        commitments_person_id: id
      })
      when (perm &&& (A.read() ||| A.edit())) == (A.read() ||| A.edit()),
      do: true

  # User can delete (resolve) cases
  def can?(:delete, %User{commitments_person_id: id, permissions: perm}, %Case{creditor_id: id})
      when (perm &&& A.delete()) !== 0,
      do: true

  # General Permissions
  def can?(:create, %{permissions: perm}, nil) when (perm &&& A.create()) !== 0, do: true
  def can?(:read, %{permissions: perm}, nil) when (perm &&& A.read()) !== 0, do: true
  def can?(:edit, %{permissions: perm}, nil) when (perm &&& A.edit()) !== 0, do: true
  def can?(:delete, %{permissions: perm}, nil) when (perm &&& A.delete()) !== 0, do: true

  def can?(_action, _user, _resource) do
    # IO.inspect(_action)
    # IO.inspect(_user)
    # IO.inspect(_resource)
    false
  end
end
