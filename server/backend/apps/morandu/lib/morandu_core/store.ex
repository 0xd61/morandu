defmodule MoranduCore.Store do
  @type region :: String.t()
  @type bucket :: String.t()
  @type key :: String.t()
  @type body :: binary
  @type opts :: Keyword.t()

  @callback get(region, bucket, key, opts) :: {:ok, body} | {:error, term}
  @callback put(region, bucket, key, body, opts) :: {:ok, term} | {:error, term}

  defp impl(), do: Application.get_env(:morandu, :store)

  def get(region, bucket, key, opts \\ []), do: impl().get(region, bucket, key, opts)
  def put(region, bucket, key, body, opts \\ []), do: impl().put(region, bucket, key, body, opts)

  def asset_type(<<0x89, 0x50, 0x4E, 0x47, 0x0D, 0x0A, 0x1A, 0x0A, _::binary>>),
    do: {:ok, "image/png", ".png"}

  def asset_type(<<0xFF, 0xD8, _::binary>>), do: {:ok, "image/jpeg", ".jpg"}

  def asset_type(<<0x25, 0x50, 0x44, 0x46, _::binary>>), do: {:ok, "application/pdf", ".pdf"}

  def asset_type(_), do: {:error, :type_not_supported}
end
