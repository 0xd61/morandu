defmodule MoranduCore.Utils do
  def secure_check(left, right) do
    if byte_size(left) == byte_size(right) do
      secure_check(left, right, 0) == 0
    else
      false
    end
  end

  defp secure_check(<<left, left_rest::binary>>, <<right, right_rest::binary>>, acc) do
    import Bitwise, only: [|||: 2, ^^^: 2]
    secure_check(left_rest, right_rest, acc ||| left ^^^ right)
  end

  defp secure_check(<<>>, <<>>, acc) do
    acc
  end

  @doc """
  Determine if a given timestamp is less than a day (86400 seconds) old
  """
  def within_last_day?(nil), do: false

  def within_last_day?(a) do
    diff = diff(NaiveDateTime.to_erl(a), :calendar.universal_time())

    diff < 24 * 60 * 60
  end

  defp diff(a, b) do
    {days, time} = :calendar.time_difference(a, b)
    :calendar.time_to_seconds(time) - days * 24 * 60 * 60
  end

  def build_csv(data, separator \\ ",")

  def build_csv(data, separator) when is_list(data) do
    header =
      data
      |> Enum.at(0, %{})
      |> Enum.map(fn {key, _val} -> key end)
      |> csv_row(separator)

    body =
      data
      |> Enum.map(&csv_row(&1, separator))
      |> Enum.join()

    header <> body
  end

  defp csv_row(data, separator) when is_map(data) do
    row = Enum.map(data, fn {_key, val} ->
      val
    end)
    |> Enum.join(separator)
    row <> "\n"
  end

  defp csv_row(data, separator) when is_list(data) do
    row = Enum.join(data, separator)
    row <> "\n"
  end
end
