defmodule MoranduCore.Store.S3.Request do
  defstruct [:method, :uri, :region, headers: [], payload: ""]
end

defmodule MoranduCore.Store.S3 do
  @behaviour MoranduCore.Store

  alias MoranduCore.Store.S3.Request

  def get(region, bucket, key, opts) do
    opts = Keyword.merge(Application.get_env(:morandu, :s3), opts)

    uri =
      opts
      |> Keyword.fetch!(:base_uri)
      |> URI.parse()
      |> URI.merge(Path.join(bucket, key))

    req = %Request{
      method: :get,
      uri: uri,
      region: region
    }

    case send_request(req, opts) do
      {:ok, 200, _, body} ->
        {:ok, body}

      {:ok, _, _, body} ->
        {:error, body}
    end
  end

  def put(region, bucket, key, body, opts) do
    opts = Keyword.merge(Application.get_env(:morandu, :s3), opts)

    uri =
      opts
      |> Keyword.fetch!(:base_uri)
      |> URI.parse()
      |> URI.merge(Path.join(bucket, key))

    req = %Request{
      method: :put,
      uri: uri,
      payload: body,
      region: region
    }

    case send_request(req, opts) do
      {:ok, 200, _, _} ->
        {:ok, to_string(uri)}

      {:ok, _, _, body} ->
        {:error, body}
    end
  end

  @doc """
  https://docs.aws.amazon.com/general/latest/gr/sigv4-create-canonical-request.html
  """
  def canonical_request_v4(method, %URI{} = uri, headers, signed_headers, payload_hash) do
    [
      # HTTPRequestMethod
      method |> Atom.to_string() |> String.upcase(),
      # CanonicalURI
      uri.path |> URI.encode(),
      # CanonicalQueryString
      (uri.query || "")
      |> URI.decode_query()
      |> Enum.to_list()
      |> Enum.sort(fn {k1, _}, {k2, _} -> k1 < k2 end)
      |> Enum.map_join("&", fn {k, v} ->
        URI.encode_www_form(to_string(k)) <> "=" <> aws_encode_www_form(to_string(v))
      end),
      # CanonicalHeaders
      headers
      |> Enum.sort(fn {k1, _}, {k2, _} -> k1 < k2 end)
      |> Enum.map_join(fn {k, v} ->
        # downcase key and trim all whitespace on value
        String.downcase(k) <> ":" <> (String.split(v) |> Enum.join(" ")) <> "\n"
      end),
      # SignedHeaders
      signed_headers,
      # RequestPayload
      payload_hash
    ]
    |> Enum.join("\n")
    |> hash_sha256()
    |> Base.encode16(case: :lower)
  end

  @doc """
  https://docs.aws.amazon.com/general/latest/gr/sigv4-calculate-signature.html
  builds authorization string for Authorization header
  Uses secret_key and access_key from config
  """
  def authorization_v4(req, secret_key, access_key, opts \\ []) do
    now = Keyword.get(opts, :date, DateTime.utc_now()) |> DateTime.truncate(:second)
    algorithm = "AWS4-HMAC-SHA256"
    payload_hash = req.payload |> hash_sha256 |> Base.encode16(case: :lower)
    # Add necessary headers for v4 signature and canonical request
    headers = [
      {"X-Amz-Date", DateTime.to_iso8601(now, :basic)},
      {"X-Amz-Content-Sha256", payload_hash},
      {"Host", req.uri.host}
      | req.headers
    ]

    signed_headers =
      headers
      |> Enum.sort(fn {k1, _}, {k2, _} -> k1 < k2 end)
      |> Enum.map_join(";", fn {k, _v} -> String.downcase(k) end)

    # Build canonical request
    canonical_req =
      canonical_request_v4(req.method, req.uri, headers, signed_headers, payload_hash)

    scope = [
      now |> DateTime.to_date() |> Date.to_iso8601(:basic),
      to_string(req.region),
      "s3",
      "aws4_request"
    ]

    string_to_sign =
      [
        # Algorithm
        algorithm,
        # RequestDateTime
        DateTime.to_iso8601(now, :basic),
        # CredentialScope
        Path.join(scope),
        # HashedCanonicalRequest
        canonical_req
      ]
      |> Enum.join("\n")

    signature =
      scope
      # build signing key
      |> Enum.reduce("AWS4" <> secret_key, &hmac_sha256(&2, &1))
      # sign string_to_sign
      |> hmac_sha256(string_to_sign)
      |> Base.encode16(case: :lower)

    # Build Authorization Header Info
    auth_header =
      {"Authorization",
       "#{algorithm} Credential=#{access_key}/#{Path.join(scope)}, SignedHeaders=#{signed_headers}, Signature=#{
         signature
       }"}

    %{req | headers: [auth_header | headers]}
  end

  # is basically the same as URI.encode_www_form
  # but does encode "+" as %20
  def aws_encode_www_form(str) when is_binary(str) do
    import Bitwise

    for <<c <- str>>, into: "" do
      case URI.char_unreserved?(c) do
        true -> <<c>>
        false -> <<"%", hex(bsr(c, 4)), hex(band(c, 15))>>
      end
    end
  end

  defp hex(n) when n <= 9, do: n + ?0
  defp hex(n), do: n + ?A - 10

  @doc """
  send request to s3 endpoint.
  opts must provide :access_key and :secret_key.
  """
  def send_request(%Request{} = req, opts) do
    opts =
      [connect_timeout: 10_000, recv_timeout: 5_000, with_body: true]
      |> Keyword.merge(opts)

    # required options
    access_key = Keyword.fetch!(opts, :access_key)
    secret_key = Keyword.fetch!(opts, :secret_key)

    authorized_req = authorization_v4(req, secret_key, access_key)

    headers = [
      {"User-Agent", "Elixir S3 Client"}
      | authorized_req.headers
    ]

    case :hackney.request(
           authorized_req.method,
           authorized_req.uri |> to_string(),
           headers,
           authorized_req.payload,
           opts
         ) do
      {status, code, header, body} when is_binary(body) ->
        body =
          case Jason.decode(body) do
            {:ok, decoded} -> decoded
            {:error, _} -> body
          end

        {status, code, Map.new(header), body}

      {status, code, header, client} ->
        body =
          case :hackney.body(client) do
            {:ok, body} -> Jason.decode!(body)
            _ -> nil
          end

        {status, code, Map.new(header), body}

      {status, code, header} ->
        {status, code, Map.new(header), nil}

      {status, reason} ->
        {status, reason, nil, nil}
    end
  end

  def hash_sha256(data) do
    :sha256
    |> :crypto.hash(data)
  end

  def hmac_sha256(key, data) do
    :crypto.hmac(:sha256, key, data)
  end
end
