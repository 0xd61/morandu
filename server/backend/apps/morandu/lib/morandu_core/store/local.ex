defmodule MoranduCore.Store.Local do
  @behaviour MoranduCore.Store

  def get(region, bucket, key, _opts) do
    path = Path.join([dir(), region(region), bucket(bucket), key])
    File.read(path)
  end

  def put(region, bucket, key, blob, _opts) do
    path = Path.join([dir(), region(region), bucket(bucket), key])
    with :ok <- File.mkdir_p(Path.dirname(path)),
         :ok <- File.write(path, blob) do
      {:ok, path}
    end
  end

  defp bucket(atom) when is_atom(atom) do
    Atom.to_string(atom)
  end

  defp bucket(binary) when is_binary(binary) do
    binary
  end

  defp region(binary) when is_binary(binary) do
    binary
  end

  defp dir() do
    Application.app_dir(:morandu, "priv")
    |> Path.join("store")
  end
end
