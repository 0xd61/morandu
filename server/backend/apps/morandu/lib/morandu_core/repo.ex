defmodule MoranduCore.Repo do
  use Ecto.Repo,
    otp_app: :morandu,
    adapter: Ecto.Adapters.Postgres
end
