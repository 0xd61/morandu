defmodule MoranduCore do
  @moduledoc """
  Morandu keeps the contexts that define your domain
  and business logic.

  Contexts are also responsible for managing your data, regardless
  if it comes from the database, an external API or others.
  """

  def context() do
    quote do
      import Ecto
      import Ecto.Changeset
      import Ecto.Query, only: [from: 1, from: 2]
      alias Ecto.Multi

      alias MoranduCore.Repo
    end
  end

  def schema() do
    quote do
      use Ecto.Schema
      # @timestamps_opts [type: :utc_datetime_usec]

      import Ecto
      import Ecto.Changeset
      import Ecto.Query, only: [from: 1, from: 2]

      import MoranduCore.Ecto.Changeset
    end
  end

  @doc """
  When used, dispatch to the appropriate controller/view/etc.
  """
  defmacro __using__(which) when is_atom(which) do
    apply(__MODULE__, which, [])
  end
end
