defmodule MoranduCore.TestHelpers do
  alias MoranduCore.Repo
  alias MoranduCore.Store

  def key_fixture(attrs \\ %{}) do
    user = user_fixture()
    key_fixture(user, attrs)
  end

  def key_fixture(user, attrs) do
    user_secret = attrs[:user_secret] || MoranduCore.Accounts.Auth.gen_key()
    app_secret = Application.get_env(:morandu, :secret)

    <<first::binary-size(32), second::binary-size(32)>> =
      :crypto.hmac(:sha256, app_secret, user_secret)
      |> Base.encode16(case: :lower)

    key = %MoranduCore.Accounts.Key{
      user_id: user.id,
      secret_first: attrs[:secret_first] || first,
      secret_second: attrs[:secret_second] || second,
      revoke_at: attrs[:revoke_at] || nil,
      revoked_at: attrs[:revoked_at] || nil,
      type: attrs[:type] || :key
    }

    {:ok, key} = Repo.insert(key)
    %{key | user_secret: user_secret}
  end

  def password_reset_fixture(attrs \\ %{}) do
    user = user_fixture()
    password_reset_fixture(user, attrs)
  end

  def password_reset_fixture(user, attrs) do
    password_reset = %MoranduCore.Accounts.PasswordReset{
      user_id: user.id,
      email: attrs[:email] || user.email,
      key: attrs[:key] || MoranduCore.Accounts.Auth.gen_key()
    }

    {:ok, password_reset} = Repo.insert(password_reset)
    password_reset
  end

  def user_fixture(attrs \\ %{}) do
    user_fixture(person_fixture(), attrs)
  end

  def user_fixture(person, attrs) do
    username = "user#{System.unique_integer([:positive])}"

    user = %MoranduCore.Accounts.User{
      email: attrs[:email] || "#{username}@example.com",
      username: attrs[:username] || "#{username}",
      commitments_person_id: person.id
    }

    {:ok, user} = Repo.insert(user)
    user
  end

  def person_fixture(attrs \\ %{}) do
    username = "person#{System.unique_integer([:positive])}"
    cedula = attrs[:cedula] || "#{rng(6)}"

    person = %MoranduCore.Commitments.Person{
      cedula: cedula,
      ruc: attrs[:ruc] || "#{cedula}-#{rng(1)}",
      name: attrs[:name] || username,
      phone: attrs[:phone] || "0#{rng(9)}"
    }

    {:ok, person} = Repo.insert(person)
    person
  end

  def case_fixture(attrs \\ %{}) do
    case_fixture(person_fixture(), person_fixture(), attrs)
  end

  def case_fixture(
        %MoranduCore.Commitments.Person{} = creditor,
        attrs
      ) do
    case_fixture(creditor, person_fixture(), attrs)
  end

  def case_fixture(
        %MoranduCore.Commitments.Person{} = creditor,
        %MoranduCore.Commitments.Person{} = debtor,
        attrs \\ %{}
      ) do

    file = Path.dirname(__ENV__.file) |> Path.join("../morandu_core/proof_test.png") |> File.read!()
    {:ok, file_uri} = Store.put("us-east-1", "proof00.morandu.samuu.srl", "proof_test.png", file)

    current_case = %MoranduCore.Commitments.Case{
      debtor_id: debtor.id,
      creditor_id: creditor.id,
      resolved_at: attrs[:resolved_at],
      signed_at: attrs[:signed_at] || Date.utc_today(),
      amount: attrs[:amount] || "#{rng(6)}.0" |> String.to_float(),
      verified_at: attrs[:verified_at],
      proof: attrs[:proof] || file_uri,
      proof_hash: attrs[:proof_hash] || "md5_814a0034f5549e957ee61360d87457e5"
    }

    {:ok, current_case} = Repo.insert(current_case)
    current_case
  end

  defp rng(length) do
    # credo:disable-for-lines:2
    (:rand.uniform() * :math.pow(10, length))
    |> Kernel.trunc()
    |> to_string
    |> String.pad_leading(length, "0")
  end
end
