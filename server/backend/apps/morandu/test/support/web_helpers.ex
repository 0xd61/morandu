defmodule MoranduWeb.TestHelpers do
  alias MoranduCore.Accounts
  alias Backoffice.Accounts.Users, as: InternalUsers
  alias MoranduCore.Commitments

  def bo_account_fixture(attrs \\ %{}) do
    username = "user#{System.unique_integer([:positive])}"
    password = attrs[:password] || "#{username}s very long password"

    {:ok, user} =
      attrs
      |> Enum.into(%{
        email: attrs[:email] || "#{username}@example.com",
        name: attrs[:name] || "#{username}",
        password: password
      })
      |> InternalUsers.new()

    %{user | password: password}
  end

  def person_fixture(attrs \\ %{}) do
    username = "person#{System.unique_integer([:positive])}"
    cedula = attrs[:cedula] || "#{rng(6)}"
    ruc = attrs[:ruc] || "#{cedula}-#{rng(1)}"

    {:ok, person} =
      attrs
      |> Enum.into(%{
        cedula_or_ruc: ruc || cedula,
        cedula: cedula,
        ruc: ruc,
        name: attrs[:name] || username,
        phone: attrs[:phone] || "0#{rng(9)}"
      })
      |> Commitments.Persons.new_checked()

    person
  end

  def user_fixture(attrs \\ %{}) do
    user_fixture(person_fixture(), attrs)
  end

  def user_fixture(%Commitments.Person{} = creditor, attrs) do
    username = "user#{System.unique_integer([:positive])}"

    attrs =
      attrs
      |> Enum.into(%{
        email: attrs[:email] || "#{username}@example.com",
        username: attrs[:username] || "#{username}",
        password: attrs[:password] || "#{username}s password"
      })

    {:ok, user} = Accounts.Users.new(creditor, attrs)

    Accounts.Users.set_role(user, attrs[:permissions] || 0xFF)
  end

  def key_fixture(attrs \\ %{}) do
    user = user_fixture()
    key_fixture(user, attrs)
  end

  def key_fixture(user, attrs) do
    {:ok, key} = Accounts.Keys.new(user, attrs)

    key
  end

  def case_fixture(attrs \\ %{}) do
    user = user_fixture()
    case_fixture(user, attrs)
  end

  def case_fixture(user, attrs) do
    cedula = attrs[:cedula] || "#{rng(6)}"

    client = %{
      used_at: DateTime.utc_now(),
      user_agent: ["#{cedula}-Chrome"],
      ip: {127, 0, 0, 1}
    }

    params = %{
      cedula_or_ruc: cedula,
      name: attrs[:name] || "name#{cedula}",
      signed_at: attrs[:signed_at] || Date.utc_today(),
      amount: attrs[:amount] || "#{rng(6)}.0" |> String.to_float(),
      phone: "0#{rng(9)}",
      # PDF File header with some bytes
      proof: attrs[:proof] || <<0x25, 0x50, 0x44, 0x46, 0x2D, 0x31, 0x2E, 0x35>>
    }

    creditor = Commitments.Persons.get_by!(id: user.commitments_person_id)
    {:ok, current_case} = Commitments.Cases.submit(user, creditor, client, params)

    if attrs[:resolved_at], do: Commitments.Cases.resolve!(user, client, current_case)

    Commitments.Cases.get!(current_case.id, [:debtor, :creditor])
  end

  defp rng(length) do
    # credo:disable-for-lines:2
    (:rand.uniform() * :math.pow(10, length))
    |> Kernel.trunc()
    |> to_string
    |> String.pad_leading(length, "0")
  end
end
