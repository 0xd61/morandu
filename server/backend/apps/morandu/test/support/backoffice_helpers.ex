defmodule Backoffice.TestHelpers do
  alias MoranduCore.Repo

  def user_fixture(attrs \\ %{}) do
    username = "user#{System.unique_integer([:positive])}"
    password = attrs[:password] || "verysecureandlongpassword"

    user = %Backoffice.Accounts.User{
      email: attrs[:email] || "#{username}@example.com",
      name: attrs[:username] || "#{username}",
      password_hash: Bcrypt.hash_pwd_salt(password)
    }

    {:ok, user} = Repo.insert(user)
    user
  end
end
