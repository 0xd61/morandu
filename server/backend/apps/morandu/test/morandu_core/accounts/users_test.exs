defmodule MoranduCore.Accounts.UsersTest do
  use MoranduCore.DataCase
  alias MoranduCore.Accounts.{Users, User, Key}
  use MoranduCore.Permission.Actions

  @password "mysecretpassword"

  @valid_attrs %{
    email: "someone@example.com",
    username: "some name",
    password: @password
  }

  @update_attrs %{
    email: "another@example.com",
    username: "some updated name",
    password: "new password"
  }

  @invalid_attrs %{email: "invalid email", username: nil, password: nil}

  describe "CRUD User" do
    setup [:person, :user]

    test "new/1 with valid attrs creates new user", %{person: person} do
      assert {:ok, user} = Users.new(person, @valid_attrs)
      assert user.email == @valid_attrs.email
      assert user.username == @valid_attrs.username
      assert user.commitments_person_id == person.id
      assert %Key{type: :password} = user.password_hash
    end

    test "new/1 with invalid attrs returns changeset", %{person: person} do
      assert {:error, %Ecto.Changeset{}} = Users.new(person, @invalid_attrs)
    end

    test "get!/1 returns the user with given id", %{user: user} do
      assert Users.get!(user.id, :password_hash) == user
    end

    test "get_by!/1 returns the user with given email", %{user: user} do
      assert Users.get_by!([email: user.email], :password_hash) == user

      assert_raise Ecto.NoResultsError, fn ->
        Users.get_by!(email: "email@doesnotexist.com")
      end
    end

    test "get_by_/1 returns the user with given email", %{user: user} do
      assert Users.get_by([email: user.email], :password_hash) == user
      assert Users.get_by(email: "email@doesnotexist.com") == nil
    end

    test "list_by/1 returns list of all users", %{user: user} do
      assert Users.list_by([], :password_hash) == [user]
    end

    test "list_by_ids/2 returns list of all users in ids", %{user: user} do
      assert Users.list_by_ids([user.id], :password_hash) == [user]
    end

    test "change/1 returns a user changeset", %{user: user} do
      assert %Ecto.Changeset{} = Users.change(user)
    end

    test "update/2 with valid data updates the user", %{user: user} do
      assert {:ok, %User{} = user_new} = Users.update(user, @update_attrs)
      assert user_new.email == @update_attrs.email
      assert user_new.username == @update_attrs.username
    end

    test "update/2 with invalid data returns error changeset", %{user: user} do
      assert {:error, %Ecto.Changeset{}} = Users.update(user, @invalid_attrs)
      assert user == Users.get!(user.id, :password_hash)
    end

    test "delete/1 deletes the user", %{user: user} do
      assert {:ok, %User{}} = Users.delete(user)
      assert_raise Ecto.NoResultsError, fn -> Users.get!(user.id, :password_hash) end
    end
  end

  describe "permissions" do
    setup [:person, :user]

    test "set_role/2 with integer sets permission flags", %{user: user} do
      Users.set_role(user, Actions.read())
      %{permissions: permissions} = Users.get!(user.id)
      assert permissions == Actions.read()
    end
  end

  defp user(%{person: person}) do
    user = user_fixture(person, %{}) |> Repo.preload(:password_hash)
    {:ok, user: user}
  end

  defp person(_) do
    person = person_fixture()
    {:ok, person: person}
  end
end
