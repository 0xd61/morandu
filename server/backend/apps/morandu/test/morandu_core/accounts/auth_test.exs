defmodule MoranduCore.Accounts.AuthTest do
  use MoranduCore.DataCase
  alias MoranduCore.Accounts.{Auth, Keys}

  @password "mysecretpassword"
  @usage_info %{
    used_at: DateTime.utc_now(),
    user_agent: ["Chrome"],
    ip: {127, 0, 0, 1}
  }

  describe "via_password/3" do
    setup [:user]

    test "authenticates user by password", %{user: user, password: password} do
      assert {:ok, %{key: key, user: _user, source: :password}} =
               Auth.via_password(user.email, password.user_secret, @usage_info)

      assert key.user_id == user.id
    end

    test "returns revoked if password is revoked", %{user: user, password: password} do
      Keys.revoke(password)

      assert {:error, :revoked} =
               Auth.via_password(user.email, password.user_secret, @usage_info)
    end

    test "returns error, if not authenticated", %{user: user} do
      assert {:error, :unauthorized} =
               Auth.via_password(user.email, "invalid_password", @usage_info)
    end

    test "returns not found if user does not exist" do
      assert {:error, :not_found} =
               Auth.via_password("not@existing.com", "some password", @usage_info)
    end
  end

  describe "via_key/3" do
    setup [:user, :key]

    test "authenticates user by key", %{user: user, key: key} do
      assert {:ok, %{key: key, user: _user, source: :key}} =
               Auth.via_key(key.user_secret, @usage_info)

      assert key.user_id == user.id
    end

    test "returns revoked if key is revoked", %{key: key} do
      Keys.revoke(key)
      assert {:error, :revoked} = Auth.via_key(key.user_secret, @usage_info)
    end

    test "returns error, if not authenticated" do
      assert {:error, :not_found} = Auth.via_key("invalid_key", @usage_info)
    end
  end

  defp user(_) do
    user = user_fixture()

    password =
      key_fixture(user, %{
        type: :password,
        user_secret: @password,
        secret_first: Bcrypt.hash_pwd_salt(@password)
      })

    {:ok, user: user, password: password}
  end

  defp key(%{user: user}) do
    key = key_fixture(user, %{})

    {:ok, key: key}
  end
end
