defmodule MoranduCore.Commitments.SubmissionWorkflowTest do
  use MoranduCore.DataCase

  alias MoranduCore.Commitments.SubmissionWorkflow, as: Submit
  alias MoranduCore.Commitments.Case
  alias MoranduCore.Commitments.Audit
  alias MoranduCore.Commitments.{Persons, PersonChange}
  alias MoranduCore.Repo

  @valid_attrs %{
    cedula_or_ruc: "432579-8",
    name: "Firstname Lastname",
    phone: "0123456789",
    amount: 20000.0,
    proof: Path.dirname(__ENV__.file) |> Path.join("../proof_test.png") |> File.read!()
  }

  @invalid_attrs %{cedula: nil, name: nil, phone: nil, proof: nil}

  @client_info %{
    used_at: DateTime.utc_now(),
    user_agent: ["Chrome"],
    ip: {127, 0, 0, 1}
  }

  setup [:creditor, :debtor, :user]

  describe "run/4 changeset checks" do
    test "returns changeset if proof filetype is not supported", %{user: user, creditor: creditor} do
      attrs = %{@valid_attrs | proof: <<0x2C, 0x3F, 0x78, 0x6D, 0x6C, 0x20, 0x76, 0x65>>}
      assert {:error, %Ecto.Changeset{}} = Submit.run(user, creditor, @client_info, attrs)
    end

    test "with invalid data returns changeset", %{user: user, creditor: creditor} do
      assert {:error, %Ecto.Changeset{}} = Submit.run(user, creditor, @client_info, @invalid_attrs)
    end

    test "is valid if cedula/ruc matches name", %{user: user, debtor: debtor, creditor: creditor} do
      assert debtor.name == @valid_attrs.name
      assert {:ok, %Case{}} = Submit.run(user, creditor, @client_info, @valid_attrs)
    end

    test "is invalid if cedula/ruc does not match name", %{user: user, creditor: creditor} do
      attrs = %{@valid_attrs | name: "some invalid name"}
      assert {:error, %Ecto.Changeset{}} = Submit.run(user, creditor, @client_info, attrs)
    end
  end

  describe "run/4 workflow checks" do
    test "creates a new case", %{user: user, creditor: creditor, debtor: debtor} do
      assert {:ok, %Case{} = current_case} = Submit.run(user, creditor, @client_info, @valid_attrs)
      assert current_case.creditor_id == creditor.id
      assert current_case.debtor_id == debtor.id
      assert current_case.signed_at == Date.utc_today()
    end

    test "creates audit data", %{user: user, creditor: creditor} do
      assert {:ok, %Case{id: id, current_audit_id: audit_id}} =
               Submit.run(user, creditor, @client_info, @valid_attrs)

      assert %{params: %{"id" => ^id}} = Repo.get_by(Audit, action: "case.submit")
      assert %{id: ^audit_id, params: %{"id" => ^id}} = Repo.get_by(Audit, action: "case.verify")
    end

    test "creates debtor if not exists", %{user: user, debtor: debtor, creditor: creditor} do
      attrs = %{@valid_attrs | cedula_or_ruc: "444333"}
      assert {:ok, %Case{debtor: new_debtor}} = Submit.run(user, creditor, @client_info, attrs)
      assert new_debtor.id != debtor.id && new_debtor.id != nil
      assert new_debtor.is_verified == false
    end

    test "updates debtor if exists and stores changes", %{user: user, debtor: debtor, creditor: creditor} do
      attrs = %{@valid_attrs | name: "Firstname2 Lastname2"}
      assert {:ok, %Case{debtor_id: id, debtor: new_debtor}} = Submit.run(user, creditor, @client_info, attrs)

      assert id == debtor.id
      assert new_debtor.name == "Firstname2 Lastname2"

      assert [%PersonChange{person_id: ^id, field: :name, value: old_value, new_value: new_value}] = Repo.all(PersonChange)
      assert old_value == @valid_attrs.name
      assert new_value == new_debtor.name
    end

    test "creating a new debtor without cedula/ruc lookup sets is_verified false", %{user: user, creditor: creditor} do
      attrs = %{@valid_attrs | cedula_or_ruc: "444333"}
      assert {:ok, %Case{debtor: debtor}} = Submit.run(user, creditor, @client_info, attrs)
      assert debtor.is_verified == false
    end

    # NOTE: if a case with a person is created more than once, we set
    # is_verified to true
    test "creating a case with existing debtor with cedula/ruc lookup sets is_verified true", %{user: user, creditor: creditor} do
      attrs = %{@valid_attrs | cedula_or_ruc: "444333"}
      assert {:ok, %Case{debtor: debtor}} = Submit.run(user, creditor, @client_info, attrs)
      assert {:ok, %Case{debtor: debtor}} = Submit.run(user, creditor, @client_info, attrs)
      assert debtor.is_verified == true
    end

    test "uploads proof and puts proof uri and hash in case", %{user: user, creditor: creditor} do
      assert {:ok, %Case{proof: proof, proof_hash: hash} = current_case} =
               Submit.run(user, creditor, @client_info, @valid_attrs)

      assert hash === "md5_814a0034f5549e957ee61360d87457e5"
      assert {:ok, file} = File.read(proof)

      [type, sum] = hash |> String.split("_")
      assert String.to_atom(type) |> :crypto.hash(file) |> Base.encode16(case: :lower) === sum
    end
  end

  defp user(%{creditor: person}) do
    user = user_fixture(person, %{})
    {:ok, user: user}
  end

  defp creditor(_) do
    person = person_fixture()
    {:ok, creditor: person}
  end

  defp debtor(_) do
    # needs to go though changeset for cedula parsing
    {:ok, person} = Persons.new_checked(@valid_attrs)
    {:ok, debtor: person}
  end
end
