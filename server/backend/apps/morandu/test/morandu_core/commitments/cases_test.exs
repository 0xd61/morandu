defmodule MoranduCore.Commitments.CasesTest do
  use MoranduCore.DataCase
  alias MoranduCore.Commitments.Cases
  alias MoranduCore.Commitments.{Audit, Audits, Person}
  alias MoranduCore.Repo

  @client_info %{
    used_at: DateTime.utc_now(),
    user_agent: ["Chrome"],
    ip: {127, 0, 0, 1}
  }

  describe "CRUD Cases" do
    setup [:cases]

    test "get!/2 retuns case by id", %{cases: current_case} do
      assert Cases.get!(current_case.id) == current_case
    end
  end

  describe "verify!/3" do
    setup [:user, :cases]

    test "verifies case", %{user: user, cases: current_case} do
      assert current_case.verified_at == nil
      %{verified_at: now} = Cases.verify!(user, @client_info, current_case)
      assert now != nil
    end

    test "creates audit entry", %{user: user, cases: current_case} do
      %{id: id} = Cases.verify!(user, @client_info, current_case)
      user_id = user.id

      assert %{user_id: ^user_id, params: %{"id" => ^id}} =
               Repo.get_by(Audit, action: "case.verify")
    end
  end

  describe "resolve!/3" do
    setup [:user, :cases]

    test "resolves case", %{user: user, cases: current_case} do
      assert current_case.resolved_at == nil
      %{resolved_at: now} = Cases.resolve!(user, @client_info, current_case)
      assert now != nil
    end

    test "creates audit entry", %{user: user, cases: current_case} do
      %{id: id} = Cases.resolve!(user, @client_info, current_case)
      user_id = user.id

      assert %{user_id: ^user_id, params: %{"id" => ^id}} =
               Repo.get_by(Audit, action: "case.resolve")
    end
  end

  describe "get/store_proof" do
    setup [:person, :cases]

    test "get_proof/1 returns proof from storage", %{cases: current_case} do
      file = Path.dirname(__ENV__.file) |> Path.join("../proof_test.png") |>  File.read!()
      assert {:ok, ^file, _key} = Cases.get_proof(current_case)
    end

    test "get_proof/1 returns error on hash mismatch", %{cases: current_case} do
      case_ = %{current_case | proof_hash: "md5_invalidhash"}
      assert {:error, :hash_mismatch} = Cases.get_proof(case_)
    end

    test "store_proof/3 stores proof in storage", %{person: person} do
      file = <<0x89, 0x50, 0x4e, 0x47, 0x0d, 0x0a, 0x1a, 0x0a>>
      assert {:ok, %{uri: uri, hash: hash}} = Cases.store_proof(person, person, file)

      assert File.exists?(uri) == true
      [type, hash] = String.split(hash, "_")
      assert :crypto.hash(String.to_atom(type), file) |> Base.encode16(case: :lower) === hash
    end

    test "store_proof/3 returns error on invalid filetype", %{person: person} do
      file = <<0x2c, 0x3f, 0x78, 0x6d, 0x6c, 0x20, 0x76, 0x65>>
      assert {:error, _} = Cases.store_proof(person, person, file)
    end
  end

  describe "list_active_by/2" do
    setup [:user, :verified, :resolved]

    test "lists only verified and not resolved cases", %{verified: verified_case} do
      assert Cases.list_active_by() == [verified_case]
    end

    test "lists only verified and not resolved cases by creditor" do
      creditor = person_fixture()
      active = case_fixture(creditor, %{verified_at: DateTime.utc_now()})
      assert Cases.list_active_by(creditor_id: creditor.id) == [active]
    end

    test "lists only verified and not resolved cases by debtor", %{verified: verified} do
      debtor = Repo.get(Person, verified.debtor_id)
      assert Cases.list_active_by(debtor_id: debtor.id) == [verified]
    end
  end

  describe "list_changed_since/2" do
    setup [:cases, :verified, :resolved]

    test "lists *insert* cases since an audit id", %{
      verified: verified_case,
      verified_audit: audit,
      resolved_audit: last_audit
    } do
      assert Cases.list_changed_since(audit.id - 1) == {[verified_case], last_audit.id}
    end

    test "lists *delete* cases since an audit id", %{
      resolved: resolved_case,
      resolved_audit: audit
    } do
      assert Cases.list_changed_since(audit.id - 1) == {[resolved_case], audit.id}
    end
  end

  defp user(_) do
    user = user_fixture()
    {:ok, user: user}
  end

  defp person(_) do
    person = person_fixture()
    {:ok, person: person}
  end

  defp cases(_) do
    current_case = case_fixture()
    {:ok, cases: current_case}
  end

  defp verified(_) do
    user = user_fixture()
    current_case = case_fixture(%{verified_at: DateTime.utc_now()})

    {:ok, %{"case.verify": audit}} =
      Ecto.Multi.new()
      |> Audits.add(user, @client_info, "case.verify", current_case)
      |> Repo.transaction()

    {:ok, verified: current_case, verified_audit: audit}
  end

  defp resolved(_) do
    user = user_fixture()

    current_case =
      case_fixture(%{verified_at: DateTime.utc_now(), resolved_at: DateTime.utc_now()})

    {:ok, %{"case.resolve": audit}} =
      Ecto.Multi.new()
      |> Audits.add(user, @client_info, "case.verify", current_case)
      |> Audits.add(user, @client_info, "case.resolve", current_case)
      |> Repo.transaction()

    {:ok, resolved: current_case, resolved_audit: audit}
  end
end
