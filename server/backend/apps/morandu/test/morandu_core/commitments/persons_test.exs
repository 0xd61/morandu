defmodule MoranduCore.Commitments.PersonsTest do
  use MoranduCore.DataCase

  alias MoranduCore.Commitments.Persons

  @valid_attrs %{
    name: "some name",
    cedula_or_ruc: "432547-9",
    phone: "0982134525"
  }

  @update_attrs %{
    name: "new name",
    cedula: "123456",
    ruc: "654321-0",
    phone: "0123456789"
  }
  @invalid_attrs %{name: nil, cedula: nil, cedula_or_ruc: nil, ruc: nil, phone: nil}

  describe "CRUD Person" do
    setup [:person, :user]

    test "new_checked/2 with valid attrs creates new person" do
      assert {:ok, person} = Persons.new_checked(@valid_attrs)
      assert person.name == @valid_attrs.name
      assert person.cedula == @valid_attrs.cedula_or_ruc |> String.split("-") |> List.first()
      assert person.ruc == @valid_attrs.cedula_or_ruc
      assert person.phone == @valid_attrs.phone
      assert person.is_verified == true
    end

    test "new_checked/2 with invalid attrs returns changeset" do
      assert {:error, %Ecto.Changeset{}} = Persons.new_checked(@invalid_attrs)
    end

    test "get/2 returns the user by cedula or ruc", %{person: person} do
      assert Persons.get(person.cedula) == person
      assert Persons.get(person.ruc) == person
    end

    test "get_by/2 and get_by!/2 returns the user by params", %{person: person} do
      assert Persons.get_by!(name: person.name) == person
      assert Persons.get_by(name: person.name) == person

      assert_raise Ecto.NoResultsError, fn ->
        Persons.get_by!(name: "unknown name")
      end

      assert Persons.get_by(name: "unknown name") == nil
    end

    test "get_by!/1 returns the person with given accounts_user_id", %{
      user: user,
      person: person
    } do
      assert Persons.get_by!(id: user.commitments_person_id) == person
    end

    test "change/1 returns a person changeset", %{person: person} do
      assert %Ecto.Changeset{} = Persons.change(person)
    end

    test "update_checked/2 with valid data updates the person", %{person: person} do
      assert {:ok, person_new} = Persons.update_checked(person, @update_attrs)
      assert person_new.name == @update_attrs.name
      assert person_new.cedula == @update_attrs.cedula
      assert person_new.phone == @update_attrs.phone
      assert person_new.is_verified == true
    end

    test "update_checked/2 with invalid data returns error changeset", %{person: person} do
      assert {:error, %Ecto.Changeset{}} = Persons.update_checked(person, @invalid_attrs)
    end

    test "delete/1 deletes the person", %{person: person} do
      assert {:ok, _} = Persons.delete(person)
      assert_raise Ecto.NoResultsError, fn -> Persons.get_by!(id: person.id) end
    end
  end

  describe "search/1" do
    setup [:person, :user]

    test "returns persons by cedula", %{person: person} do
      assert Persons.search(person.cedula) == [person]
      assert Persons.search("123456") == []
    end

    test "returns persons by company name", %{person: person} do
      assert Persons.search(person.name) == [person]
      assert Persons.search("anonymous") == []
    end
  end

  defp user(%{person: person}) do
    user = user_fixture(person, %{})
    {:ok, user: user}
  end

  defp person(_) do
    person = person_fixture()
    {:ok, person: person}
  end
end
