defmodule MoranduCore.Commitments.AuditsTest do
  use MoranduCore.DataCase
  alias Ecto.Multi
  alias MoranduCore.Repo
  alias MoranduCore.Commitments.Audits
  alias MoranduCore.Commitments.Audit

  setup [:person, :user, :cases]

  @client_info %{
    used_at: DateTime.utc_now(),
    user_agent: ["Chrome"],
    ip: {127, 0, 0, 1}
  }

  test "add/5 creates new audit with multi", %{user: user, cases: current_case} do
    assert {:ok, %{"case.submit": %Audit{id: id}}} =
             Multi.new()
             |> Audits.add(user, @client_info, "case.submit", current_case)
             |> Repo.transaction()

    audit = Repo.get!(Audit, id)

    assert audit.user_module == user.__struct__
    assert audit.user_id == user.id
    assert audit.params["id"] == current_case.id
  end

  test "add/5 creates new audit with params by fun", %{user: user, cases: current_case} do
    assert {:ok, %{"case.submit": %Audit{id: id}}} =
             Multi.new()
             |> Audits.add(user, @client_info, "case.submit", fn _ -> current_case end)
             |> Repo.transaction()

    audit = Repo.get!(Audit, id)

    assert audit.user_module == user.__struct__
    assert audit.user_id == user.id
    assert audit.params["id"] == current_case.id
  end

  test "maybe_add/5 creates new audit if fun returns :ok", %{user: user, cases: current_case} do
    assert {:ok, %{"case.submit": %Audit{id: id}}} =
             Multi.new()
             |> Audits.maybe_add(user, @client_info, "case.submit", fn _ ->
               {:ok, current_case}
             end)
             |> Repo.transaction()

    audit = Repo.get!(Audit, id)

    assert audit.user_module == user.__struct__
    assert audit.user_id == user.id
    assert audit.params["id"] == current_case.id
  end

  test "maybe_add/5 does nothing if fun retuns not :ok", %{user: user, cases: current_case} do
    assert {:ok, %{}} =
             Multi.new()
             |> Audits.maybe_add(user, @client_info, "case.submit", fn _ ->
               {:error, current_case}
             end)
             |> Repo.transaction()
  end

  defp user(%{person: person}) do
    user = user_fixture(person, %{})
    {:ok, user: user}
  end

  defp person(_) do
    person = person_fixture()
    {:ok, person: person}
  end

  defp cases(%{person: person}) do
    current_case = case_fixture(person, %{})
    {:ok, cases: current_case}
  end
end
