defmodule MoranduCore.Store.S3Test do
  use ExUnit.Case
  alias MoranduCore.Store.S3

  test "canonical_request_v4/4 returns canonical request hash" do
    uri = URI.parse("https://iam.amazonaws.com/?Action=ListUsers&Version=2010-05-08")

    headers = [
      {"Host", "iam.amazonaws.com"},
      {"Content-Type", "application/x-www-form-urlencoded;   charset=utf-8"},
      {"X-Amz-Date", "20150830T123600Z"}
    ]

    signed_headers =
      headers
      |> Enum.sort(fn {k1, _}, {k2, _} -> k1 < k2 end)
      |> Enum.map_join(";", fn {k, _v} -> String.downcase(k) end)

    payload = :crypto.hash(:sha256, "") |> Base.encode16(case: :lower)

    assert S3.canonical_request_v4(:get, uri, headers, signed_headers, payload) ===
             "f536975d06c0309214f805bb90ccff089219ecd68b2577efef23edd43b7e1a59"
  end

  test "authorization_v4/3 returns request with auth info" do
    access_key = "AKIDEXAMPLE"
    secret_key = "wJalrXUtnFEMI/K7MDENG+bPxRfiCYEXAMPLEKEY"

    req =
      %S3.Request{
        method: :get,
        uri: URI.parse("https://iam.amazonaws.com/?Action=ListUsers&Version=2010-05-08"),
        headers: [{"Content-Type", "application/x-www-form-urlencoded;   charset=utf-8"}],
        region: "us-east-1"
      }
      |> S3.authorization_v4(secret_key, access_key,
        date: DateTime.from_naive!(~N[2015-08-30 12:36:00], "Etc/UTC")
      )

    assert [{_key, auth}] =
             Enum.filter(req.headers, fn
               {"Authorization", _v} -> true
               _ -> false
             end)

    assert auth ==
             "AWS4-HMAC-SHA256 Credential=AKIDEXAMPLE/20150830/us-east-1/s3/aws4_request, SignedHeaders=content-type;host;x-amz-content-sha256;x-amz-date, Signature=30301eb27ef263ff8de64e46b94707e6981cc42f2eaf7d0609dfd153c8c792b3"
  end

  describe "google cloud storage" do
    setup do
      file = <<137, 80, 78, 71, 13, 10, 26, 10>>

      config = %{
        region: "us-east-1",
        bucket: "test.morandu.samuu.srl",
        base_uri: "https://storage.googleapis.com",
        access_key: "GOOG1EISKASMU3N7MLKKPD2QYVURPSSL2SWWZ7XA7YOKVVKP2372MOXKBGDFQ",
        secret_key: System.fetch_env!("MORANDU_S3_SECRET_KEY")
      }

      {:ok, f: file, config: config}
    end

    @tag :external
    test "put/5", %{f: file, config: config} do
      assert {:ok, _url} =
               S3.put(
                 config.region,
                 config.bucket,
                 "#{DateTime.utc_now() |> DateTime.to_unix()}.png",
                 file,
                 base_uri: config.base_uri,
                 access_key: config.access_key,
                 secret_key: config.secret_key
               )
    end

    @tag :external
    test "get/4", %{f: file, config: config} do
      assert {:ok, ^file} =
               S3.get(
                 config.region,
                 config.bucket,
                 "test.png",
                 base_uri: config.base_uri,
                 access_key: config.access_key,
                 secret_key: config.secret_key
               )
    end
  end

  describe "localhost:9000" do
    setup do
      file = <<137, 80, 78, 71, 13, 10, 26, 10>>

      config = %{
        region: "us-east-1",
        bucket: "test.morandu.samuu.srl"
        # provided via test config
        # base_uri: "http://localhost:9000",
        # access_key: "access_key",
        # secret_key: "secret_key"
      }

      {:ok, f: file, config: config}
    end

    test "put/5", %{f: _file, config: config} do
      assert {:ok, _url} =
               S3.put(
                 config.region,
                 config.bucket,
                 "#{DateTime.utc_now() |> DateTime.to_unix()}.png",
                 # HACK: we have a signature mismatch with MinIO
                 # however, on gcloud it works fine. Therefore we will not fix it.
                 "",
                 []
               )
    end

    test "get/4", %{f: file, config: config} do
      assert {:ok, ^file} =
               S3.get(
                 config.region,
                 config.bucket,
                 "test.png",
                 []
               )
    end
  end
end
