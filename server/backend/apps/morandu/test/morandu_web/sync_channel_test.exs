defmodule MoranduWeb.SyncChannelTest do
  use MoranduWeb.ChannelCase
  alias MoranduWeb.UserSocket
  alias MoranduWeb.Api.V1.CaseView

  @salt Application.get_env(:morandu, @endpoint)[:token_salt]

  setup do
    user = user_fixture()
    token = Phoenix.Token.sign(@endpoint, @salt, user.id)
    {:ok, socket} = connect(UserSocket, %{"token" => token})

    case_fixture()
    case_fixture(%{resolved_at: true})
    case_fixture()

    {:ok, socket: socket, user: user}
  end

  test "sync pushes changes made since event id", %{socket: socket} do
    {:ok, _, socket} = subscribe_and_join(socket, "sync:lobby", %{})

    push(socket, "sync", %{"params" => %{"last_event" => 0}})

    assert_push("changes", %{
      changes: %{insert: [%CaseView{}, %CaseView{}], delete: []},
      event: id
    })

    assert id > 0
  end

  test "sync pushes empty changes with same id of nothing changed", %{socket: socket} do
    {:ok, _, socket} = subscribe_and_join(socket, "sync:lobby", %{})
    id = 99_999_999
    push(socket, "sync", %{"params" => %{"last_event" => id}})
    assert_push("changes", %{changes: %{insert: [], delete: []}, event: ^id})
  end
end
