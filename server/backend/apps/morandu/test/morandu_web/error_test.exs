defmodule MoranduWeb.ErrorViewTest do
  use MoranduWeb.ConnCase, async: true
  import Phoenix.ConnTest, except: [conn: 0]

  import Phoenix.View
  @view MoranduWeb.ErrorView

  test "renders 400.html" do
    assert render_to_string(@view, "400.html", conn: conn()) =~ "Bad request"
  end

  test "renders 404.html" do
    assert render_to_string(@view, "404.html", conn: conn()) =~ "Page not found"
  end

  test "renders 408.html" do
    assert render_to_string(@view, "408.html", conn: conn()) =~ "Request timeout"
  end

  test "renders 413.html" do
    assert render_to_string(@view, "413.html", conn: conn()) =~ "Payload too large"
  end

  test "renders 415.html" do
    assert render_to_string(@view, "415.html", conn: conn()) =~
             "Unsupported media type"
  end

  test "renders 422.html" do
    assert render_to_string(@view, "422.html", conn: conn()) =~ "Validation error(s)"
  end

  test "render 500.html" do
    assert render_to_string(@view, "500.html", conn: conn()) =~
             "Internal server error"
  end

  test "render any other" do
    render_to_string(@view, "505.html", conn: conn()) =~
      "Internal server error"
  end

  defp conn() do
    put_in(build_conn().private[:phoenix_flash], %{})
  end
end
