defmodule MoranduWeb.PlugsTest do
  use MoranduWeb.ConnCase, async: true
  alias MoranduWeb.Plugs
  alias MoranduCore.Accounts
  import Phoenix.ConnTest, except: [conn: 0]

  setup %{conn: conn} do
    conn =
      conn
      |> bypass_through(MoranduWeb.Router, :browser)
      |> get("/")

    {:ok, %{conn: conn}}
  end

  test "user_agent/2 puts user agent in conn", %{conn: conn} do
    conn =
      conn
      |> Plugs.user_agent([])

    assert conn.assigns.user_agent === "missing"
  end

  describe "login_session/2" do
    test "places user form session into assigns", %{conn: conn} do
      user = user_fixture()

      conn =
        conn
        |> put_session(:user_id, user.id)
        |> Plugs.login_session(user_fun: &Accounts.Users.get!/1)

      assert conn.assigns.current_user.id == user.id
    end

    test "with no session sets current_user assign to nil", %{conn: conn} do
      conn =
        conn
        |> Plugs.login_session(user_fun: &Accounts.Users.get!/1)

      assert conn.assigns.current_user == nil
    end
  end

  describe "authenticate/2" do
    test "checks authentication and places user into assigns", %{conn: conn} do
      user = user_fixture()
      {:ok, key} = Accounts.Keys.new(user)

      conn =
        conn
        |> Plug.Conn.put_req_header("authorization", "Bearer #{key.user_secret}")
        |> Plugs.authenticate([])

      assert conn.assigns.key.id == key.id
      assert conn.assigns.current_user.id == user.id
    end

    test "with existing session (current user in assigns) places user into assigns", %{conn: conn} do
      user = user_fixture()

      conn =
        conn
        |> put_session(:user_id, user.id)
        |> Plugs.login_session(user_fun: &Accounts.Users.get!/1)
        |> Plugs.authenticate([])

      assert conn.assigns[:key] == nil
      assert conn.assigns.current_user.id == user.id
    end

    test "with invalid authentication halts connection with error message", %{
      conn: conn
    } do
      conn =
        conn
        |> Plug.Conn.put_req_header("authorization", "Bearer invalid")
        |> Plugs.authenticate([])

      assert html_response(conn, 401) =~ "invalid API key"
    end

    test "without auth header sets current user and key to nil", %{
      conn: conn
    } do
      conn =
        conn
        |> Plugs.authenticate([])

      assert conn.assigns.key == nil
      assert conn.assigns.current_user == nil
    end
  end

  describe "localize/2" do
    test "use locale from parameters", %{conn: conn} do
      conn
      |> get("/api/v1/users/?lang=es")

      assert Fluex.get_locale(MoranduWeb.Fluex) == "es"
    end

    test "use fallback language if nothing provided", %{conn: conn} do
      conn
      |> get("/api/v1/users")

      assert Fluex.get_locale(MoranduWeb.Fluex) == "en"
    end
  end
end
