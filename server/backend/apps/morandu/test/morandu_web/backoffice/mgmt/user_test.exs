defmodule MoranduWeb.Backoffice.Mgmt.UserControllerTest do
  use MoranduWeb.ConnCase

  @create_attrs %{
    email: "test@example.com",
    name: "Example User",
    password: "somesecureandlongpassword"
  }

  @update_attrs %{
    email: "new@email.com",
    name: "Updated Name",
    password: "mynewsecureandlongpassword"
  }

  @invalid_attrs %{
    email: nil,
    name: nil,
    password: nil
  }

  describe "logged out users" do
    test "requires user authentication on actions", %{conn: conn} do
      Enum.each(
        [
          get(conn, Routes.backoffice_mgmt_user_path(conn, :index)),
          get(conn, Routes.backoffice_mgmt_user_path(conn, :new)),
          post(conn, Routes.backoffice_mgmt_user_path(conn, :create)),
          get(conn, Routes.backoffice_mgmt_user_path(conn, :edit, 1)),
          put(conn, Routes.backoffice_mgmt_user_path(conn, :update, 1)),
          delete(conn, Routes.backoffice_mgmt_user_path(conn, :delete, 1))
        ],
        fn conn ->
          assert html_response(conn, 401) =~ "unauthorized"
          assert conn.halted
        end
      )
    end
  end

  describe "index" do
    setup [:user, :login]

    test "lists all users", %{conn: conn} do
      conn = get(conn, Routes.backoffice_mgmt_user_path(conn, :index))
      assert html_response(conn, 200) =~ "Listing Backoffice Users"
    end
  end

  describe "new user" do
    setup [:user, :login]

    test "renders form", %{conn: conn} do
      conn = get(conn, Routes.backoffice_mgmt_user_path(conn, :new))
      assert html_response(conn, 200) =~ "New User"
    end
  end

  describe "create user" do
    setup [:user, :login]

    test "redirects to show when data is valid", %{conn: conn} do
      conn = post(conn, Routes.backoffice_mgmt_user_path(conn, :create), params: @create_attrs)
      assert redirected_to(conn) == Routes.backoffice_mgmt_user_path(conn, :index)
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.backoffice_mgmt_user_path(conn, :create), params: @invalid_attrs)
      assert html_response(conn, 200) =~ "New User"
    end
  end

  describe "edit user" do
    setup [:user, :login]

    test "renders form for editing chosen user", %{conn: conn, user: user} do
      conn = get(conn, Routes.backoffice_mgmt_user_path(conn, :edit, user))
      assert html_response(conn, 200) =~ "Edit User"
    end
  end

  describe "update user" do
    setup [:user, :login]

    test "redirects when data is valid", %{conn: conn, user: user} do
      conn = put(conn, Routes.backoffice_mgmt_user_path(conn, :update, user), params: @update_attrs)
      assert redirected_to(conn) == Routes.backoffice_mgmt_user_path(conn, :index)

      conn = get(conn, Routes.backoffice_mgmt_user_path(conn, :index))
      assert html_response(conn, 200)
    end

    test "renders errors when data is invalid", %{conn: conn, user: user} do
      conn = put(conn, Routes.backoffice_mgmt_user_path(conn, :update, user), params: @invalid_attrs)
      assert html_response(conn, 200) =~ "Edit User"
    end
  end

  describe "delete user" do
    setup [:user, :login]

    test "deletes chosen user", %{conn: conn, user: user} do
      conn = delete(conn, Routes.backoffice_mgmt_user_path(conn, :delete, user))
      assert redirected_to(conn) == Routes.backoffice_mgmt_user_path(conn, :index)

      assert_error_sent 404, fn ->
        get(conn, Routes.backoffice_mgmt_user_path(conn, :edit, user))
      end
    end
  end

  def user(_) do
    user = bo_account_fixture()

    {:ok, user: user}
  end

  def login(%{conn: conn, user: user}) do
    conn =
      conn
      |> init_test_session(user_id: user.id)
      |> assign(:current_user, user)

    {:ok, conn: conn}
  end
end
