defmodule MoranduWeb.Backoffice.Mgmt.SessionControllerTest do
  use MoranduWeb.ConnCase, async: true

  describe "logged out users" do
    test "requires user authentication on actions", %{conn: conn} do
      Enum.each(
        [
          delete(conn, Routes.backoffice_mgmt_session_path(conn, :delete))
        ],
        fn conn ->
          assert html_response(conn, 401) =~ "unauthorized"
          assert conn.halted
        end
      )
    end
  end

  test "new", %{conn: conn} do
    conn = get(conn, Routes.backoffice_mgmt_session_path(conn, :new))
    assert html_response(conn, 200) =~ "Welcome"
  end

  describe "create" do
    setup [:user]

    test "redirects in user if email and password is correct", %{conn: conn, user: user} do
      conn =
        post(conn, Routes.backoffice_mgmt_session_path(conn, :create),
          params: %{email: user.email, password: user.password}
        )

      # Remove unencrypted password
      user = %{user | password: nil}
      assert redirected_to(conn) == Routes.backoffice_page_path(conn, :index)
      assert conn.assigns.current_user == user
    end

    test "renders error if email or password is incorrect", %{conn: conn, user: user} do
      conn =
        post(
          conn,
          Routes.backoffice_mgmt_session_path(conn, :create),
          params: %{email: user.email, password: "invalid password"}
        )

      assert html_response(conn, 200) =~ "Invalid email/password"
    end
  end

  describe "delete" do
    setup [:user, :login]

    test "revokes user auth key", %{conn: conn} do
      conn = delete(conn, Routes.backoffice_mgmt_session_path(conn, :delete))

      assert private:
               %Plug.Conn{
                 private: %{
                   plug_session_info: :drop
                 }
               } = conn

      assert redirected_to(conn) == Routes.backoffice_mgmt_session_path(conn, :new)
    end
  end

  def user(_) do
    user = bo_account_fixture()

    {:ok, user: user}
  end

  def login(%{conn: conn, user: user}) do
    conn =
      conn
      |> init_test_session(user_id: user.id)
      |> assign(:current_user, user)

    {:ok, conn: conn}
  end
end
