defmodule MoranduWeb.Backoffice.PageControllerTest do
  use MoranduWeb.ConnCase, async: true

  describe "logged out users" do
    test "requires user authentication on actions", %{conn: conn} do
      Enum.each(
        [
          get(conn, Routes.backoffice_page_path(conn, :index))
        ],
        fn conn ->
          assert redirected_to(conn) =~ Routes.backoffice_mgmt_session_path(conn, :new)
          assert conn.halted
        end
      )
    end
  end
end
