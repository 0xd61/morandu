defmodule MoranduWeb.Backoffice.PersonControllerTest do
  use MoranduWeb.ConnCase, async: true

  @create_attrs %{
    name: "Some Name",
    cedula_or_ruc: "666666-7",
    phone: "0987123456"
  }

  @update_attrs %{
    name: "new name",
    cedula: "987654",
    ruc: "987654-9",
    phone: "0987654322"
  }

  @invalid_attrs %{
    name: nil,
    cedula: nil,
    ruc: nil,
    phone: nil
  }

  describe "logged out users" do
    test "requires user authentication on actions", %{conn: conn} do
      Enum.each(
        [
          get(conn, Routes.backoffice_person_path(conn, :index)),
          get(conn, Routes.backoffice_person_path(conn, :export_csv)),
          get(conn, Routes.backoffice_person_path(conn, :new)),
          post(conn, Routes.backoffice_person_path(conn, :create)),
          get(conn, Routes.backoffice_person_path(conn, :show, 1)),
          get(conn, Routes.backoffice_person_path(conn, :edit, 1)),
          put(conn, Routes.backoffice_person_path(conn, :update, 1))
        ],
        fn conn ->
          assert html_response(conn, 401) =~ "unauthorized"
          assert conn.halted
        end
      )
    end
  end

  describe "index" do
    setup [:login]

    test "show liveview search form", %{conn: conn} do
      conn = get(conn, Routes.backoffice_person_path(conn, :index))
      assert html_response(conn, 200) =~ "PersonSearchLive"
    end
  end

  describe "new" do
    setup [:login]

    test "renders form", %{conn: conn} do
      conn = get(conn, Routes.backoffice_person_path(conn, :new))
      assert html_response(conn, 200) =~ "New Person"
    end
  end

  describe "create" do
    setup [:login]

    test "redirects to show when data is valid", %{conn: conn} do
      conn = post(conn, Routes.backoffice_person_path(conn, :create), params: @create_attrs)
      assert %{person: id} = redirected_params(conn)
      assert redirected_to(conn) == Routes.backoffice_person_path(conn, :show, id)
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.backoffice_person_path(conn, :create), params: @invalid_attrs)
      assert html_response(conn, 200) =~ "New Person"
    end
  end

  describe "show" do
    setup [:login, :person]

    test "shows user, person and cases", %{conn: conn, person: person} do
      user1 = user_fixture(person, %{})
      user2 = user_fixture(person, %{})

      conn = get(conn, Routes.backoffice_person_path(conn, :show, person))
      assert html_response(conn, 200) =~ person.name
      assert html_response(conn, 200) =~ person.cedula
      assert html_response(conn, 200) =~ user1.email
      assert html_response(conn, 200) =~ user1.username
      assert html_response(conn, 200) =~ user2.email
      assert html_response(conn, 200) =~ user2.username
      assert html_response(conn, 200) =~ "Cases"
    end

    test "shows button to create login credentials if no user exists", %{conn: conn} do
      person = person_fixture()
      conn = get(conn, Routes.backoffice_person_path(conn, :show, person))
      assert html_response(conn, 200) =~ Routes.backoffice_user_path(conn, :new, person)
    end
  end

  describe "edit" do
    setup [:login, :person]

    test "renders form", %{conn: conn, person: person} do
      conn = get(conn, Routes.backoffice_person_path(conn, :edit, person))
      assert html_response(conn, 200) =~ "Edit Person"
      assert html_response(conn, 200) =~ person.cedula
    end
  end

  describe "update" do
    setup [:login, :person]

    test "redirects to show when data is valid", %{conn: conn, person: person} do
      conn =
        put(conn, Routes.backoffice_person_path(conn, :update, person), params: @update_attrs)

      assert %{person: id} = redirected_params(conn)
      assert redirected_to(conn) == Routes.backoffice_person_path(conn, :show, id)
    end

    test "renders errors when data is invalid", %{conn: conn, person: person} do
      conn =
        put(conn, Routes.backoffice_person_path(conn, :update, person), params: @invalid_attrs)

      assert html_response(conn, 200) =~ "Edit Person"
    end
  end

  describe "export_csv" do
    setup [:login, :person, :user]

    test "provides person data as csv", %{conn: conn, person: person, user: user1} do
      user2 = user_fixture(person, %{})
      conn = get(conn, Routes.backoffice_person_path(conn, :export_csv))

      assert response(conn, 200) =~
               "1_id,2_cedula,3_ruc,4_name,5_phone,6_user_count,7_emails,8_case_count\n"

      assert response(conn, 200) =~
               "#{person.id},#{person.cedula},#{person.ruc},#{person.name},#{person.phone},2,#{
                 user1.email <> " " <> user2.email
               },0\n"

      assert response_content_type(conn, :csv) =~ "csv"
    end
  end

  def user(%{person: person}) do
    user = user_fixture(person, %{})
    {:ok, user: user}
  end

  def person(_) do
    person = person_fixture()

    {:ok, person: person}
  end

  def login(%{conn: conn}) do
    user = bo_account_fixture()

    conn =
      conn
      |> init_test_session(user_id: user.id)
      |> assign(:current_user, user)

    {:ok, conn: conn}
  end
end
