defmodule MoranduWeb.Backoffice.CaseControllerTest do
  use MoranduWeb.ConnCase, async: true
  alias MoranduWeb.Api.V1.CaseView
  alias MoranduCore.Commitments.Cases

  @create_attrs %{
    cedula_or_ruc: "555555",
    name: "Some Name",
    phone: "0123456789",
    proof: %Plug.Upload{
      path: Path.dirname(__ENV__.file) |> Path.join("../proof_test.jpg")
    }
  }

  @invalid_attrs %{
    cedula_or_ruc: nil,
    name: nil,
    phone: nil,
    proof: nil
  }

  describe "logged out debtors" do
    test "requires debtor authentication on actions", %{conn: conn} do
      Enum.each(
        [
          get(conn, Routes.backoffice_case_path(conn, :index)),
          get(conn, Routes.backoffice_case_path(conn, :new, 1)),
          post(conn, Routes.backoffice_case_path(conn, :create, 1, %{})),
          get(conn, Routes.backoffice_case_path(conn, :show, 1)),
          post(conn, Routes.backoffice_case_path(conn, :resolve, 1)),
          post(conn, Routes.backoffice_case_path(conn, :verify, 1)),
          get(conn, Routes.backoffice_case_path(conn, :proof, 1)),
          post(conn, Routes.backoffice_case_path(conn, :clone, 1))
        ],
        fn conn ->
          assert html_response(conn, 401) =~ "unauthorized"
          assert conn.halted
        end
      )
    end
  end

  describe "index" do
    setup [:login, :account, :resolved, :verified]

    test "list active cases", %{conn: conn, resolved: resolved, verified: verified} do
      conn = get(conn, Routes.backoffice_case_path(conn, :index))
      assert html_response(conn, 200) =~ "#{verified.id}"
      refute html_response(conn, 200) =~ "#{resolved.id}"
    end
  end

  describe "new" do
    setup [:login, :account]

    test "new renders form", %{conn: conn, creditor: creditor} do
      conn = get(conn, Routes.backoffice_case_path(conn, :new, creditor))
      assert html_response(conn, 200) =~ "Create Case"
      assert html_response(conn, 200) =~ creditor.name
    end
  end

  describe "create" do
    setup [:login, :account]

    test "redirects to show when data is valid", %{conn: conn, creditor: creditor} do
      conn =
        post(conn, Routes.backoffice_case_path(conn, :create, creditor), params: @create_attrs)

      assert %{case: id} = redirected_params(conn)
      assert redirected_to(conn) == Routes.backoffice_case_path(conn, :show, id)
    end

    test "renders errors when data is invalid", %{conn: conn, creditor: creditor} do
      conn =
        post(conn, Routes.backoffice_case_path(conn, :create, creditor), params: @invalid_attrs)

      assert html_response(conn, 200) =~ "Create Case"
      assert html_response(conn, 200) =~ creditor.name
      assert html_response(conn, 200) =~ "Oops, something went wrong"
    end
  end

  describe "show" do
    setup [:login, :account, :verified]

    test "shows creditor and debtor", %{conn: conn, verified: verified} do
      conn = get(conn, Routes.backoffice_case_path(conn, :show, verified))
      assert html_response(conn, 200) =~ verified.creditor.name
      assert html_response(conn, 200) =~ verified.creditor.ruc
      assert html_response(conn, 200) =~ verified.debtor.name
      assert html_response(conn, 200) =~ verified.debtor.cedula
    end
  end

  # in tests all cases are verified by default. Nevertheless should broadcast changes and redirect
  describe "verify" do
    setup [:login, :account, :verified]

    test "verifies case and broadcasts change", %{conn: conn, verified: verified} do
      Phoenix.PubSub.subscribe(MoranduWeb.PubSub, "sync:lobby")
      conn = post(conn, Routes.backoffice_case_path(conn, :verify, verified))
      assert %{case: id} = redirected_params(conn)
      assert redirected_to(conn) == Routes.backoffice_case_path(conn, :show, id)

      assert_receive %Phoenix.Socket.Broadcast{
        topic: "sync:lobby",
        event: "changes",
        payload: %{changes: %{insert: [%CaseView{}], delete: []}, event: _}
      }

      Phoenix.PubSub.unsubscribe(MoranduWeb.PubSub, "sync:lobby")
    end

    test "redirects to origin", %{conn: conn, verified: verified, user: user} do
      conn =
        post(
          conn,
          Routes.backoffice_case_path(conn, :verify, verified,
            origin: Routes.backoffice_person_path(conn, :show, user)
          )
        )

      assert redirected_to(conn) == Routes.backoffice_person_path(conn, :show, user)
    end
  end

  describe("resolve") do
    setup [:login, :account, :verified]

    test "resolves case and broadcasts change", %{conn: conn, verified: verified} do
      Phoenix.PubSub.subscribe(MoranduWeb.PubSub, "sync:lobby")
      conn = post(conn, Routes.backoffice_case_path(conn, :resolve, verified))
      assert %{case: id} = redirected_params(conn)
      assert redirected_to(conn) == Routes.backoffice_case_path(conn, :show, id)

      assert_receive %Phoenix.Socket.Broadcast{
        topic: "sync:lobby",
        event: "changes",
        payload: %{changes: %{insert: [], delete: [%CaseView{}]}, event: _}
      }

      Phoenix.PubSub.unsubscribe(MoranduWeb.PubSub, "sync:lobby")
    end

    test "redirects to origin", %{conn: conn, verified: verified, user: user} do
      conn =
        post(
          conn,
          Routes.backoffice_case_path(conn, :resolve, verified,
            origin: Routes.backoffice_person_path(conn, :show, user)
          )
        )

      assert redirected_to(conn) == Routes.backoffice_person_path(conn, :show, user)
    end
  end

  describe("proof") do
    setup [:login, :account]

    test "provides the proof as download", %{conn: conn, user: user} do
      case_ = case_fixture(user, %{proof: <<0x89, 0x50, 0x4E, 0x47, 0x0D, 0x0A, 0x1A, 0x0A>>})
      conn = get(conn, Routes.backoffice_case_path(conn, :proof, case_))
      assert response(conn, 200) =~ <<0x89, 0x50, 0x4E, 0x47, 0x0D, 0x0A, 0x1A, 0x0A>>
      assert response_content_type(conn, :png) =~ "image/png"
    end

    test "on error redirects to show", %{conn: conn, user: user} do
      case_ = case_fixture(user, %{proof: <<0x89, 0x50, 0x4E, 0x47, 0x0D, 0x0A, 0x1A, 0x0A>>})
      File.rm!(case_.proof)
      conn = get(conn, Routes.backoffice_case_path(conn, :proof, case_))
      assert redirected_to(conn) == Routes.backoffice_case_path(conn, :show, case_)
    end
  end

  describe("clone") do
    setup [:login, :account, :resolved]

    test "clones case", %{conn: conn, resolved: resolved} do
      conn = post(conn, Routes.backoffice_case_path(conn, :clone, resolved))
      assert %{case: id} = redirected_params(conn)
      assert redirected_to(conn) == Routes.backoffice_case_path(conn, :show, id)

      cloned = Cases.get!(id)
      assert cloned.id != resolved.id
      assert sanitize_case(resolved) == sanitize_case(cloned)
    end

    def sanitize_case(case_) do
      %{
        case_
        | id: nil,
          debtor: nil,
          creditor: nil,
          inserted_at: nil,
          resolved_at: nil,
          verified_at: nil,
          updated_at: nil
      }
    end
  end

  def account(_) do
    creditor = person_fixture()
    user = user_fixture(creditor, %{})

    {:ok, user: user, creditor: creditor}
  end

  def verified(%{user: user}) do
    verified = case_fixture(user, %{})
    {:ok, verified: verified}
  end

  def resolved(%{user: user}) do
    resolved = case_fixture(user, %{resolved_at: true})
    {:ok, resolved: resolved}
  end

  def login(%{conn: conn}) do
    user = bo_account_fixture()

    conn =
      conn
      |> init_test_session(user_id: user.id)
      |> assign(:current_user, user)

    {:ok, conn: conn}
  end
end
