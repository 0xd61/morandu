defmodule MoranduWeb.Backoffice.UserControllerTest do
  use MoranduWeb.ConnCase, async: true

  @create_attrs %{
    email: "new@email.xyz",
    username: "new username",
    password: "mynewlongpassword"
  }

  @update_attrs %{
    email: "changed@email.xyz",
    username: "other username",
    password: "myotherlongpassword"
  }

  @invalid_attrs %{
    email: nil,
    username: nil,
    password: nil
  }

  describe "logged out users" do
    test "requires user authentication on actions", %{conn: conn} do
      Enum.each(
        [
          get(conn, Routes.backoffice_user_path(conn, :new, 1)),
          post(conn, Routes.backoffice_user_path(conn, :create, 1)),
          get(conn, Routes.backoffice_user_path(conn, :edit, 1)),
          put(conn, Routes.backoffice_user_path(conn, :update, 1))
        ],
        fn conn ->
          assert html_response(conn, 401) =~ "unauthorized"
          assert conn.halted
        end
      )
    end
  end

  describe "new" do
    setup [:login, :person]

    test "renders form", %{conn: conn, person: person} do
      conn = get(conn, Routes.backoffice_user_path(conn, :new, person))
      assert html_response(conn, 200) =~ "Create User"
    end
  end

  describe "create" do
    setup [:login, :person]

    test "redirects to show when data is valid", %{conn: conn, person: person} do
      conn = post(conn, Routes.backoffice_user_path(conn, :create, person), params: @create_attrs)

      assert %{person: id} = redirected_params(conn)
      assert redirected_to(conn) == Routes.backoffice_person_path(conn, :show, id)
    end

    test "renders errors when data is invalid", %{conn: conn, person: person} do
      conn =
        post(conn, Routes.backoffice_user_path(conn, :create, person), params: @invalid_attrs)

      assert html_response(conn, 200) =~ "Create User"
    end
  end

  describe "edit" do
    setup [:login, :user]

    test "renders form", %{conn: conn, user: user} do
      conn = get(conn, Routes.backoffice_user_path(conn, :edit, user))
      assert html_response(conn, 200) =~ "User Settings"
    end

    test "renders user session keys", %{conn: conn, user: user} do
      key = key_fixture(user, %{})
      conn = get(conn, Routes.backoffice_user_path(conn, :edit, user))
      assert html_response(conn, 200) =~ "#{key.id}"
    end
  end

  describe "update" do
    setup [:login, :user]

    test "redirects to show when data is valid", %{conn: conn, user: user} do
      conn = put(conn, Routes.backoffice_user_path(conn, :update, user), params: @update_attrs)

      assert %{person: id} = redirected_params(conn)

      assert redirected_to(conn) ==
               Routes.backoffice_person_path(conn, :show, user.commitments_person_id)
    end

    test "renders errors when data is invalid", %{conn: conn, user: user} do
      conn = put(conn, Routes.backoffice_user_path(conn, :update, user), params: @invalid_attrs)

      assert html_response(conn, 200) =~ "User Settings"
    end
  end

  describe "delete" do
    setup [:login, :user]

    test "deletes user", %{conn: conn, user: user} do
      conn = delete(conn, Routes.backoffice_user_path(conn, :delete, user))

      assert redirected_to(conn) ==
               Routes.backoffice_person_path(conn, :show, user.commitments_person_id)

      assert_error_sent 404, fn ->
        get(conn, Routes.backoffice_user_path(conn, :edit, user))
      end
    end
  end

  def user(_) do
    user = user_fixture()

    {:ok, user: user}
  end

  def person(_) do
    person = person_fixture()

    {:ok, person: person}
  end

  def login(%{conn: conn}) do
    user = bo_account_fixture()

    conn =
      conn
      |> init_test_session(user_id: user.id)
      |> assign(:current_user, user)

    {:ok, conn: conn}
  end
end
