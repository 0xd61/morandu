defmodule MoranduWeb.Backoffice.TokenControllerTest do
  use MoranduWeb.ConnCase, async: true
  alias MoranduCore.Accounts.Keys

  describe "logged out users" do
    test "requires user authentication on actions", %{conn: conn} do
      Enum.each(
        [
          delete(conn, Routes.backoffice_token_path(conn, :delete, 1, 1))
        ],
        fn conn ->
          assert html_response(conn, 401) =~ "unauthorized"
          assert conn.halted
        end
      )
    end
  end

  describe "delete" do
    setup [:login, :user, :key]

    test "revokes token", %{conn: conn, user: user, key: key} do
      assert key.revoked_at == nil
      conn = delete(conn, Routes.backoffice_token_path(conn, :delete, user, key))

      assert redirected_to(conn) ==
               Routes.backoffice_user_path(conn, :edit, user)

      key = Keys.get_by!(id: key.id, user_id: user.id)
      assert key.revoked_at != nil
    end
  end

  def key(%{user: user}) do
    key = key_fixture(user, %{})
    {:ok, key: key}
  end

  def user(_) do
    user = user_fixture()
    {:ok, user: user}
  end

  def login(%{conn: conn}) do
    user = bo_account_fixture()

    conn =
      conn
      |> init_test_session(user_id: user.id)
      |> assign(:current_user, user)

    {:ok, conn: conn}
  end
end
