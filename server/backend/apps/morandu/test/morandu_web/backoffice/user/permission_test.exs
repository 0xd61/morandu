defmodule MoranduWeb.Backoffice.User.PermissionControllerTest do
  use MoranduWeb.ConnCase, async: true

  @update_attrs %{
    "create" => "true",
    "edit" => "false",
    "read" => "false",
    "delete" => "true",
    "undefined" => "true"
  }

  describe "logged out users" do
    test "requires user authentication on actions", %{conn: conn} do
      Enum.each(
        [
          put(conn, Routes.backoffice_user_permission_path(conn, :update, 1))
        ],
        fn conn ->
          assert html_response(conn, 401) =~ "unauthorized"
          assert conn.halted
        end
      )
    end
  end

  describe "update" do
    setup [:login, :user]

    test "updates user permissions", %{conn: conn, user: user} do
      conn = put(conn, Routes.backoffice_user_permission_path(conn, :update, user), params: @update_attrs)
      assert redirected_to(conn) == Routes.backoffice_user_path(conn, :edit, user)
    end
  end

  def user(_) do
    user = user_fixture()

    {:ok, user: user}
  end

  def login(%{conn: conn}) do
    user = bo_account_fixture()

    conn =
      conn
      |> init_test_session(user_id: user.id)
      |> assign(:current_user, user)

    {:ok, conn: conn}
  end
end
