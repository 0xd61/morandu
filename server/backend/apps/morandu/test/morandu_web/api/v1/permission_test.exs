defmodule MoranduWeb.Api.V1.PermissionTest do
  use MoranduWeb.ConnCase, async: true
  alias MoranduCore.Commitments.Persons

  describe "logged out users" do
    test "require user authenticaiton on actions", %{conn: conn} do
      Enum.each(
        [
          # auth
          delete(conn, Routes.api_v1_auth_path(conn, :delete)),
          # channel
          post(conn, Routes.api_v1_channel_path(conn, :create)),
          # lookup
          get(conn, Routes.api_v1_lookup_path(conn, :show, 123)),
          # user
          get(conn, Routes.api_v1_user_path(conn, :index)),
          get(conn, Routes.api_v1_user_path(conn, :show, 123)),
          post(conn, Routes.api_v1_user_path(conn, :update, 123)),
          post(conn, Routes.api_v1_user_path(conn, :permission, 123)),
          # person
          get(conn, Routes.api_v1_person_path(conn, :index)),
          get(conn, Routes.api_v1_person_path(conn, :show, 123)),
          post(conn, Routes.api_v1_person_path(conn, :vote, 123), vote: 1),
          post(conn, Routes.api_v1_person_path(conn, :delete_vote, 123)),

          # case
          get(conn, Routes.api_v1_case_path(conn, :index)),
          post(conn, Routes.api_v1_case_path(conn, :create, %{})),
          post(conn, Routes.api_v1_case_path(conn, :resolve, 1))
        ],
        fn conn ->
          assert %{
                   "error" => %{
                     "code" => "unauthorized",
                     "message" => _,
                     "status" => 401
                   }
                 } = json_response(conn, 401)

          assert conn.halted
        end
      )
    end
  end

  describe "logged in users w/o permissions" do
    setup [:user_w_o_permission, :login]

    test "cannot access resources (even if they belong to same person)", %{
      conn: conn,
      user: %{commitments_person_id: person_id}
    } do
      person = Persons.get_by!(id: person_id)
      user = user_fixture(person, %{})
      case_ = case_fixture(user, %{})

      Enum.each(
        [
          # channel
          post(conn, Routes.api_v1_channel_path(conn, :create)),
          # lookup
          get(conn, Routes.api_v1_lookup_path(conn, :show, person.cedula)),
          # user
          get(conn, Routes.api_v1_user_path(conn, :index)),
          get(conn, Routes.api_v1_user_path(conn, :show, user)),
          post(conn, Routes.api_v1_user_path(conn, :update, user)),
          post(conn, Routes.api_v1_user_path(conn, :permission, user)),
          # case
          get(conn, Routes.api_v1_case_path(conn, :index)),
          post(conn, Routes.api_v1_case_path(conn, :create, %{})),
          post(conn, Routes.api_v1_case_path(conn, :resolve, case_))
        ],
        fn conn ->
          assert %{
                   "error" => %{
                     "code" => "forbidden",
                     "message" => _,
                     "status" => 403
                   }
                 } = json_response(conn, 403)

          assert conn.halted
        end
      )
    end
  end

  describe "logged in users w/ permissions" do
    setup [:user_w_permission, :login]

    test "cannot access resources from other persons (companies)", %{conn: conn} do
      user = user_fixture()
      case_ = case_fixture()

      Enum.each(
        [
          # user
          get(conn, Routes.api_v1_user_path(conn, :show, user)),
          post(conn, Routes.api_v1_user_path(conn, :update, user)),
          post(conn, Routes.api_v1_user_path(conn, :permission, user)),
          # case
          post(conn, Routes.api_v1_case_path(conn, :resolve, case_))
        ],
        fn conn ->
          assert %{
                   "error" => %{
                     "code" => "forbidden",
                     "message" => _,
                     "status" => 403
                   }
                 } = json_response(conn, 403)

          assert conn.halted
        end
      )
    end
  end

  def user_w_permission(_) do
    user = user_fixture()

    {:ok, user: user}
  end

  def user_w_o_permission(_) do
    user = user_fixture(%{permissions: 0})

    {:ok, user: user}
  end

  def login(%{conn: conn, user: user}) do
    conn =
      conn
      |> init_test_session(user_id: user.id)
      |> assign(:current_user, user)

    {:ok, conn: conn}
  end
end
