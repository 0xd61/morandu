defmodule MoranduWeb.Api.V1.AuthControllerTest do
  use MoranduWeb.ConnCase, async: true
  alias MoranduCore.Accounts
  alias MoranduWeb.AuthHelpers

  describe "create" do
    setup [:user]

    test "returns api token is email and password is correct", %{conn: conn, user: user} do
      conn =
        post(conn, Routes.api_v1_auth_path(conn, :create),
          params: %{email: user.email, password: user.password_hash.user_secret}
        )

      user_id = user.id

      assert %{"token" => _, "user" => %{"id" => ^user_id}} = json_response(conn, 200)
    end

    test "returns error if email or password is incorrect", %{conn: conn, user: user} do
      conn =
        post(
          conn,
          Routes.api_v1_auth_path(conn, :create),
          params: %{email: user.email, password: "invalid password"}
        )

      assert %{"error" => %{"status" => 401}} = json_response(conn, 401)
    end
  end

  describe "delete" do
    setup %{conn: conn} do
      user = user_fixture()
      {:ok, key} = Accounts.Keys.new(user)

      conn =
        conn
        |> put_req_header("authorization", "Bearer #{key.user_secret}")

      {:ok, conn: conn, user: user, key: key}
    end

    test "revokes user auth key", %{conn: conn, key: key} do
      conn = delete(conn, Routes.api_v1_auth_path(conn, :delete))

      assert json_response(conn, 200) == %{"success" => true}

      assert Accounts.Auth.via_key(key.user_secret, AuthHelpers.usage_info(conn)) ==
               {:error, :revoked}
    end
  end

  def user(_) do
    user = user_fixture(%{email: "auth@test.com", password: "password123"})

    {:ok, user: user}
  end
end

defmodule MoranduWeb.Api.V1.AuthViewTest do
  use MoranduWeb.ConnCase
  import Phoenix.View
  alias MoranduWeb.Api.V1.{AuthView, UserView}

  setup [:user, :key]

  test "renders create.json", %{user: user, key: key} do
    secret = key.user_secret

    assert %{token: ^secret, user: %UserView{}} =
             render(AuthView, "create.json", %{user: user, key: key})
  end

  defp user(_) do
    user = user_fixture()
    {:ok, user: user}
  end

  defp key(%{user: user}) do
    key = key_fixture(user, %{})
    {:ok, key: key}
  end
end
