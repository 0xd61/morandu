defmodule MoranduWeb.Api.V1.UserControllerTest do
  use MoranduWeb.ConnCase, async: true
  alias MoranduCore.Accounts.Auth
  alias MoranduCore.Accounts.Users
  @update_attrs %{email: "new@email.com", password: "updated password"}
  @invalid_attrs %{email: nil, password: "12345"}

  describe "show" do
    setup [:login]

    test "returns user", %{conn: conn, user: user} do
      conn = get(conn, Routes.api_v1_user_path(conn, :show, user))
      user_id = user.id
      assert %{"id" => ^user_id} = json_response(conn, 200)
    end
  end

  describe "update" do
    setup [:login]

    test "updates user if params are valid", %{conn: conn, user: user} do
      conn = post(conn, Routes.api_v1_user_path(conn, :update, user), params: @update_attrs)

      assert res = json_response(conn, 200)

      assert res["email"] === @update_attrs.email
      assert res["id"] === user.id

      assert {:ok, _} =
               Auth.via_password(@update_attrs.email, @update_attrs.password, %{
                 used_at: DateTime.utc_now(),
                 user_agent: ["Chrome"],
                 ip: {127, 0, 0, 1}
               })
    end

    test "renders errors if params are invalid", %{conn: conn, user: user} do
      conn = post(conn, Routes.api_v1_user_path(conn, :update, user), params: @invalid_attrs)

      assert %{
               "error" => %{
                 "form" => %{
                   "email" => [_],
                   "password" => [_]
                 }
               }
             } = json_response(conn, 422)
    end
  end

  describe "permission" do
    setup [:login]
    test "sets the user permission based on a role", %{conn: conn, user: user} do
      conn = post(conn, Routes.api_v1_user_path(conn, :permission, user), params: %{"role" => "admin"})

      assert %{"role" => "admin"} = json_response(conn, 200)
    end

    test "user with permission can update other user in same person", %{conn: conn, person: person} do
      user = user_fixture(person, %{})
      conn = post(conn, Routes.api_v1_user_path(conn, :permission, user), params: %{"role" => "admin"})

      assert %{"role" => "admin"} = json_response(conn, 200)
    end

    test "user cannot change his own role without permission", %{conn: conn, user: user} do
      # Update user in conn with 0 permissions
      user = Users.set_role(user, 0)
      conn = assign(conn, :current_user, user)

      conn = post(conn, Routes.api_v1_user_path(conn, :permission, user), params: %{"role" => "admin"})

      assert %{"error" => %{"code" => "forbidden"}} = json_response(conn, 403)
    end

    test "with invalid role renders error", %{conn: conn, user: user} do
      conn = post(conn, Routes.api_v1_user_path(conn, :permission, user), params: %{"role" => "undefined"})
      assert %{"error" => %{"message" => _, "status" => 400}} = json_response(conn, 400)
    end
  end

  def login(%{conn: conn}) do
    person = person_fixture()
    user = user_fixture(person, %{})

    conn =
      conn
      |> init_test_session(user_id: user.id)
      |> assign(:current_user, user)

    {:ok, conn: conn, user: user, person: person}
  end
end

defmodule MoranduWeb.Api.V1.UserViewTest do
  use MoranduWeb.ConnCase
  use MoranduWeb.Api.V1.UserHelpers
  import Phoenix.View
  alias MoranduWeb.Api.V1.UserView

  setup [:user]

  test "renders show.json", %{user: user} do
    assert %UserView{} = render(UserView, "show.json", %{user: user})
  end

  test "renders update.json", %{user: user} do
    assert %UserView{} = render(UserView, "update.json", %{user: user})
  end

  test "renders user.json", %{user: user} do
    assert render(UserView, "user.json", %{user: user}) == %UserView{
             id: user.id,
             email: user.email,
             username: user.username,
             role: role_from_permission(user.permissions)
           }
  end

  defp user(_) do
    user = user_fixture()
    {:ok, user: user}
  end
end
