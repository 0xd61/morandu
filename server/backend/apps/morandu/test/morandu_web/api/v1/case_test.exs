defmodule MoranduWeb.Api.V1.CaseControllerTest do
  use MoranduWeb.ConnCase
  alias MoranduCore.Commitments.Cases

  @valid_attrs %{
    cedula_or_ruc: "555555",
    name: "Some Name",
    phone: "0123456789",
    proof:
      "data:text/plain;base64," <>
        (Path.dirname(__ENV__.file)
         |> Path.join("../../proof_test.jpg")
         |> File.read!()
         |> Base.encode64())
  }

  @invalid_attrs %{
    cedula_or_ruc: nil,
    name: nil,
    phone: nil,
    proof: nil
  }

  describe "index" do
    setup [:login, :resolved, :verified]

    test "lists unresolved cases of creditor", %{conn: conn, verified: %{id: id}} do
      conn = get(conn, Routes.api_v1_case_path(conn, :index))

      assert [
               %{
                 "id" => ^id,
                 "c_cedula" => _,
                 "c_name" => _,
                 "c_ruc" => _,
                 "d_cedula" => _,
                 "d_name" => _,
                 "d_ruc" => _
               }
             ] = json_response(conn, 200)
    end
  end

  describe "create" do
    setup [:login]

    test "creates new case with valid attrs", %{conn: conn} do
      conn = post(conn, Routes.api_v1_case_path(conn, :create), params: @valid_attrs)
      assert %{"id" => _id} = json_response(conn, 200)
    end

    test "returns changeset with invalid attrs", %{conn: conn} do
      conn = post(conn, Routes.api_v1_case_path(conn, :create), params: @invalid_attrs)

      assert %{
               "error" => %{
                 "form" => %{
                   "cedula_or_ruc" => [_],
                   "name" => [_],
                   "proof" => [_]
                 }
               }
             } = json_response(conn, 422)
    end
  end

  describe "resolve" do
    setup [:login, :resolved, :verified]

    test "resolves case and broadcasts change", %{conn: conn, verified: %{id: id}} do
      Phoenix.PubSub.subscribe(MoranduWeb.PubSub, "sync:lobby")
      conn = post(conn, Routes.api_v1_case_path(conn, :resolve, id))
      assert %{"success" => true} = json_response(conn, 200)

      assert_receive %Phoenix.Socket.Broadcast{
        topic: "sync:lobby",
        event: "changes",
        payload: %{changes: %{insert: [], delete: [%{id: ^id}]}, event: _}
      }

      Phoenix.PubSub.unsubscribe(MoranduWeb.PubSub, "sync:lobby")

      resolved = Cases.get!(id)
      assert resolved.resolved_at !== nil
    end
  end

  def resolved(%{user: user}) do
    current_case = case_fixture(user, %{resolved_at: true})
    {:ok, resolved: current_case}
  end

  def verified(%{user: user}) do
    current_case = case_fixture(user, %{})
    {:ok, verified: current_case}
  end

  def verified(_) do
    current_case = case_fixture(%{})
    {:ok, verified: current_case}
  end

  def login(%{conn: conn}) do
    creditor = person_fixture()
    user = user_fixture(creditor, %{})

    conn =
      conn
      |> init_test_session(user_id: user.id)
      |> assign(:current_user, user)

    {:ok, conn: conn, user: user, creditor: creditor}
  end
end
