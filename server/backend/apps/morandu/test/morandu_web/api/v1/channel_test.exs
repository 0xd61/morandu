defmodule MoranduWeb.Api.V1.ChannelControllerTest do
  use MoranduWeb.ConnCase, async: true

  @salt Application.get_env(:morandu, MoranduWeb.Endpoint)[:token_salt]
  @max_age 24 * 60 * 60

  describe "create" do
    setup [:login]

    test "returns phoenix token for channel auth", %{conn: conn, user: user} do
      conn = post(conn, Routes.api_v1_channel_path(conn, :create))
      assert %{"token" => token} = json_response(conn, 200)
      id = user.id
      assert {:ok, ^id} = Phoenix.Token.verify(conn, @salt, token, max_age: @max_age)
    end
  end

  def login(%{conn: conn}) do
    user = user_fixture()

    conn =
      conn
      |> init_test_session(user_id: user.id)
      |> assign(:current_user, user)

    {:ok, conn: conn, user: user}
  end
end
