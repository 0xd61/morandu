defmodule MoranduWeb.Api.V1.LookupControllerTest do
  use MoranduWeb.ConnCase, async: true

  @cedula_or_ruc "123456-8"

  describe "show" do
    setup [:login, :verified, :resolved]

    test "returns active cases for cedula", %{conn: conn, verified: verified} do
      conn = get(conn, Routes.api_v1_lookup_path(conn, :show, "123456"))
      assert [cases] = json_response(conn, 200)

      assert cases["d_cedula"] == verified.debtor.cedula
      assert cases["c_name"] == verified.creditor.name
      assert cases["id"] == verified.id
    end

    test "returns active cases for ruc", %{conn: conn, verified: verified} do
      conn = get(conn, Routes.api_v1_lookup_path(conn, :show, "123456-8"))
      assert [cases] = json_response(conn, 200)

      assert cases["d_cedula"] == verified.debtor.cedula
      assert cases["c_name"] == verified.creditor.name
      assert cases["id"] == verified.id
    end

    test "returns empty list if no cases found", %{conn: conn} do
      conn = get(conn, Routes.api_v1_lookup_path(conn, :show, "not_found"))
      assert [] = json_response(conn, 200)
    end
  end

  def verified(_) do
    current_case = case_fixture(%{cedula: @cedula_or_ruc})

    {:ok, verified: current_case}
  end

  def resolved(_) do
    current_case = case_fixture(%{cedula: @cedula_or_ruc, resolved_at: true})

    {:ok, resolved: current_case}
  end

  def login(%{conn: conn}) do
    user = user_fixture()

    conn =
      conn
      |> init_test_session(user_id: user.id)
      |> assign(:current_user, user)

    {:ok, conn: conn, user: user}
  end
end
