defmodule MoranduWeb.Api.V1.PersonControllerTest do
  use MoranduWeb.ConnCase, async: true
  alias MoranduCore.Commitments.Persons
  alias MoranduCore.Commitments.PersonAttribute

  # NOTE: Currently we dont want people to edit their info
  # @update_attrs %{name: "some updated name", phone: "0365284965"}
  # @invalid_attrs %{name: nil, phone: nil}

  describe "index" do
    setup [:login]

    test "returns person for current user", %{conn: conn, creditor: creditor} do
      conn = get(conn, Routes.api_v1_person_path(conn, :index))
      creditor_id = creditor.id
      assert %{"id" => ^creditor_id, "cedula" => _} = json_response(conn, 200)
    end

    test "returns error if user has no access", %{conn: conn, user: user} do
      user = %{user | permissions: 0}
      conn = assign(conn, :current_user, user)
      conn = get(conn, Routes.api_v1_person_path(conn, :index))
      assert %{"error" => %{"code" => "forbidden"}} = json_response(conn, 403)
    end
  end

  describe "show" do
    setup [:login, :person]
    test "returns public person info", %{conn: conn, person: person} do
      Persons.set_attribute(person_fixture(), person, %{type: :vote, value: 3})
      conn = get(conn, Routes.api_v1_person_path(conn, :show, person.cedula))
      assert json_response(conn, 200) == %{"id" => person.id,
                                            "cedula" => person.cedula,
                                            "ruc" => person.ruc,
                                            "score" => 3,
                                            "vote_count" => 1,
                                            "voted" => false}
    end

    test "returns voted, if current person upvoted person", %{conn: conn, person: person} do
      post(conn, Routes.api_v1_person_path(conn, :vote, person.cedula), value: 2)

      conn = get(conn, Routes.api_v1_person_path(conn, :show, person.cedula))
      assert json_response(conn, 200) == %{"id" => person.id,
                                            "cedula" => person.cedula,
                                            "ruc" => person.ruc,
                                            "score" => 2,
                                            "vote_count" => 1,
                                            "voted" => true}
    end

    test "returns error if user has no access", %{conn: conn, user: user, person: person} do
      user = %{user | permissions: 0}
      conn =
        conn
        |> assign(:current_user, user)
        |> get(Routes.api_v1_person_path(conn, :show, person.ruc))

      assert %{"error" => %{"code" => "forbidden"}} = json_response(conn, 403)
    end

    test "returns error if not found", %{conn: conn} do
      conn = get(conn, Routes.api_v1_person_path(conn, :show, 999999))
      assert %{"error" => %{"message" => "Page not found"}} = json_response(conn, 404)
    end
  end

  describe "vote and delete_vote" do
    setup [:login, :person]

    test "creates person (just cedula/ruc) if does not exist", %{conn: conn} do
      assert Persons.get_by(cedula: "112233") == nil

      conn = post(conn, Routes.api_v1_person_path(conn, :vote, "112233"), value: 2)
      assert %{"success" => true} = json_response(conn, 200)

      assert Persons.get_by!(cedula: "112233")
    end

    test "votes if has not been voted yet", %{conn: conn, person: person} do
      conn = post(conn, Routes.api_v1_person_path(conn, :vote, person.cedula), value: 4)
      assert %{"success" => true} = json_response(conn, 200)

      assert %PersonAttribute{} = Persons.get_attribute_by(person_id: person.id, type: :vote)
      assert Persons.count_attributes_by(person_id: person.id, type: :vote) == 1
    end

    test "does nothing if already voted", %{conn: conn, person: person} do
      post(conn, Routes.api_v1_person_path(conn, :vote, person.cedula), value: 1)
      assert Persons.count_attributes_by(person_id: person.id, type: :vote) == 1

      conn = post(conn, Routes.api_v1_person_path(conn, :vote, person.cedula), value: 1)
      assert %{"success" => true} = json_response(conn, 200)
      assert Persons.count_attributes_by(person_id: person.id, type: :vote) == 1
    end

    test "renders error if person votes itself", %{conn: conn, creditor: person} do
      conn = post(conn, Routes.api_v1_person_path(conn, :vote, person.cedula), value: 5)
      assert %{"error" => %{"message" => "Bad request"}} = json_response(conn, 400)
    end

    test "removes vote if has been voted", %{conn: conn, person: person} do
      post(conn, Routes.api_v1_person_path(conn, :vote, person.cedula), value: 3)
      assert Persons.count_attributes_by(person_id: person.id, type: :vote) == 1

      conn = delete(conn, Routes.api_v1_person_path(conn, :delete_vote, person.cedula))
      assert %{"success" => true} = json_response(conn, 200)
      assert Persons.count_attributes_by(person_id: person.id, type: :vote) == 0
    end

    test "renders error if not voted yet", %{conn: conn, person: person} do
      Persons.set_attribute(person_fixture(), person, %{type: :vote})
      assert Persons.count_attributes_by(person_id: person.id, type: :vote) == 1

      conn = delete(conn, Routes.api_v1_person_path(conn, :delete_vote, person.cedula))
      assert %{"error" => %{"message" => "Page not found"}} = json_response(conn, 404)
      assert Persons.count_attributes_by(person_id: person.id, type: :vote) == 1
    end

  end

  def person(_) do
    person = person_fixture()
    {:ok, person: person}
  end

  def login(%{conn: conn}) do
    creditor = person_fixture()
    user = user_fixture(creditor, %{})

    conn =
      conn
      |> init_test_session(user_id: user.id)
      |> assign(:current_user, user)

    {:ok, conn: conn, user: user, creditor: creditor}
  end
end
