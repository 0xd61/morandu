defmodule MoranduWeb.ControllerHelpersTest do
  use MoranduWeb.ConnCase, async: true
  alias MoranduWeb.ControllerHelpers, as: Helpers
  alias MoranduCore.Accounts.Users
  import Phoenix.Controller, only: [accepts: 2]

  test "render_error/3 renders json error from error view", %{conn: conn} do
    conn =
      conn
      |> put_req_header("accept", "application/json")
      |> accepts(["json"])
      |> Helpers.render_error(400, code: "foo", message: "bar")

    assert %{
             "error" => %{"code" => "foo", "message" => "bar", "status" => 400}
           } = json_response(conn, 400)

    assert conn.halted
  end

  test "render_error/3 renders html error from error view", %{conn: conn} do
    conn =
      conn
      |> put_req_header("accept", "text/html")
      |> accepts(["html"])
      |> Helpers.render_error(400, code: "foo", message: "bar")

    assert html_response(conn, 400) =~ "bar"
    assert conn.halted
  end

  test "changeset_error/3 renders all errors from a changeset", %{conn: conn} do
    user = user_fixture()
    {:error, changeset} = Users.update(user, %{email: "invalid email"})

    conn =
      conn
      |> put_req_header("accept", "application/json")
      |> accepts(["json"])
      |> Helpers.changeset_error(changeset)

    assert %{"error" => %{"form" => %{"email" => [_]}}} = json_response(conn, 422)
  end
end
