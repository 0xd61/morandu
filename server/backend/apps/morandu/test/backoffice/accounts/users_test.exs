defmodule Backoffice.Accounts.UsersTest do
  use MoranduCore.DataCase, async: true
  alias Backoffice.Accounts.{Users, User}

  @password "myverylongsecretpassword"

  @valid_attrs %{
    email: "someone@example.com",
    name: "some name",
    password: @password
  }

  @update_attrs %{
    email: "another@example.com",
    name: "some updated name",
    password: "mynewverylongpassword"
  }

  @invalid_attrs %{email: "invalid email", name: nil, password: nil}

  describe "CRUD User" do
    setup [:user]

    test "new/1 with valid attrs creates new user" do
      assert {:ok, user} = Users.new(@valid_attrs)
      assert user.email == @valid_attrs.email
      assert user.name == @valid_attrs.name
      assert user.password_hash != nil
    end

    test "new/1 with invalid attrs returns changeset" do
      assert {:error, %Ecto.Changeset{}} = Users.new(@invalid_attrs)
    end

    test "list/1 returns a list of current users", %{user: user} do
      assert Users.list() == [user]
    end

    test "get!/1 returns the user with given id", %{user: user} do
      assert Users.get!(user.id) == user
    end

    test "get_by!/1 returns the user with given email", %{user: user} do
      assert Users.get_by!(email: user.email) == user

      assert_raise Ecto.NoResultsError, fn ->
        Users.get_by!(email: "email@doesnotexist.com")
      end
    end

    test "get_by/1 returns the user by params", %{user: user} do
      assert Users.get_by(email: user.email) == user
      assert Users.get_by(email: "email@doesnotexist.com") == nil
    end

    test "change/1 returns a user changeset", %{user: user} do
      assert %Ecto.Changeset{} = Users.change(user)
    end

    test "update/2 with valid data updates the user", %{user: user} do
      assert {:ok, %User{} = user_new} = Users.update(user, @update_attrs)
      assert user_new.email == @update_attrs.email
      assert user_new.name == @update_attrs.name
      assert user_new.password_hash != user.password_hash
    end

    test "update/2 with invalid data returns error changeset", %{user: user} do
      assert {:error, %Ecto.Changeset{}} = Users.update(user, @invalid_attrs)
    end

    test "delete/1 deletes the user", %{user: user} do
      assert {:ok, %User{}} = Users.delete(user)
      assert_raise Ecto.NoResultsError, fn -> Users.get!(user.id) end
    end
  end

  describe "authenticate_by_email_and_pass/2" do
    @pass "secret password"

    setup do
      {:ok, user: BOHelpers.user_fixture(password: @pass)}
    end

    test "returns user with correct password", %{user: user} do
      assert {:ok, auth_user} = Users.authenticate_by_email_and_pass(user.email, @pass)
      assert auth_user.id == user.id
    end

    test "returns unauthorized with incorrect password", %{user: user} do
      assert {:error, :unauthorized} =
               Users.authenticate_by_email_and_pass(user.email, "wrong pass")
    end

    test "returns not found with no matching user" do
      assert {:error, :not_found} =
               Users.authenticate_by_email_and_pass("invalid@email.com", "some pass")
    end
  end

  defp user(_) do
    user = BOHelpers.user_fixture()
    {:ok, user: user}
  end
end
