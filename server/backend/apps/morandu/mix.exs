defmodule Morandu.MixProject do
  use Mix.Project

  def project do
    [
      app: :morandu,
      version: "0.1.0",
      build_path: "../../_build",
      config_path: "../../config/config.exs",
      deps_path: "../../deps",
      lockfile: "../../mix.lock",
      elixir: "~> 1.9",
      elixirc_paths: elixirc_paths(Mix.env()),
      compilers: [:phoenix, :gettext] ++ Mix.compilers(),
      start_permanent: Mix.env() == :prod,
      aliases: aliases(),
      deps: deps()
    ]
  end

  # Configuration for the OTP application.
  #
  # Type `mix help compile.app` for more information.
  def application do
    [
      mod: {MoranduCore.Application, []},
      extra_applications: [:logger, :runtime_tools, :os_mon]
    ]
  end

  # Specifies which paths to compile per environment.
  defp elixirc_paths(:test), do: ["lib", "test/support"]
  defp elixirc_paths(_), do: ["lib"]

  # Specifies your project dependencies.
  #
  # Type `mix help deps` for examples and options.
  defp deps do
    [
      # Core
      {:ecto_sql, "~> 3.4"},
      {:postgrex, "~> 0.15.1"},
      {:jason, "~> 1.0"},
      {:bcrypt_elixir, "~> 2.0"},
      {:hackney, "~> 1.15"},
      {:telemetry_poller, "~> 0.5"},
      {:telemetry_metrics, "~> 0.5"},

      # Web
      {:phoenix, "~> 1.5.0"},
      {:phoenix_pubsub, "~> 2.0"},
      {:phoenix_ecto, "~> 4.0"},
      {:phoenix_html, "~> 2.14"},
      {:phoenix_live_reload, "~> 1.2", only: :dev},
      {:gettext, "~> 0.11"},
      {:plug_cowboy, "~> 2.2"},
      {:rollbax, "~> 0.10"},
      {:corsica, "~> 1.1"},
      {:phoenix_live_view, "~> 0.13"},
      # {:fluex, git: "https://github.com/kaitsh/fluex.git", branch: "develop"},
      {:fluex, "~> 0.1.1"},
      {:phoenix_live_dashboard, "~> 0.1"},
      {:jose, "~> 1.9"},
      {:x509, "~> 0.8"}
    ]
  end

  # Aliases are shortcuts or tasks specific to the current project.
  # For example, to create, migrate and run the seeds file at once:
  #
  #     $ mix ecto.setup
  #
  # See the documentation for `Mix` for more info on aliases.
  defp aliases do
    [
      "ecto.seed": ["run priv/repo/seeds.#{Mix.env()}.exs"],
      "ecto.setup": ["ecto.create", "ecto.migrate", "ecto.seed"],
      "ecto.reset": ["ecto.drop", "ecto.setup"],
      test: ["ecto.create --quiet", "ecto.migrate", "test"]
    ]
  end
end
