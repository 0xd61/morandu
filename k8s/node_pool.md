Create preemtive with taints:

```
task=preemptive
```

deployments that can run on preemtive need:

```yaml
template:
  spec:
    tolerations:
      - effect: NoSchedule
    key: task
    operator: Equal
    value: preemptive
```
